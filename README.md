# 2D Playground

2D Playground is an educational project where the goal is to learn about different 2D topics/tools found in game development and rendering.
The project is built as a single application where each topic explored is its own "mode". The project currently only supports Windows, is built with C++14 and uses OpenGL 4.3 for rendering.

### Project wide TODOs:
 - Support Linux
 - Integrate one other graphics API(either Direct3D 12 or Vulkan)

### Topics/Tools
 - [Tile Editor](#tile-editor)
 - [Delaunay/Voronoi Generator](#delaunayvoronoi-generator)
 - [Shape Shooter](#shape-shooter)
 - Procedurally generated maps/worlds(TODO)
 - 2D light(TODO)
 - Animation(TODO)

## Tile Editor

 A tile editor inspired by [Tiled](https://github.com/mapeditor/tiled). This is currently a brute-force approach since a lot of exploration of 
 how the editor should work and what features it should support is still being done. This means that it uses a lot(and I mean a lot) of 
 unnecessary resources both in terms of rendering and memory in general. 

### Features:
 - Load custom tile sets
 - Add layers
 - Stamp brush tool
 - Rectangle Fill tool
 - Flood Fill
 - Erase
 - Rectangle Erase

### TODOs:
 - Tile collision or collision polygons
 - Solid color tile
 - Add animated sprites
 - Change layer order
 - Saving and loading with custom made format as well as formats like TMX and TSX
 - Optimize memory usage and rendering
 - More tools?

![Tile Editor](https://wintrytbs.gitlab.io/website_test/images/2d_playground/tile_editor.PNG)

Tile set by [Kauzz](https://kauzz.itch.io/pixelforest)
## Delaunay/Voronoi Generator 

 A basic delaunay-voronoi generator developed to explore topics of triangulation and just as a fun visual tool in general.
 At the moment voronoi is generated from the delaunay set and neither generator uses an efficient algorithm so it's a bit slow.
 In the future I will look into better algorithms for generating both as well as trying to animate the result. 

 The screenshot below is made using the delaunay/voronoi set as a sampling pattern for an image of [Vincent van Gogh](https://www.artic.edu/artworks/80607/self-portrait)

![Delaunay-Voronoi](https://wintrytbs.gitlab.io/website_test/images/2d_playground/delaunay_voronoi.PNG)

## Shape Shooter

 A simple minigame where the player controls a spaceship while trying to avoid and destroy the dangerous particles
 bouncing around.  
 The simulation of particles is based on a simplified version of elastic collision. Simplified in the sense that a lot of edge cases aren't handled  
 and all the objects are spheres with the same mass.
 
 ![Shape Shooter Demo](https://wintrytbs.gitlab.io/website_test/images/2d_playground/shape_shooter1.gif)
## Dependencies
 Since this is an educational project the dependencies are minimal and are all included in the libs folder.
 - [Dear ImGui](https://github.com/ocornut/imgui) - For everything GUI
 - [stb_image](https://github.com/nothings/stb) - For loading images
 - [OpenGL API and Extension Header Files](https://www.khronos.org/registry/OpenGL/index_gl.php)

## Building
 The application can be built by running the **build.bat** file located in the **code** directory. The default compiler is MSVC's cl.exe so it 
 needs to be available on the command line.
 ```
 build.bat
 ``` 
 Clang can be invoked instead by passing **clang** as an argument to the build file 
 ```
 build.bat clang
 ```
 This will create a build directory at the project root containing the output files.
