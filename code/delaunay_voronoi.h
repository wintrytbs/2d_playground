#ifndef DELAUNAY_VORONOI_H
#define DELAUNAY_VORONOI_H

namespace dv
{
    struct triangle_2d
    {
        v2 v[3];
    };

    struct triangle_edge
    {
        v2 p0;
        v2 p1;
        bool deleted;
        u32 index;
    };

    struct shared_edge
    {
        v2 p0;
        v2 p1;
        u32 t1;
        u32 t2;
    };

    struct voronoi_edge
    {
        v2 p0;
        v2 p1;
    };

    struct point_list
    {
        v2 *points;
        u32 count;
        f32 scaleX;
        f32 scaleY;
    };

    struct delaunay_state
    {
        triangle_2d *triangleList;
        u32 triangleCount;
        v2 *pointList;
        u32 pointCount;

        v2 *circumCenterList;
        f32 *circumRadiusList;

        u32 currentPointIndex;

        v2 containerPoints[3];

        shared_edge *sharedEdgeList;
        u32 sharedEdgeCount;
    };

    struct delaunay_result
    {
        triangle_2d *triangles;
        u32 triangleCount;
        shared_edge *edges;
        u32 edgeCount;
    };

    struct voronoi_state
    {
        voronoi_edge *edges;
        u32 edgeCount;
        triangle_2d *triangles;
        u32 triangleCount;
    };

    struct delaunay_voronoi_render_state
    {
        GLuint pointVAOID;
        GLuint pointVBOID;

        GLuint delaunayVAOID;
        GLuint delaunayVBOID;

        GLuint voronoiVAOID;
        GLuint voronoiVBOID;

        GLuint voronoiFillVAOID;
        GLuint voronoiFillVBOID;

        delaunay_state dState;
        voronoi_state vState;

        point_list pointList;

        std::mt19937 rngState;

        GLuint imageTexID;
        bool isInit;
    };
}
#endif
