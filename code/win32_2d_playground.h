#ifndef WIN32_2D_PLAYGROUND_H
struct file_data
{
    u8 *data;
    u64 size;
};

enum input_key
{
    InputKey_Tab,
    InputKey_LeftArrow,
    InputKey_RightArrow,
    InputKey_UpArrow,
    InputKey_DownArrow,
    InputKey_PageUp,
    InputKey_PageDown,
    InputKey_Home,
    InputKey_End,
    InputKey_Insert,
    InputKey_Delete,
    InputKey_Backspace,
    InputKey_Space,
    InputKey_Enter,
    InputKey_Escape,
    InputKey_KeyPadEnter,
    InputKey_A,
    InputKey_C,
    InputKey_V,
    InputKey_X,
    InputKey_Y,
    InputKey_Z,
    InputKey_W,
    InputKey_D,
    InputKey_S,
    InputKey_P,
    InputKey_Plus,

    INPUT_KEY_COUNT,
};

enum mouse_button
{
    MouseButton_Left,
    MouseButton_Middle,
    MouseButton_Right,
    MouseButton_Count,
};

struct input_map
{
    bool keyDown[INPUT_KEY_COUNT];
    f32 keyDownDuration[INPUT_KEY_COUNT];
    v2 mousePos;
    v2 mousePosPrev;
    v2 mouseDownPos;
    bool mouseButtonDown[MouseButton_Count];
    f32 mouseDownDuration[MouseButton_Count];
    f32 mouseWheelDelta;
};

struct rect_data
{
    v2 pos;
    v2 size;

    rect_data() = default;

    rect_data(v2 p, v2 s)
    {
        pos = p;
        size = s;
    }
};

template <typename T>
struct tbs_array_g
{
    T *data;
    i32 count;
    i32 capacity;

    tbs_array_g()
    {
        data = nullptr;
        count = 0;
        capacity = 0;
    }

    inline T& operator[](i32 i)
    {
        tbs_assert(i < count);
        return data[i];
    }
    inline void Add(const T& v)
    {
        if(count == capacity)
        {
            i32 newCap = (capacity ? capacity * 2 : 8);
            T *newMem = new T[(size_t)newCap];
            if(data)
            {
                memcpy(newMem, data, CalcSize());
                delete[] data;
            }

            data = newMem;
            capacity = newCap;
        }

        memcpy(data + count, &v, sizeof(T));
        count++;
    }

    inline size_t CalcSize()
    {
        size_t result = sizeof(T) * (size_t)count;
        return result;
    }

    inline void Free()
    {
        tbs_assert(data != nullptr);
        tbs_assert(capacity > 0);
        delete[] data;
        data = nullptr;
        count = 0;
        capacity = 0;
    }
};

enum scene_id
{
    SceneID_None,
    SceneID_TileEditor,
    SceneID_ShapeShooter,
    SceneID_Delaunay_Voronoi,
    SceneID_PauseMenu,
};

enum shader_id
{
    ShaderID_Rect,
    ShaderID_Point,
    ShaderID_Triangle,
    ShaderID_Sprite,
    ShaderID_Imgui,
    ShaderID_Tile,
    ShaderID_Delaunay,
    ShaderID_Voronoi,
    ShaderID_ShapeShooterPlayer,
    ShaderID_ShapeShooterPrimitive,
    ShaderID_ShapeShooterSdfCircle,

    SHADER_ID_COUNT,
};

struct application_context
{
    f32 runtime;
    f32 dt;
    v2 clientSize;

    // TODO(torgrim): Handle longer paths?
    char executablePath[MAX_PATH];
    char shaderPath[MAX_PATH];
    char texturePath[MAX_PATH];

    opengl_shader_info shaderList[SHADER_ID_COUNT];
};

#if defined(__clang__)
__attribute__((__format__(__printf__, 1, 2)))
#endif
static void PrintDebugMsg(const char *fmt, ...)
{
    va_list argList;
    va_start(argList, fmt);
    vprintf(fmt, argList);
    va_end(argList);
}

#if defined(__clang__)
__attribute__((__format__(__printf__, 1, 2)))
#endif
static void PrintDebugMsgDebugger(const char *fmt, ...)
{
    char buffer[512];
    tbs_s_assert(array_count(buffer) == 512, "Debug Message Buffer Invalid Size");
    va_list argList;
    va_start(argList, fmt);
    int inputSize = vsnprintf_s(buffer, array_count(buffer), fmt, argList);
    tbs_assert(inputSize < 512);
    va_end(argList);

    OutputDebugString(buffer);
}

static f32 NextRand01(std::mt19937 &mt);
static f32 NextRandRangeF32(std::mt19937 &mt, f32 min, f32 max);
static u32 NextRandRangeU32(std::mt19937 &mt, u32 min, u32 max);
static LARGE_INTEGER Win32GetWallClockTime();
static f32 Win32GetElapsedSeconds(LARGE_INTEGER startTime, LARGE_INTEGER endTime);
static file_data Win32ReadFile(const char *fileName, bool zeroTerminate);
static inline file_data Win32ReadAllText(const char *filename)
{
    file_data result = Win32ReadFile(filename, true);
    return result;
}

static inline char *GetTextureAssetFullPath(application_context *appCtx, char *buffer, const char *name)
{
    RawStringConcat(buffer, appCtx->texturePath, name);
    return buffer;
}

static inline char *GetShaderFullPath(application_context *appCtx, char *buffer, const char *name)
{
    RawStringConcat(buffer, appCtx->shaderPath, name);
    return buffer;
}

static inline opengl_shader_info *GetShaderInfo(application_context *appCtx, shader_id id)
{
    opengl_shader_info *shader = appCtx->shaderList+id;
    return shader;
}

#define WIN32_2D_PLAYGROUND_H
#endif
