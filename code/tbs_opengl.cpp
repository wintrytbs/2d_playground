#include "tbs_opengl.h"

static void OpenGLFlushError()
{
    while(glGetError() != GL_NO_ERROR);
}

static bool OpenGLCatchError()
{
    GLenum error = glGetError();
    if(error == GL_NO_ERROR)
    {
        return false;
    }

    switch(error)
    {
        case GL_INVALID_ENUM:
            {
                PrintDebugMsg("OpenGL::ERROR:: Invalid Enum\n");
            } break;
        case GL_INVALID_VALUE:
            {
                PrintDebugMsg("OpenGL::ERROR:: Invalid Value\n");
            } break;
        case GL_INVALID_OPERATION:
            {
                PrintDebugMsg("OpenGL::ERROR:: Invalid Operation\n");
            } break;
        case GL_INVALID_FRAMEBUFFER_OPERATION:
            {
                PrintDebugMsg("OpenGL::ERROR:: Invalid Framebuffer Operation\n");
            } break;
        case GL_OUT_OF_MEMORY:
            {
                PrintDebugMsg("OpenGL::ERROR:: Out of Memory\n");
            } break;
        case GL_STACK_UNDERFLOW:
            {
                PrintDebugMsg("OpenGL::ERROR:: Stack Underflow\n");
            } break;
        case GL_STACK_OVERFLOW:
            {
                PrintDebugMsg("OpenGL::ERROR:: Stack Overflow\n");
            } break;
        default:
            {
                PrintDebugMsg("OpenGL::ERROR:: Unknown Error\n");
            }
    }

    tbs_assert(false);

    return false;
}

static bool OpenGLCheckFramebufferStatus()
{
    if(GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        switch(status)
        {
            case GL_FRAMEBUFFER_UNDEFINED:
            {
                PrintDebugMsg("FRAMEBUFFER STATUS ERROR:: GL_FRAMEBUFFER_UNDEFINED\n");
            }break;
            case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
            {
                PrintDebugMsg("FRAMEBUFFER STATUS ERROR:: GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT\n");
            }break;
            case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
            {
                PrintDebugMsg("FRAMEBUFFER STATUS ERROR:: GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT\n");
            }break;
            case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:
            {
                PrintDebugMsg("FRAMEBUFFER STATUS ERROR:: GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER\n");
            }break;
            case GL_FRAMEBUFFER_UNSUPPORTED:
            {
                PrintDebugMsg("FRAMEBUFFER STATUS ERROR:: GL_FRAMEBUFFER_UNSUPPORTED\n");
            }break;
            case GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE:
            {
                PrintDebugMsg("FRAMEBUFFER STATUS ERROR:: GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE\n");
            }break;
            case GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS:
            {

                PrintDebugMsg("FRAMEBUFFER STATUS ERROR:: GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGET\n");
            }break;
            default:
            {
                PrintDebugMsg("FRAMEBUFFER STATUS ERROR:: UNKNOWN ERROR\n");
            }
        }

        tbs_assert(false);
    }

    return true;
}

static void OpenGLCompileShader(GLuint shaderID, const char *sourceList[], GLsizei count)
{
    glShaderSource(shaderID, count, sourceList, nullptr);
    glCompileShader(shaderID);

    GLint vertCompileResult;
    glGetShaderiv(shaderID, GL_COMPILE_STATUS, &vertCompileResult);
    if(vertCompileResult == GL_FALSE)
    {
        GLsizei logLength;
        GLchar logBuffer[OPENGL_MAX_DEBUG_MSG_SIZE];
        glGetShaderInfoLog(shaderID, OPENGL_MAX_DEBUG_MSG_SIZE, &logLength, logBuffer);
        PrintDebugMsg("ERROR::SHADER_COMPILE:: could not compile vert shader\n");
        PrintDebugMsg(logBuffer);

        tbs_assert(false);
    }

}

static void OpenGLLinkProgram(GLuint programID, GLuint *shaderList, i32 shaderCount)
{
    for(i32 shaderIndex = 0; shaderIndex < shaderCount; ++shaderIndex)
    {
        glAttachShader(programID, shaderList[shaderIndex]);
    }

    glLinkProgram(programID);
    GLint linkStatus;
    glGetProgramiv(programID, GL_LINK_STATUS, &linkStatus);
    if(linkStatus == GL_FALSE)
    {
        GLsizei logLength;
        GLchar logBuffer[OPENGL_MAX_DEBUG_MSG_SIZE];
        glGetProgramInfoLog(programID, OPENGL_MAX_DEBUG_MSG_SIZE, &logLength, logBuffer);
        PrintDebugMsg("ERROR::SHADER_LINK:: could not link program\n");
        PrintDebugMsg(logBuffer);

        tbs_assert(false);
    }
}


static opengl_shader_info OpenGLCreateProgram(const char *shaderSource)
{
    GLuint vertShaderID = glCreateShader(GL_VERTEX_SHADER);
    const char *vertexShader[] =
    {
        SHADER_VERSION_PREFIX,
        VERT_SHADER_PREFIX,
        shaderSource,
    };
    OpenGLCompileShader(vertShaderID, vertexShader, array_count(vertexShader));

    GLuint fragShaderID = glCreateShader(GL_FRAGMENT_SHADER);
    const char *fragmentShader[] =
    {
        SHADER_VERSION_PREFIX,
        FRAG_SHADER_PREFIX,
        shaderSource,
    };
    OpenGLCompileShader(fragShaderID, fragmentShader, array_count(fragmentShader));

    GLuint shaderList[] =
    {
        vertShaderID,
        fragShaderID,
    };

    GLuint programID = glCreateProgram();
    OpenGLLinkProgram(programID, shaderList, array_count(shaderList));

    opengl_shader_info si = {};
    si.vertShaderID = vertShaderID;
    si.fragShaderID = fragShaderID;
    si.programID = programID;
    return si;
}

// TODO(torgrim): Using win32 specific functionality directly which shouldn't be
// the case
static void OpenGLReloadShader(opengl_shader_info *shader)
{
    tbs_assert(shader->programID > 0);
    tbs_assert(shader->vertShaderID > 0);
    tbs_assert(shader->fragShaderID > 0);
    tbs_assert(shader->filename != NULL);

    glDetachShader(shader->programID, shader->vertShaderID);
    glDetachShader(shader->programID, shader->fragShaderID);

    file_data shaderFile = Win32ReadAllText(shader->filename);
    tbs_assert(shaderFile.size != 0);

    const char *vertexShader[] =
    {
        SHADER_VERSION_PREFIX,
        VERT_SHADER_PREFIX,
        (char *)shaderFile.data,
    };
    OpenGLCompileShader(shader->vertShaderID, vertexShader, array_count(vertexShader));

    const char *fragmentShader[] =
    {
        SHADER_VERSION_PREFIX,
        FRAG_SHADER_PREFIX,
        (char *)shaderFile.data,
    };
    OpenGLCompileShader(shader->fragShaderID, fragmentShader, array_count(fragmentShader));

    GLuint shaderList[] =
    {
        shader->vertShaderID,
        shader->fragShaderID,
    };

    OpenGLLinkProgram(shader->programID, shaderList, array_count(shaderList));
}

// TODO(torgrim): Using win32 specific functionality directly which shouldn't be
// the case
static opengl_shader_info OpenGLCreateProgramFromSingleFile(const char *filename)
{
    file_data shaderFile = Win32ReadAllText(filename);
    tbs_assert(shaderFile.size != 0);
    opengl_shader_info si = OpenGLCreateProgram((char *)shaderFile.data);
    si.filename = RawStringAllocCopy(filename);
    return si;
}

static opengl_texture_info OpenGLCreateTextureWithInfoFromFile(const char *filename)
{
    i32 imageW;
    i32 imageH;
    i32 imageChannels;
    stbi_set_flip_vertically_on_load(true);
    unsigned char *imagePixels = stbi_load(filename, &imageW, &imageH, &imageChannels, 0);
    tbs_assert(imagePixels != nullptr);
    tbs_assert(imageChannels == 3 || imageChannels == 4);
    GLenum imageFormat = GL_RGB;
    if(imageChannels != 3)
    {
        imageFormat = GL_RGBA;
    }

    GLuint imageTexID;
    glGenTextures(1, &imageTexID);
    glBindTexture(GL_TEXTURE_2D, imageTexID);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, imageW, imageH, 0, imageFormat, GL_UNSIGNED_BYTE, imagePixels);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glBindTexture(GL_TEXTURE_2D, 0);

    opengl_texture_info result = {};
    result.texID = imageTexID;
    result.width = imageW;
    result.height = imageH;

    return result;
}

static GLuint OpenGLCreateDefaultTextureFromFile(char *filename)
{
    i32 imageW;
    i32 imageH;
    i32 imageChannels;
    stbi_set_flip_vertically_on_load(true);
    unsigned char *imagePixels = stbi_load(filename, &imageW, &imageH, &imageChannels, 0);
    tbs_assert(imagePixels != nullptr);
    tbs_assert(imageChannels == 3 || imageChannels == 4);
    GLenum imageFormat = GL_RGB;
    if(imageChannels != 3)
    {
        imageFormat = GL_RGBA;
    }

    GLuint imageTexID;
    glGenTextures(1, &imageTexID);
    glBindTexture(GL_TEXTURE_2D, imageTexID);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, imageW, imageH, 0, imageFormat, GL_UNSIGNED_BYTE, imagePixels);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glBindTexture(GL_TEXTURE_2D, 0);

    return imageTexID;
}

