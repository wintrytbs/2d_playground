#pragma warning(disable: 4710 4711)
#pragma warning(push, 0)
#include <stdio.h>
#include <memory.h>
#include <stdlib.h>
#pragma warning(pop)


struct tbs_gl_func
{
    const char *funcName;
    const char *funcMacro;
};

static char *Upp(const char *s)
{
    size_t i = 0;
    char c = s[i];

    while(s[i] != '\0')
    {
        ++i;
    }

    size_t l = i+1u;
    char *result = (char *)malloc(l);
    memcpy(result, s, l);
    i = 0;

    while(c != '\0')
    {
        if(c >= 'a' && c <= 'z')
        {
            result[i] = 'A' + (c - 'a');
        }
        else
        {
            result[i] = c;
        }

        ++i;
        c = s[i];
    }

    return result;
}

#define gs(name) {#name, "PFN" #name "PROC"}
#define array_count(arr) (sizeof(arr) / sizeof((arr)[0]))

static tbs_gl_func funcList[]
{
    gs(wglChoosePixelFormatARB),
    gs(wglCreateContextAttribsARB),
    gs(wglSwapIntervalEXT),
    gs(glShaderSource),
    gs(glCompileShader),
    gs(glGetShaderiv),
    gs(glGetShaderInfoLog),
    gs(glAttachShader),
    gs(glDetachShader),
    gs(glLinkProgram),
    gs(glGetProgramiv),
    gs(glGetProgramInfoLog),
    gs(glCreateShader),
    gs(glCreateProgram),
    gs(glGenBuffers),
    gs(glBindBuffer),
    gs(glBufferData),
    gs(glVertexAttribPointer),
    gs(glEnableVertexAttribArray),
    gs(glDisableVertexAttribArray),
    gs(glUseProgram),
    gs(glGetUniformLocation),
    gs(glUniform1i),
    gs(glUniform1f),
    gs(glUniform2f),
    gs(glUniform3f),
    gs(glGenVertexArrays),
    gs(glBindVertexArray),
    gs(glUniformMatrix4fv),
    gs(glUniformMatrix3fv),
    gs(glBindFramebuffer),
    gs(glBlitFramebuffer),
    gs(glGenFramebuffers),
    gs(glFramebufferTexture2D),
    gs(glCheckFramebufferStatus),
    gs(glTexImage2DMultisample),
    gs(glBlendEquation),
    gs(glActiveTexture),
    gs(glDeleteBuffers),
    gs(glDeleteVertexArrays),
    gs(glGetAttribLocation),
    gs(glDrawArraysIndirect),
    gs(glMultiDrawArrays),
};


int main()
{
    for(size_t i = 0; i < array_count(funcList); ++i)
    {
        funcList[i].funcMacro = Upp(funcList[i].funcMacro);
    }
    for(size_t i = 0; i < array_count(funcList); ++i)
    {
        printf("static %s %s;\n", funcList[i].funcMacro, funcList[i].funcName);
    }

    printf("static void LoadGLFunctions()\n");
    printf("{\n");
    for(size_t i = 0; i < array_count(funcList); ++i)
    {
        tbs_gl_func fn = funcList[i];
        printf("    %s = (%s)wglGetProcAddress(\"%s\");\n", fn.funcName, fn.funcMacro, fn.funcName);
    }
    printf("}\n");
}
