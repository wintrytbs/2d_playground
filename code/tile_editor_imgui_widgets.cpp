enum file_browser_dialog_result
{
    DialogResult_None,
    DialogResult_Cancel,
    DialogResult_Open,
};

struct file_browser_context
{
    bool init;
    char searchDir[255];
    char extension[5];

    file_browser_dialog_result dialogResult;
    char dataResult[255];

};

static bool ShowFileBrowserWidget(file_browser_context *ctx)
{
    // TODO(torgrim): We probably don't want to query
    // the OS for FindFirstFile etc each frame.
    // Just cache the result and update when changing
    // directory. Might subscribe to notifications
    // for changes in the current directory as well.
    bool result = false;

    ImGui::OpenPopup("File Browser##tbs_file_browser");
    ImGui::SetNextWindowSize(ImVec2(600, 400), ImGuiCond_FirstUseEver);
    if(ImGui::BeginPopupModal("File Browser##tbs_file_browser"))
    {

        // TODO(torgrim): Move this so that we don't need a local persist, maybe into
        // the gui state.
        char *searchDir = ctx->searchDir;
        local_persist i32 selectedDirItem = -1;

        char queryString[MAX_PATH];
        RawStringConcat(queryString, searchDir, "*");
        WIN32_FIND_DATAA fileList[255];
        HANDLE searchHandle = FindFirstFileA(queryString, fileList);
        i32 dirCount = 1;
        if(searchHandle != INVALID_HANDLE_VALUE)
        {
            while(FindNextFile(searchHandle, fileList + dirCount))
            {
                tbs_assert(dirCount < 255);
                ++dirCount;
            }

            FindClose(searchHandle);
        }

        if(dirCount > 0)
        {

            ImGui::Text("Directory: %s", searchDir);

            ImGui::PushStyleColor(ImGuiCol_ChildBg, IM_COL32(100, 100, 100, 100));
            ImGui::Columns(1, "tbs_file_viewer_columns");
            ImGui::Separator();
            ImGui::Text("Name");
            ImGui::Separator();
            ImGui::BeginChild("tbs_file_viewer", ImVec2(0, -ImGui::GetFrameHeightWithSpacing()));
            for(i32 i = 0; i < dirCount; ++i)
            {
                if((fileList[i].dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) || ctx->extension[0] == '\0' || MatchFileExtension(fileList[i].cFileName, ctx->extension))
                {
                    if(ImGui::Selectable(fileList[i].cFileName, selectedDirItem == i, ImGuiSelectableFlags_SpanAllColumns))
                    {
                        selectedDirItem = i;
                    }
                }
            }

            ImGui::Columns(1);
            ImGui::Separator();
            ImGui::PopStyleColor();
            ImGui::EndChild();

            if(selectedDirItem != -1 && (fileList[selectedDirItem].dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
            {
                char *dirName = fileList[selectedDirItem].cFileName;
                size_t nameLength = RawStringLength(dirName);
                if(nameLength != 1 || dirName[0] != '.')
                {
                    if(nameLength == 2 && dirName[0] == '.' && dirName[1] == '.')
                    {
                        auto offsetResult = GetParentDirectoryOffset(searchDir);
                        if(offsetResult.valid)
                        {
                            searchDir[offsetResult.offset+1] = '\0';
                        }
                    }
                    else
                    {
                        AppendToPath(searchDir, fileList[selectedDirItem].cFileName, PathAppendOption_AddPathSeparator);
                    }
                }

                selectedDirItem = -1;
            }

            if(ImGui::Button("Open"))
            {
                tbs_assert(selectedDirItem != -1);
                if(ctx->extension[0] == '\0' || MatchFileExtension(fileList[selectedDirItem].cFileName, ctx->extension))
                {
                    ImGui::CloseCurrentPopup();

                    ctx->dialogResult = DialogResult_Open;
                    RawStringConcat(ctx->dataResult, searchDir, fileList[selectedDirItem].cFileName);
                    selectedDirItem = -1;

                    result = true;
                }
            }

            ImGui::SameLine();

            if(ImGui::Button("Cancel"))
            {
                ImGui::CloseCurrentPopup();

                result = true;
                ctx->dialogResult = DialogResult_Cancel;
                selectedDirItem = -1;
            }
            ImGui::EndPopup();
        }
    }

    return result;
}

