#if defined(__clang__)
#pragma GCC diagnostic push
// NOTE(torgrim): Allow this here since it lines up better with the
// the msdn win32 documentation.
#pragma GCC diagnostic ignored "-Wzero-as-null-pointer-constant"
#endif
static HWND Win32InitOpenGL(HINSTANCE instance, WNDPROC windowMsgProc, i32 w, i32 h)
{
    // TODO(torgrim): not sure we actually need to create a dummy windows,
    // only a dummy context?
    WNDCLASSA dummyClass = {};
    dummyClass.style = CS_OWNDC;
    dummyClass.lpfnWndProc = DefWindowProc;
    dummyClass.hInstance = instance;
    dummyClass.lpszClassName = "dummy";

    if(RegisterClass(&dummyClass) == 0)
    {
        PrintDebugMsg("ERROR::WIN32:: could not register dummy class");
        return nullptr;
    }

    HWND dummyWindowHandle = CreateWindowExA(0, "dummy", NULL, 0,
                                             50, 50,
                                             w, h,
                                             NULL, NULL,
                                             instance, NULL);
    if(dummyWindowHandle == NULL)
    {
        PrintDebugMsg("ERROR::WIN32:: could not create dummy window");
        return NULL;
    }

    HDC dummyDC = GetDC(dummyWindowHandle);

    PIXELFORMATDESCRIPTOR dummy_ppfd = {};
    dummy_ppfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
    dummy_ppfd.nVersion = 1;
    dummy_ppfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
    dummy_ppfd.iPixelType = PFD_TYPE_RGBA;

    int dummyFormatIndex = ChoosePixelFormat(dummyDC, &dummy_ppfd);

    if(dummyFormatIndex == 0)
    {
        PrintDebugMsg("ERROR::WIN32:: could not choose pixel format");
    }

    if(SetPixelFormat(dummyDC, dummyFormatIndex, &dummy_ppfd) == FALSE)
    {
        PrintDebugMsg("ERROR::WIN32:: could not set pixel format");
        return NULL;
    }

    HGLRC dummyRC = wglCreateContext(dummyDC);
    if(wglMakeCurrent(dummyDC, dummyRC) == FALSE)
    {
        PrintDebugMsg("ERROR::WGL:: could not make current context");
        return NULL;
    }


    const char *className = "opengl_main_window";
    const char *windowTitle = "";
    WNDCLASSA windowClass = {};
    windowClass.style = CS_OWNDC | CS_HREDRAW | CS_VREDRAW;
    windowClass.lpfnWndProc = windowMsgProc;
    windowClass.hInstance = instance;
    windowClass.lpszClassName = className;

    // TODO(torgrim): This is only temporary should be
    // NULL when we want full control over the cursor
    windowClass.hCursor = LoadCursor(NULL, IDC_ARROW);

    if(RegisterClass(&windowClass) == 0)
    {
        PrintDebugMsg("ERROR::WIN32:: could not register class");
        return NULL;
    }

    HWND windowHandle = CreateWindowExA(0, className, windowTitle,
                                        WS_VISIBLE | WS_OVERLAPPEDWINDOW,
                                        50, 50,
                                        w, h,
                                        NULL, NULL,
                                        instance, NULL);
    if(windowHandle == NULL)
    {
        PrintDebugMsg("ERROR::WIN32:: could not create window");
        return NULL;
    }

    HDC deviceContext = GetDC(windowHandle);

    LoadGLFunctions();

    const i32 pixelFormatAttribList[] =
    {
        WGL_DRAW_TO_WINDOW_ARB, GL_TRUE,
        WGL_SUPPORT_OPENGL_ARB, GL_TRUE,
        WGL_DOUBLE_BUFFER_ARB, GL_TRUE,
        WGL_PIXEL_TYPE_ARB, WGL_TYPE_RGBA_ARB,
        WGL_COLOR_BITS_ARB, 32,
        WGL_DEPTH_BITS_ARB, 24,
        WGL_STENCIL_BITS_ARB, 8,
        WGL_SAMPLE_BUFFERS_ARB, 1,
        WGL_SAMPLES_ARB, 8,
        0,
    };

    UINT matchingFormats;
    i32 formatIndex;
    BOOL gotWGLFormat = wglChoosePixelFormatARB(deviceContext,
                                                pixelFormatAttribList,
                                                NULL,
                                                1,
                                                &formatIndex,
                                                &matchingFormats);
    if(gotWGLFormat == FALSE)
    {
        PrintDebugMsg("ERROR::WGL:: could not choose pixel format");
        return NULL;
    }


    PIXELFORMATDESCRIPTOR ppfd = {};
    if(DescribePixelFormat(deviceContext, formatIndex, sizeof(ppfd), &ppfd) == 0)
    {
        PrintDebugMsg("ERROR::WIN32:: could not describe pixel format");
        return NULL;
    }

    if(SetPixelFormat(deviceContext, formatIndex, &ppfd) == FALSE)
    {
        PrintDebugMsg("ERROR::WIN32:: could not set pixel format");
        return NULL;
    }

    const i32 contextAttribList[] =
    {
        WGL_CONTEXT_MAJOR_VERSION_ARB, 4,
        WGL_CONTEXT_MINOR_VERSION_ARB, 3,
        WGL_CONTEXT_PROFILE_MASK_ARB, WGL_CONTEXT_CORE_PROFILE_BIT_ARB,
        0,
    };

    HGLRC renderContext = wglCreateContextAttribsARB(deviceContext, 0, contextAttribList);

    if(wglMakeCurrent(deviceContext, renderContext) == FALSE)
    {
        PrintDebugMsg("ERROR::WGL:: could not make current context");
        return NULL;
    }

    if(wglDeleteContext(dummyRC) == FALSE)
    {
        PrintDebugMsg("ERROR::WGL:: could not delete dummy context");
        return NULL;
    }

    if(DeleteDC(dummyDC) == FALSE)
    {
        PrintDebugMsg("ERROR::WIN32:: could not delete dummy dc");
        return NULL;
    }

    if(DestroyWindow(dummyWindowHandle) == FALSE)
    {
        PrintDebugMsg("ERROR::WIN32:: could not destroy dummy window");
        return NULL;
    }

    if(UnregisterClass("dummy", instance) == FALSE)
    {
        PrintDebugMsg("ERROR::WIN32:: could not unregister dummy class");
        return NULL;
    }

    LoadGLFunctions();

    return windowHandle;
}
#if defined(__clang__)
#pragma GCC diagnostic pop
#endif
