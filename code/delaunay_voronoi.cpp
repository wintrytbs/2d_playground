#include "delaunay_voronoi.h"

namespace dv
{
    global const f32 SUPER_TRIANGLE_MIN_X =  -2.0f;
    global const f32 SUPER_TRIANGLE_MAX_X =  6.0f;
    global const f32 SUPER_TRIANGLE_MIN_Y =  -2.0f;
    global const f32 SUPER_TRIANGLE_MAX_Y =  8.0f;

    static delaunay_state InitDelaunayState(v2 *points, u32 pointCount)
    {
        triangle_2d superTriangle = {};
        superTriangle.v[0] = v2(SUPER_TRIANGLE_MIN_X, SUPER_TRIANGLE_MIN_Y);
        superTriangle.v[1] = v2(SUPER_TRIANGLE_MAX_X, SUPER_TRIANGLE_MIN_Y);
        superTriangle.v[2] = v2(SUPER_TRIANGLE_MIN_X, SUPER_TRIANGLE_MAX_Y);

        v2 circumCenter = CircumCenter(superTriangle.v[0], superTriangle.v[1], superTriangle.v[2]);
        f32 circumRadius = Distance(circumCenter, superTriangle.v[0]);

        u32 maxTriangleCount = (2u * (pointCount + 3)) - 2 - 3;
        auto *triangleList = new triangle_2d[maxTriangleCount];
        auto *circumCenterList = new v2[maxTriangleCount];
        auto *circumRadiusList = new f32[maxTriangleCount];

        triangleList[0] = superTriangle;
        circumCenterList[0] = circumCenter;
        circumRadiusList[0] = circumRadius;

        delaunay_state result = {};
        result.triangleList = triangleList;
        result.triangleCount = 1;
        result.circumCenterList = circumCenterList;
        result.circumRadiusList = circumRadiusList;
        result.pointList = points;
        result.pointCount = pointCount;
        result.containerPoints[0] = superTriangle.v[0];
        result.containerPoints[1] = superTriangle.v[1];
        result.containerPoints[2] = superTriangle.v[2];

        return result;
    }

    // TODO(torgrim): Consider putting this into its own file together with
    // other random related functions.
    static point_list GenerateRandomPoints(std::mt19937 &gen, f32 scaleX, f32 scaleY, u32 count)
    {
        auto startTime = Win32GetWallClockTime();
        v2 *pointSet = new v2[count]();

        pointSet[0] = v2(0.0f, 0.0f);
        pointSet[1] = v2(scaleX + 0.0f, 0.0f);
        pointSet[2] = v2(scaleX + 0.0f, scaleY + 0.0f);
        pointSet[3] = v2(0.0f, scaleY + 0.0f);
        u32 pIndex = 4;
        bool validPlacement = false;
        while(pIndex < count)
        {
            validPlacement = true;
            pointSet[pIndex] = v2(NextRand01(gen) * scaleX + 0.0f, NextRand01(gen) * scaleY + 0.0f);
            for(u32 p2Index = 0; p2Index < pIndex; p2Index++)
            {
                if(Distance(pointSet[pIndex], pointSet[p2Index]) < 0.01f)
                {
                    validPlacement = false;
                    break;
                }
            }

            if(validPlacement)
            {
                ++pIndex;
            }
        }

        point_list result;
        result.points = pointSet;
        result.count = count;
        result.scaleX = scaleX;
        result.scaleY = scaleY;


        auto endTime = Win32GetWallClockTime();
        auto elapsed = Win32GetElapsedSeconds(startTime, endTime);
        PrintDebugMsg("Total Time To Generate %d Points: %f\n", count, elapsed);
        return result;
    }

    static delaunay_voronoi_render_state InitDelaunayVoronoiScene(application_context *ctx)
    {
        std::mt19937 tbs_mt19937(3432);

        GLuint pointVAOID;
        GLuint pointVBOID;
        glGenVertexArrays(1, &pointVAOID);
        glBindVertexArray(pointVAOID);
        glGenBuffers(1, &pointVBOID);
        glBindBuffer(GL_ARRAY_BUFFER, pointVBOID);
        glBufferData(GL_ARRAY_BUFFER, 0, NULL, GL_DYNAMIC_DRAW);
        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
        glEnableVertexAttribArray(0);

        GLuint delaunayVAOID;
        GLuint delaunayVBOID;
        glGenVertexArrays(1, &delaunayVAOID);
        glBindVertexArray(delaunayVAOID);
        glGenBuffers(1, &delaunayVBOID);
        glBindBuffer(GL_ARRAY_BUFFER, delaunayVBOID);
        glBufferData(GL_ARRAY_BUFFER, 0, NULL, GL_DYNAMIC_DRAW);
        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
        glEnableVertexAttribArray(0);

        GLuint voronoiVAOID;
        GLuint voronoiVBOID;
        glGenVertexArrays(1, &voronoiVAOID);
        glBindVertexArray(voronoiVAOID);
        glGenBuffers(1, &voronoiVBOID);
        glBindBuffer(GL_ARRAY_BUFFER, voronoiVBOID);
        glBufferData(GL_ARRAY_BUFFER, 0, NULL, GL_DYNAMIC_DRAW);
        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
        glEnableVertexAttribArray(0);

        GLuint voronoiFillVAOID;
        GLuint voronoiFillVBOID;
        glGenVertexArrays(1, &voronoiFillVAOID);
        glBindVertexArray(voronoiFillVAOID);
        glGenBuffers(1, &voronoiFillVBOID);
        glBindBuffer(GL_ARRAY_BUFFER, voronoiFillVBOID);
        glBufferData(GL_ARRAY_BUFFER, 0, NULL, GL_DYNAMIC_DRAW);
        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
        glEnableVertexAttribArray(0);

        i32 imageW;
        i32 imageH;
        i32 imageChannels;
        stbi_set_flip_vertically_on_load(true);
        char strBuffer[MAX_PATH];
        unsigned char *imagePixels = stbi_load(GetTextureAssetFullPath(ctx, strBuffer, "van_gogh.jpg"), &imageW, &imageH, &imageChannels, 0);
        tbs_assert(imagePixels != nullptr);

        GLuint imageTexID;
        glGenTextures(1, &imageTexID);
        glBindTexture(GL_TEXTURE_2D, imageTexID);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, imageW, imageH, 0, GL_RGB, GL_UNSIGNED_BYTE, imagePixels);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glBindTexture(GL_TEXTURE_2D, 0);

        stbi_image_free(imagePixels);

        // TODO(torgrim): This needs to be handled better. We should have and aspect
        // both for width and height where the one that is not dominating = 1.0f.
        f32 aspect = (imageW > imageH ? (f32)imageW / (f32)imageH : (f32)imageH / (f32)imageW);
        point_list imagePoints = GenerateRandomPoints(tbs_mt19937, 1.0f, 1.0f * aspect, 2000);

        glBindBuffer(GL_ARRAY_BUFFER, pointVBOID);
        glBufferData(GL_ARRAY_BUFFER, (GLsizeiptr)(sizeof(v2) * imagePoints.count), imagePoints.points, GL_DYNAMIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        auto dState = InitDelaunayState(imagePoints.points, imagePoints.count);
        voronoi_state vState = {};

        delaunay_voronoi_render_state result = {};
        result.pointVAOID = pointVAOID;
        result.pointVBOID = pointVBOID;
        result.delaunayVAOID = delaunayVAOID;
        result.delaunayVBOID = delaunayVBOID;
        result.voronoiVAOID = voronoiVAOID;
        result.voronoiVBOID = voronoiVBOID;
        result.voronoiFillVAOID = voronoiFillVAOID;
        result.voronoiFillVBOID = voronoiFillVBOID;

        result.dState = dState;
        result.vState = vState;

        result.pointList = imagePoints;
        result.rngState = tbs_mt19937;

        result.imageTexID = imageTexID;
        result.isInit = true;

        return result;
    }

    // TODO(torgrim): Not an optimal solution. Need to try different methods
    // that has better performance.
    // Currently the triangles generated aren't guaranteed have counter-clockwise
    // order for their vertices. Should try and maintain that property so that
    // it's easier to process data.
    static void IncrementDelaunay(delaunay_state *state, bool computeRemaining)
    {
        if(state->currentPointIndex >= state->pointCount)
        {
            return;
        }

        auto triangulationTimerStart = Win32GetWallClockTime();

        u32 maxTriangleCount = (2u * (state->pointCount + 3)) - 2 - 3;
        u32 maxEdgeCount = (3 * (state->pointCount + 3)) - 3 - 3;
        auto *swapTriangleList = new triangle_2d[maxTriangleCount];
        u32 swapListCount = 0;
        auto *edgeList = new triangle_edge[maxEdgeCount];
        u32 edgeCount = 0;

        auto *triangleList = state->triangleList;
        auto *circumRadiusList = state->circumRadiusList;
        auto *circumCenterList = state->circumCenterList;

        u32 loopCount = 1;
        if(computeRemaining)
        {
            loopCount = state->pointCount - state->currentPointIndex;
        }

        for(u32 pIndex = 0; pIndex < loopCount; ++pIndex)
        {
            edgeCount = 0;
            swapListCount = 0;
            v2 p = state->pointList[state->currentPointIndex++];
            for(u32 tIndex = 0; tIndex < state->triangleCount; ++tIndex)
            {
                triangle_2d t = triangleList[tIndex];
                f32 circumR = circumRadiusList[tIndex];
                v2 circumC = circumCenterList[tIndex];

                f32 circumRadiusSq = circumR * circumR;
                f32 xDistSq = p.x - circumC.x;
                xDistSq *= xDistSq;
                if(xDistSq >= circumRadiusSq)
                {
                    circumCenterList[swapListCount] = circumC;
                    circumRadiusList[swapListCount] = circumR;
                    swapTriangleList[swapListCount++] = t;
                    continue;
                }

                f32 yDist = p.y - circumC.y;
                f32 distSq = xDistSq + (yDist * yDist);
                if(distSq < circumRadiusSq)
                {
                    // Remove triangle and add new edges to the edge list
                    triangle_edge newEdge = {t.v[0], t.v[1], false, 0};
                    edgeList[edgeCount++] = newEdge;
                    newEdge = {t.v[1], t.v[2], false, 0};
                    edgeList[edgeCount++] = newEdge;
                    newEdge = {t.v[2], t.v[0], false, 0};
                    edgeList[edgeCount++] = newEdge;
                }
                else
                {
                    // Is still a valid triangle, add it to the swap list.
                    circumCenterList[swapListCount] = circumC;
                    circumRadiusList[swapListCount] = circumR;
                    swapTriangleList[swapListCount++] = t;
                }
            }

            for(u32 eIndex = 0; eIndex < edgeCount; ++eIndex)
            {
                auto *e = edgeList + eIndex;
                if(!e->deleted)
                {
                    if(eIndex < edgeCount - 1)
                    {
                        for(u32 compareIndex = eIndex + 1; compareIndex < edgeCount; ++compareIndex)
                        {
                            auto *e2 = edgeList + compareIndex;
                            if((e->p0 == e2->p0 && e->p1 == e2->p1) || (e->p0 == e2->p1 && e->p1 == e2->p0))
                            {
                                e->deleted = true;
                                e2->deleted = true;
                                break;
                            }
                        }
                    }

                    if(!e->deleted)
                    {
                        triangle_2d newTriangle;
                        newTriangle.v[0] = e->p0;
                        newTriangle.v[1] = p;
                        newTriangle.v[2] = e->p1;

                        v2 newCenter = CircumCenter(newTriangle.v[0], newTriangle.v[1], newTriangle.v[2]);
                        f32 newRadius = Distance(newCenter, newTriangle.v[0]);
                        circumCenterList[swapListCount] = newCenter;
                        circumRadiusList[swapListCount] = newRadius;
                        swapTriangleList[swapListCount++] = newTriangle;
                    }
                }
            }

            memcpy(triangleList, swapTriangleList, sizeof(triangle_2d) * swapListCount);
            state->triangleCount = swapListCount;

            tbs_assert(state->triangleCount <= maxTriangleCount);
            tbs_assert(edgeCount <= maxEdgeCount);
        }

        f32 triangulationTimerElapsed = Win32GetElapsedSeconds(triangulationTimerStart, Win32GetWallClockTime());
        PrintDebugMsg("Time to create delaunay triangulation: %f seconds\n", triangulationTimerElapsed);

        auto edgeCreationTimerStart = Win32GetWallClockTime();

        tbs_assert(state->triangleCount > 0);
        delete[] swapTriangleList;
        delete[] edgeList;
    }

    // TODO(torgrim): This is slow at the moment(finding shared edges).
    // Should find a better way so that we can update and render in real time
    // while still having a lot of points.
    // TODO(torgrim): Have a method that also calculates voronoi directly
    // from the points instead of through delaunay.
    static voronoi_state CalculateVoronoiFromDelaunay(delaunay_state *state)
    {
        auto edgeCreationTimerStart = Win32GetWallClockTime();

        auto *fullEdgeList = new triangle_edge[state->triangleCount * 3];
        u32 fullEdgeCount = 0;
        for(u32 tIndex = 0; tIndex < state->triangleCount; ++tIndex)
        {
            v2 p0 = state->triangleList[tIndex].v[0];
            v2 p1 = state->triangleList[tIndex].v[1];
            v2 p2 = state->triangleList[tIndex].v[2];

            triangle_edge e0 = {p0, p1, false, tIndex};
            triangle_edge e1 = {p1, p2, false, tIndex};
            triangle_edge e2 = {p2, p0, false, tIndex};

            fullEdgeList[fullEdgeCount++] = e0;
            fullEdgeList[fullEdgeCount++] = e1;
            fullEdgeList[fullEdgeCount++] = e2;
        }

        auto *sharedEdgeList = new shared_edge[state->triangleCount * 3];
        u32 sharedCount = 0;
        while(fullEdgeCount > 1)
        {
            auto e0 = fullEdgeList[--fullEdgeCount];
            for(u32 compareIndex = 0; compareIndex < fullEdgeCount; ++compareIndex)
            {
                auto e1 = fullEdgeList[compareIndex];
                if((e0.p0 == e1.p0 && e0.p1 == e1.p1) || (e0.p0 == e1.p1 && e0.p1 == e1.p0))
                {
                    sharedEdgeList[sharedCount++] = {e0.p0, e0.p1, e0.index, e1.index};
                    fullEdgeList[compareIndex] = fullEdgeList[--fullEdgeCount];
                    break;
                }
            }
        }

        auto edgeCreationTimerEnd = Win32GetWallClockTime();
        f32 edgeCreationElapsed = Win32GetElapsedSeconds(edgeCreationTimerStart, edgeCreationTimerEnd);
        PrintDebugMsg("Time taken to find delaunay shared edges: %f seconds\n", edgeCreationElapsed);
        auto voronoiStartTime = Win32GetWallClockTime();
        voronoi_edge *voronoiEdgeList = new voronoi_edge[state->triangleCount * 3];
        u32 voronoiEdgeCount = 0;

        for(u32 eIndex = 0; eIndex < sharedCount; ++eIndex)
        {
            shared_edge e = sharedEdgeList[eIndex];
            voronoiEdgeList[voronoiEdgeCount++] = {state->circumCenterList[e.t1], state->circumCenterList[e.t2]};
        }

        triangle_2d *voronoiTriangleList = new triangle_2d[sharedCount * 2];
        u32 voronoiTriangleCount = 0;
        for(u32 eIndex = 0; eIndex < sharedCount; ++eIndex)
        {
            shared_edge e = sharedEdgeList[eIndex];
            voronoiTriangleList[voronoiTriangleCount++] = {{state->circumCenterList[e.t1], state->circumCenterList[e.t2], e.p0}};
            voronoiTriangleList[voronoiTriangleCount++] = {{state->circumCenterList[e.t1], state->circumCenterList[e.t2], e.p1}};
        }

        auto voronoiEndTime = Win32GetWallClockTime();
        f32 voronoiElapsed = Win32GetElapsedSeconds(voronoiStartTime, voronoiEndTime);
        PrintDebugMsg("Time Taken to produce voronoi diagram: %f seconds\n", voronoiElapsed);

        voronoi_state result = {};
        result.edges = voronoiEdgeList;
        result.edgeCount = voronoiEdgeCount;
        result.triangles = voronoiTriangleList;
        result.triangleCount = voronoiTriangleCount;

        delete[] sharedEdgeList;
        delete[] fullEdgeList;

        return result;
    }

    static void UpdateDataBuffers(delaunay_voronoi_render_state *dvrState)
    {
        glBindBuffer(GL_ARRAY_BUFFER, dvrState->delaunayVBOID);
        glBufferData(GL_ARRAY_BUFFER, (GLsizeiptr)(sizeof(triangle_2d) * dvrState->dState.triangleCount), dvrState->dState.triangleList, GL_DYNAMIC_DRAW);

        glBindBuffer(GL_ARRAY_BUFFER, dvrState->voronoiVBOID);
        glBufferData(GL_ARRAY_BUFFER, (GLsizeiptr)(sizeof(voronoi_edge) * dvrState->vState.edgeCount), dvrState->vState.edges, GL_DYNAMIC_DRAW);

        glBindBuffer(GL_ARRAY_BUFFER, dvrState->voronoiFillVBOID);
        glBufferData(GL_ARRAY_BUFFER, (GLsizeiptr)(sizeof(triangle_2d) * dvrState->vState.triangleCount), dvrState->vState.triangles, GL_DYNAMIC_DRAW);

        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }

    // TODO(torgrim): Since voronoi state allocates memory dynamically and we
    // overwrite this memory each time we calculate the diagram we need to
    // free the memory before we allocate new space.
    static void IncrementDelaunayVoronoiAndUpdateBuffers(delaunay_voronoi_render_state *dvrState)
    {
        tbs_assert(dvrState->isInit);

        auto *dState = &dvrState->dState;
        IncrementDelaunay(dState, false);

        delete[] dvrState->vState.edges;
        delete[] dvrState->vState.triangles;
        dvrState->vState.edgeCount = 0;
        dvrState->vState.triangleCount = 0;
        dvrState->vState = CalculateVoronoiFromDelaunay(dState);
        UpdateDataBuffers(dvrState);
    }

    static void CalculateFullDelaunayVoronoiAndUpdateBuffers(delaunay_voronoi_render_state *dvrState)
    {
        tbs_assert(dvrState->isInit);

        auto *dState = &dvrState->dState;
        IncrementDelaunay(dState, true);

        delete[] dvrState->vState.edges;
        delete[] dvrState->vState.triangles;
        dvrState->vState.edgeCount = 0;
        dvrState->vState.triangleCount = 0;
        dvrState->vState = CalculateVoronoiFromDelaunay(dState);
        UpdateDataBuffers(dvrState);
    }

    // TODO(torgrim): Not sure I like this function being inside this file.
    // but ok for now.
    static void RenderDelaunayVoronoiScene(application_context *ctx, delaunay_voronoi_render_state *dvrState)
    {
        if(dvrState->isInit == false || dvrState->dState.currentPointIndex == 0)
        {
            return;
        }

        opengl_shader_info *delaunayShader = GetShaderInfo(ctx, ShaderID_Delaunay);
        opengl_shader_info *voronoiShader = GetShaderInfo(ctx, ShaderID_Voronoi);
        opengl_shader_info *pointShader = GetShaderInfo(ctx, ShaderID_Point);

        f32 aspect = dvrState->pointList.scaleY;

        f32 scaleY = (ctx->clientSize.y - 200.0f);
        f32 scaleX = scaleY / aspect;
        i32 scaleYi = s_cast(i32, scaleY);
        i32 scaleXi = s_cast(i32, (scaleX + 0.5f));

        i32 viewportOffsetX = 100;
        i32 viewportOffsetY = 100;
        i32 viewportSpacing = 100;

        mat4 proj = CreateOrtho((u32)scaleXi, (u32)scaleYi);

        glPointSize(2.0f);
        glLineWidth(1.0f);

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, dvrState->imageTexID);
#if 1
        // TODO(torgrim): All uniform locations should be
        // fetched and checked when initializing shaders.
        glViewport(viewportOffsetX, viewportOffsetY, scaleXi, scaleYi);
        glBindVertexArray(dvrState->delaunayVAOID);
        glUseProgram(delaunayShader->programID);
        glUniform3f(glGetUniformLocation(delaunayShader->programID, "color"), 0.0f, 0.0f, 1.0f);
        glUniform1i(glGetUniformLocation(delaunayShader->programID, "use_color"), 0);
        glUniformMatrix4fv(glGetUniformLocation(delaunayShader->programID, "proj"), 1, GL_FALSE, &(proj.m[0][0]));
        glUniform1f(glGetUniformLocation(delaunayShader->programID, "iTime"), ctx->runtime);
        glUniform1i(glGetUniformLocation(delaunayShader->programID, "tex"), 0);
        glUniform2f(glGetUniformLocation(delaunayShader->programID, "scale"), scaleX, scaleX);
        glUniform2f(glGetUniformLocation(delaunayShader->programID, "offset"), 0.0f, 0.0f);


        //size_t currentVisibleTriangleCount = s_cast(size_t, ctx->runtime / 0.01f);
        size_t currentVisibleTriangleCount = dvrState->dState.triangleCount;

        if(currentVisibleTriangleCount > dvrState->dState.triangleCount)
        {
            currentVisibleTriangleCount = dvrState->dState.triangleCount;
        }

        glDrawArrays(GL_TRIANGLES, 0, (GLsizei)(currentVisibleTriangleCount * 3));
#endif

#if 1
        glViewport(viewportOffsetX + viewportSpacing + scaleXi, viewportOffsetY, scaleXi, scaleYi);
        glBindVertexArray(dvrState->voronoiFillVAOID);
        glUseProgram(voronoiShader->programID);
        glUniform1i(glGetUniformLocation(voronoiShader->programID, "tex"), 0);
        glUniform2f(glGetUniformLocation(voronoiShader->programID, "scale"), scaleX, scaleX);
        glUniform2f(glGetUniformLocation(voronoiShader->programID, "offset"), 0.0f, 0.0f);
        glUniform3f(glGetUniformLocation(voronoiShader->programID, "color"), 1.0f, 0.0f, 1.0f);
        glUniform1i(glGetUniformLocation(voronoiShader->programID, "use_color"), 0);
        glUniformMatrix4fv(glGetUniformLocation(voronoiShader->programID, "proj"), 1, GL_FALSE, &(proj.m[0][0]));
        glUniform1f(glGetUniformLocation(voronoiShader->programID, "iTime"), ctx->runtime);
        glDrawArrays(GL_TRIANGLES, 0, (GLsizei)(dvrState->vState.triangleCount * 3));
#endif

#if 0
        glUniform3f(glGetUniformLocation(triangleShader.programID, "color"), 1.0f, 1.0f, 1.0f);
        glUniform1i(glGetUniformLocation(triangleShader.programID, "use_color"), 1);
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        glDrawArrays(GL_TRIANGLES, 0, (GLsizei)(currentVisibleTriangleCount * 3));
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
#endif

#if 1
        glViewport(viewportOffsetX + (viewportSpacing * 2) + (scaleXi * 2), viewportOffsetY, scaleXi, scaleYi);
        glBindVertexArray(dvrState->pointVAOID);
        glUseProgram(pointShader->programID);
        glUniform1i(glGetUniformLocation(pointShader->programID, "use_buffer_data"), 1);
        glUniform2f(glGetUniformLocation(pointShader->programID, "scale"), scaleX, scaleX);
        glUniform2f(glGetUniformLocation(pointShader->programID, "offset"), 0.0f, 0.0f);
        glUniform3f(glGetUniformLocation(pointShader->programID, "color"), 1.0f, 0.0f, 0.0f);
        glUniformMatrix4fv(glGetUniformLocation(pointShader->programID, "proj"), 1, GL_FALSE, &(proj.m[0][0]));
        //glDrawArrays(GL_POINTS, 0, s_cast(GLsizei, imagePoints.count));
        glDrawArrays(GL_POINTS, 0, s_cast(GLsizei, dvrState->dState.currentPointIndex));
#endif

        glBindTexture(GL_TEXTURE_2D, 0);
        glBindVertexArray(0);

    }
}
