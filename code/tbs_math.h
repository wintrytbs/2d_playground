#ifndef TBS_MATH_H
#define TBS_MATH_H

#include <cmath>

constexpr f32 PI_F = 3.14159265358979323846f;
constexpr f32 TAU_F = 2.0f*PI_F;
constexpr f32 F32_EPSILON = 0.0001f;

// TODO(torgrim): Just collapse to one.
#define mat4_addr(mat) (&(mat.m[0][0]))
#define mat3_addr(mat) (&(mat.m[0][0]))

static inline f32 FloorF32(f32 v)
{
    f32 result = floorf(v);
    return result;
}

static inline f32 CeilF32(f32 v)
{
    f32 result = ceilf(v);
    return result;
}

static inline f32 ClampF32(f32 v, f32 min, f32 max)
{
    f32 result = (v < min) ? min : (v > max) ? max : v;
    return result;
}

static inline i32 ClampI32(i32 v, i32 min, i32 max)
{
    i32 result = (v < min) ? min : (v > max) ? max : v;
    return result;
}

static inline i32 AbsI32(i32 value)
{
    i32 result = abs(value);
    return result;
}

static inline i32 MaxI32(i32 value, i32 max)
{
    i32 result = value >= max ? value : max;
    return result;
}

static inline i32 MinI32(i32 value, i32 min)
{
    i32 result = value <= min ? value : min;
    return result;
}

static inline f32 Cos(f32 v)
{
    f32 result = cosf(v);
    return result;
}

static inline f32 Sin(f32 v)
{
    f32 result = sinf(v);
    return result;
}

static inline f32 ArcCos(f32 v)
{
    f32 result = acosf(v);
    return result;
}

static inline f32 ArcTan2(f32 y, f32 x)
{
    f32 result = atan2f(y, x);
    return result;
}

static inline f32 Abs(f32 v)
{
    f32 result = fabsf(v);
    return result;
}

static inline f32 SquareRoot(f32 v)
{
    f32 result = sqrtf(v);
    return result;
}

static inline f64 SquareRoot(f64 v)
{
    f64 result = sqrt(v);
    return result;
}

static inline bool IsNearlyZero(f32 v)
{
    bool result = (v > -F32_EPSILON && v < F32_EPSILON);
    return result;
}

static inline f32 DegreeToRadian(f32 d)
{
    f32 result = (PI_F/180.0f) * d;
    return result;
}



struct v2i
{
    i32 x;
    i32 y;

    v2i() = default;

    v2i(i32 a, i32 b)
    {
        x = a;
        y = b;
    }
};

static inline v2i operator - (i32 s, v2i a)
{
    v2i result(s - a.x, s - a.y);
    return result;
}

struct v2
{
    f32 x;
    f32 y;

    v2() = default;

    v2(f32 a, f32 b)
    {
        x = a;
        y = b;
    }
};

static inline v2 operator - (v2 a)
{
    v2 result(-a.x, -a.y);
    return result;
}

static inline v2 operator - (v2 a, v2 b)
{
    v2 result(a.x - b.x, a.y - b.y);
    return result;
}

static inline v2 operator / (v2 a, f32 s)
{
    tbs_assert(s > 0);
    v2 result(a.x/s, a.y/s);
    return result;
}

static inline v2 operator * (v2 a, f32 s)
{
    v2 result(a.x * s, a.y * s);
    return result;
}

static inline v2 operator * (f32 s, v2 a)
{
    v2 result(a.x * s, a.y * s);
    return result;
}

static inline v2 operator + (v2 a, v2 b)
{
    v2 result(a.x + b.x, a.y + b.y);
    return result;
}

static inline bool operator == (v2 a, v2 b)
{
    bool result = (a.x == b.x) && (a.y == b.y);
    return result;
}

static inline v2& operator += (v2 &a, v2 b)
{
    a.x += b.x;
    a.y += b.y;
    return a;
}

static inline v2& operator *= (v2 &a, f32 s)
{
    a = a * s;
    return a;
}

static inline v2& operator /= (v2 &a, f32 s)
{
    a = a / s;
    return a;
}

struct v3
{
    f32 x;
    f32 y;
    f32 z;

    v3() = default;

    v3(f32 a, f32 b, f32 c)
    {
        x = a;
        y = b;
        z = c;
    }
};

struct v4
{
    f32 x;
    f32 y;
    f32 z;
    f32 w;

    v4() = default;

    v4(f32 a, f32 b, f32 c, f32 d)
    {
        x = a;
        y = b;
        z = c;
        w = d;
    }
};

struct mat2
{
    f32 m[2][2];

    mat2() = default;

    mat2(f32 m00, f32 m01,
         f32 m10, f32 m11)
    {
        m[0][0] = m00;
        m[1][0] = m01;
        m[0][1] = m10;
        m[1][1] = m11;
    }

    const f32& operator()(i32 i, i32 j) const
    {
        const f32& result = m[j][i];
        return result;
    }

    f32& operator()(i32 i, i32 j)
    {
        f32& result = m[j][i];
        return result;
    }
};

static inline v2 operator * (mat2 m, v2 a)
{
    v2 result = v2(m(0,0) * a.x + m(0,1) * a.y,
                   m(1,0) * a.x + m(1,1) * a.y);
    return result;
}

static inline mat2 operator*(const mat2 &a, const mat2 &b)
{
    mat2 result = mat2(a(0,0) * b(0,0) + a(0,1) * b(1,0),
                       a(0,0) * b(0,1) + a(0,1) * b(1,1),

                       a(1,0) * b(0,0) + a(1,1) * b(1,0),
                       a(1,0) * b(0,1) + a(1,1) * b(1,1));
    return result;
}

struct mat3
{
    f32 m[3][3];

    mat3() = default;

    mat3(f32 m00, f32 m01, f32 m02,
         f32 m10, f32 m11, f32 m12,
         f32 m20, f32 m21, f32 m22)
    {
        m[0][0] = m00;
        m[1][0] = m01;
        m[2][0] = m02;

        m[0][1] = m10;
        m[1][1] = m11;
        m[2][1] = m12;

        m[0][2] = m20;
        m[1][2] = m21;
        m[2][2] = m22;
    }

    const f32& operator()(i32 i, i32 j) const
    {
        const f32 &result = m[j][i];
        return result;
    }

    f32& operator()(i32 i, i32 j)
    {
        f32& result = m[j][i];
        return result;
    }
};

static inline mat3 operator*(const mat3 &a, const mat3 &b)
{
    mat3 result = mat3(a(0,0) * b(0,0) + a(0,1) * b(1,0) + a(0,2) * b(2,0),
                       a(0,0) * b(0,1) + a(0,1) * b(1,1) + a(0,2) * b(2,1),
                       a(0,0) * b(0,2) + a(0,1) * b(1,2) + a(0,2) * b(2,2),

                       a(1,0) * b(0,0) + a(1,1) * b(1,0) + a(1,2) * b(2,0),
                       a(1,0) * b(0,1) + a(1,1) * b(1,1) + a(1,2) * b(2,1),
                       a(1,0) * b(0,2) + a(1,1) * b(1,2) + a(1,2) * b(2,2),

                       a(2,0) * b(0,0) + a(2,1) * b(1,0) + a(2,2) * b(2,0),
                       a(2,0) * b(0,1) + a(2,1) * b(1,1) + a(2,2) * b(2,1),
                       a(2,0) * b(0,2) + a(2,1) * b(1,2) + a(2,2) * b(2,2));
    return result;
}

static inline mat3 operator*=(mat3 &a, mat3 b)
{
    a = a*b;
    return a;
}

static inline v3 operator*(const mat3 &m, const v3 a)
{
    v3 result = v3(m(0,0) * a.x + m(0,1) * a.y + m(0,2) * a.z,
                   m(1,0) * a.x + m(1,1) * a.y + m(1,2) * a.z,
                   m(2,0) * a.x + m(2,1) * a.y + m(2,2) * a.z);
    return result;
}

struct mat4
{
    f32 m[4][4];

    mat4() = default;

    mat4(f32 m00, f32 m01, f32 m02, f32 m03,
         f32 m10, f32 m11, f32 m12, f32 m13,
         f32 m20, f32 m21, f32 m22, f32 m23,
         f32 m30, f32 m31, f32 m32, f32 m33)
    {
        m[0][0] = m00;
        m[1][0] = m01;
        m[2][0] = m02;
        m[3][0] = m03;

        m[0][1] = m10;
        m[1][1] = m11;
        m[2][1] = m12;
        m[3][1] = m13;

        m[0][2] = m20;
        m[1][2] = m21;
        m[2][2] = m22;
        m[3][2] = m23;

        m[0][3] = m30;
        m[1][3] = m31;
        m[2][3] = m32;
        m[3][3] = m33;
    }

    const f32& operator()(i32 i, i32 j) const
    {
        const f32 &result = m[j][i];
        return result;
    }

    f32& operator()(i32 i, i32 j)
    {
        f32& result = m[j][i];
        return result;
    }
};

static inline f32 LengthSq(v2 a)
{
    f32 result = a.x*a.x + a.y*a.y;
    return result;
}

static inline f32 Length(v2 a)
{
    f32 result = LengthSq(a);
    tbs_assert(result > 0);
    result = SquareRoot(result);
    return result;
}

static inline f32 Distance(v2 a, v2 b)
{
    f32 result = Length(a - b);
    return result;
}

static inline v2 Normalize(v2 a)
{
    v2 result = a / Length(a);
    return result;
}

static inline f32 Dot(v2 a, v2 b)
{
    f32 result = (a.x*b.x + a.y*b.y);
    return result;
}

// NOTE: expects b to be normalized
static inline v2 Reflect(v2 a, v2 b)
{
    v2 result = a - (2.0f * Dot(a,b)) * b;
    return result;
}

static inline mat2 Scale2D(f32 scale)
{
    mat2 result(scale, 0.0f,
                0.0f,  scale);

    return result;
}

static inline v2 Hademard(v2 a, v2 b)
{
    v2 result = v2(a.x * b.x, a.y * b.y);
    return result;
}

static inline mat2 Scale2D(f32 scaleX, f32 scaleY)
{
    mat2 result(scaleX, 0.0f,
                0.0f,   scaleY);

    return result;
}

static inline mat2 RotateRad2D(f32 radians)
{
    f32 c = Cos(radians);
    f32 s = Sin(radians);
    mat2 result(c,-s,
                s, c);

    return result;
}

static inline mat2 RotateDeg2D(f32 degrees)
{
    f32 rad = DegreeToRadian(degrees);
    mat2 result = RotateRad2D(rad);
    return result;
}

static inline mat3 Scale2D_w(f32 scale)
{
    mat3 result(scale, 0.0f,  0.0f,
                0.0f,  scale, 0.0f,
                0.0f,  0.0f,  1.0f);

    return result;
}

static inline mat3 Scale2D_w(v2 scale)
{
    mat3 result(scale.x, 0.0f,  0.0f,
                0.0f,  scale.y, 0.0f,
                0.0f,  0.0f,  1.0f);

    return result;
}

static inline mat3 Scale2D_w(f32 scaleX, f32 scaleY)
{
    mat3 result(scaleX, 0.0f,   0.0f,
                0.0f,   scaleY, 0.0f,
                0.0f,   0.0f,   1.0f);

    return result;
}

static inline mat3 RotateRad2D_w(f32 radians)
{
    f32 c = Cos(radians);
    f32 s = Sin(radians);
    mat3 result(c,    -s,    0.0f,
                s,     c,    0.0f,
                0.0f,  0.0f, 1.0f);

    return result;
}

static inline mat3 RotateDeg2D_w(f32 degrees)
{
    f32 rad = DegreeToRadian(degrees);
    mat3 result = RotateRad2D_w(rad);
    return result;
}

static inline mat3 Translate2D_w(f32 x, f32 y)
{
    mat3 result(1.0f, 0.0f, x,
                0.0f, 1.0f, y,
                0.0f, 0.0f, 1.0f);

    return result;
}

static inline mat3 Translate2D_w(v2 a)
{
    mat3 result(1.0f, 0.0f, a.x,
                0.0f, 1.0f, a.y,
                0.0f, 0.0f, 1.0f);

    return result;
}

static inline mat4 CreateIdentity4D()
{
    mat4 result(1.0f, 0.0f, 0.0f, 0.0f,
                0.0f, 1.0f, 0.0f, 0.0f,
                0.0f, 0.0f, 1.0f, 0.0f,
                0.0f, 0.0f, 0.0f, 1.0f);

    return result;
}

static inline mat3 CreateIdentity3D()
{
    mat3 result(1.0f, 0.0f, 0.0f,
                0.0f, 1.0f, 0.0f,
                0.0f, 0.0f, 1.0f);

    return result;
}

static inline mat4 CreateOrtho(u32 w, u32 h)
{
    tbs_assert(w > 0 && h > 0);

    f32 wHalf = 2.0f/s_cast(f32, w);
    f32 hHalf = 2.0f/s_cast(f32, h);
    mat4 result(wHalf,  0.0f,  0.0f, -1.0f,
                0.0f,   hHalf, 0.0f, -1.0f,
                0.0f,   0.0f,  0.0f,  0.0f,
                0.0f,   0.0f,  0.0f,  1.0f);

    return result;
}

static inline mat4 CreateOrthof32(f32 left, f32 right, f32 top, f32 bottom)
{
    mat4 result(2.0f / (right - left),       0.0f,                  0.0f, -(right + left) / (right - left),
                0.0f                       , 2.0f / (top - bottom), 0.0f, -(top + bottom) / (top - bottom),
                0.0f,                        0.0f,                  0.0f,  0.0f,
                0.0f,                        0.0f,                  0.0f,  1.0f);

    return result;
}

static inline mat4 CreateOrtho(u32 left, u32 right, u32 top, u32 bottom)
{
    mat4 result = CreateOrthof32((f32)left, (f32)right, (f32)top, (f32)bottom);
    return result;
}

static inline v2 CircumCenter(v2 a, v2 b, v2 c)
{
    f32 d = 2.0f * ((a.x * (b.y - c.y)) + (b.x * (c.y - a.y)) + (c.x * (a.y - b.y)));
    f32 comA = (a.x * a.x + a.y * a.y);
    f32 comB = (b.x * b.x + b.y * b.y);
    f32 comC = (c.x * c.x + c.y * c.y);

    f32 ux = (comA * (b.y - c.y)) + (comB * (c.y - a.y)) + (comC * (a.y - b.y));
    f32 uy = (comA * (c.x - b.x)) + (comB * (a.x - c.x)) + (comC * (b.x - a.x));

    ux /= d;
    uy /= d;

    return v2(ux, uy);
}

#endif
