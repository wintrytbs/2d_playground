@echo off

set code_dir=%~dp0
set build_rel_dir="../build/"
set lib_rel_dir="../libs/"


pushd %code_dir%

echo Code Directory: %code_dir%
echo Build Directory: %code_dir%%build_rel_dir%

if not exist %build_rel_dir% mkdir %build_rel_dir%
pushd %build_rel_dir%

if exist win32_2d_playground.* del win32_2d_playground.*
if exist gl_function_builder.exe del gl_function_builder.exe

if "%1"=="clang" goto clang_compiler

echo Compiling with MSVC

cl /std:c++14 /nologo /Wall /O2 /Zi %code_dir%gl_function_builder.cpp /link /incremental:no
gl_function_builder.exe > %code_dir%opengl_function_loader.cpp

rem MSVC compiler flags
set msvc_disabled_warn=/wd4820

rem Compiler will instert spectre mitigation for memory load if /Qspecture switch specified
set msvc_disabled_warn=%msvc_disabled_warn% /wd5045

rem NOTE: These we can enable once in a while
rem unreferenced inline function has been removed
set msvc_disabled_warn=%msvc_disabled_warn% /wd4514
rem unreferenced local function has been removed
set msvc_disabled_warn=%msvc_disabled_warn% /wd4505
rem local variable is initialized but not referenced
set msvc_disabled_warn=%msvc_disabled_warn% /wd4189
rem unreferenced formal parameters
set msvc_disabled_warn=%msvc_disabled_warn% /wd4100
rem pointer truncation
set msvc_disabled_warn=%msvc_disabled_warn% /wd4311
rem 32-bit value to 64-bit pointer type
set msvc_disabled_warn=%msvc_disabled_warn% /wd4312
rem conversion from larger type to smaller type
set msvc_disabled_warn=%msvc_disabled_warn% /wd4302
rem function not inlined(since this is in our code, it
rem can be smart to try and fix some of these)
set msvc_disabled_warn=%msvc_disabled_warn% /wd4710
rem selected for automatic inline expansion
set msvc_disabled_warn=%msvc_disabled_warn% /wd4711

set msvc_link_lib=User32.lib opengl32.lib Gdi32.lib

cl /std:c++14 /nologo /WX /Wall /Zi /O2 /EHsc- /I%lib_rel_dir% %msvc_disabled_warn% %code_dir%win32_2d_playground.cpp %msvc_link_lib%

goto compiler_finished

:clang_compiler

echo Compiling with Clang

rem Clang compiler flags
set clang_link_lib=-lUser32.lib -lopengl32.lib -lGdi32.lib
set clang_disabled_warn=-Wno-c++98-compat
set clang_disabled_warn=%clang_disabled_warn% -Wno-c++98-compat-pedantic
set clang_disabled_warn=%clang_disabled_warn% -Wno-float-equal

rem NOTE: These can be enabled once in while
set clang_disabled_warn=%clang_disabled_warn% -Wno-format-security
set clang_disabled_warn=%clang_disabled_warn% -Wno-unused-function

rem NOTE: This is just temporary
set clang_disabled_warn=%clang_disabled_warn% -Wno-old-style-cast
set clang_disabled_warn=%clang_disabled_warn% -Wno-missing-prototypes
set clang_disabled_warn=%clang_disabled_warn% -Wno-zero-as-null-pointer-constant
set clang_disabled_warn=%clang_disabled_warn% -Wno-double-promotion
set clang_disabled_warn=%clang_disabled_warn% -Wno-unused-parameter
set clang_disabled_warn=%clang_disabled_warn% -Wno-unused-variable

clang -std=c++14 -Weverything -O2 -o gl_function_builder.exe %clang_disabled_warn% %code_dir%gl_function_builder.cpp
gl_function_builder.exe > %code_dir%opengl_function_loader.cpp

clang -std=c++14 -Weverything -O0 -Werror %clang_disabled_warn% -o win32_2d_playground.exe -I%lib_rel_dir% %clang_link_lib% %code_dir%win32_2d_playground.cpp

goto compiler_finished

:compiler_finished

popd
popd
