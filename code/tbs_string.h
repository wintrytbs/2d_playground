#ifndef TBS_STRING_H

// TODO(torgrim): Make versions of these where
// the caller includes the length. Especially
// when the caller is providing the buffer as well.
static inline size_t RawStringLength(const char *s)
{
    size_t result = 0;
    while(*s)
    {
        s++;
        result++;
    }

    return result;
}

static inline char *RawStringAllocCopy(const char *src)
{
    size_t l = RawStringLength(src);
    char *result = new char[l+1];
    memcpy(result, src, l);
    result[l] = '\0';
    return result;
}

static inline void RawStringCopy(char *dst, const char *src)
{
    size_t srcLen = RawStringLength(src);
    memcpy(dst, src, srcLen);
    dst[srcLen] = '\0';
}

static size_t RawStringAppend(char *s0, char *s1)
{
    size_t s0Len = RawStringLength(s0);
    size_t s1Len = RawStringLength(s1);
    tbs_assert(s0Len != 0 && s1Len != 0);
    memcpy(s0+s0Len, s1, s1Len);
    size_t newLength = s0Len + s1Len;
    s0[newLength] = '\0';

    return newLength;
}

static size_t RawStringConcat(char *dst, const char *s0, const char *s1)
{
    tbs_assert(dst != s0 && dst != s1);

    size_t s0Len = RawStringLength(s0);
    size_t s1Len = RawStringLength(s1);
    tbs_assert(s0Len > 0 && s1Len > 0);
    memcpy(dst, s0, s0Len);
    memcpy(dst+s0Len, s1, s1Len);
    size_t newLength = s0Len + s1Len;
    dst[newLength] = '\0';

    return newLength;
}

// TODO(torgrim): For these path and file system related
// functions we could probably just use the os' api
// instead of parsing on our own(or C++17 std filesystem)
struct path_offset_result
{
    bool valid;
    size_t offset;
};

static path_offset_result GetParentDirectoryOffset(const char *p)
{
    path_offset_result result = {};

    size_t l = RawStringLength(p);
    tbs_assert(l > 0);

    i32 endOffset = (i32)l-1;
    if(p[endOffset] == '\\')
    {
        endOffset--;
    }

    for(i32 i = endOffset; i >= 0; i--)
    {
        if(p[i] == '\\')
        {
            result.offset = (size_t)i;
            result.valid = true;
            break;
        }
    }

    return result;
}

static const char *GetFileExtension(const char *s, size_t l)
{
    const char *result = nullptr;
    tbs_assert(l > 0);
    size_t i = l - 1;
    while(i > 0 && s[i] != '.')
    {
        i--;
    }

    if(i > 0)
    {
        result = s + i + 1;
    }

    return result;
}

static bool MatchFileExtension(const char *p, const char *ext)
{
    bool result = false;
    size_t l0 = RawStringLength(p);
    size_t l1 = RawStringLength(ext);
    const char *fileExt = GetFileExtension(p, l0);
    if(fileExt)
    {
        i32 i = 0;
        while(fileExt[i] && fileExt[i] == ext[i])
        {
            i++;
        }

        if(fileExt[i] == '\0' && fileExt[i] == ext[i])
        {
            result = true;
        }
    }

    return result;
}

enum path_append_option
{
    PathAppendOption_None,
    PathAppendOption_AddPathSeparator,
};

static void AppendToPath(char *pathDst, char *aStr, path_append_option op)
{
    size_t newLength = RawStringAppend(pathDst, aStr);
    if(op & PathAppendOption_AddPathSeparator)
    {
        pathDst[newLength] = '\\';
        pathDst[newLength+1] = '\0';
    }
}

#define TBS_STRING_H
#endif
