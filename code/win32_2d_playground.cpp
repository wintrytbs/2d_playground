#if !defined(_MSC_VER) && !defined(__clang__)
#error "Unsupported compiler detected"
#endif


#if defined(__clang__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Weverything"
#elif defined(_MSC_VER)
#pragma warning(push, 0)
#pragma warning(disable : 4571 4626 4625 5026 5027 4711 4710)
#endif

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

#include <stdio.h>
#include <stdint.h>
#include <float.h>
#include <assert.h>
#include <unordered_map>
#include <random>

#include <cstdlib>
#include <ctime>

#include <gl/GL.h>
#include <opengl/wglext.h>
#include <opengl/glext.h>

#define STB_IMAGE_IMPLEMENTATION
#include <stb/stb_image.h>

#include <imgui/imgui.cpp>
#include <imgui/imgui_draw.cpp>
#include <imgui/imgui_widgets.cpp>
#include <imgui/imgui_tables.cpp>

// NOTE: Some win32 header define max macro which we don't want
// polluting our code.
#undef max

#if defined(__clang__)
#pragma GCC diagnostic pop
#elif defined(_MSC_VER)
#pragma warning(pop)
#endif


#if defined(__clang__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wold-style-cast"
#elif defined(_MSC_VER)
#pragma warning (push)
#pragma warning (disable: 4191)
#endif
#include "opengl_function_loader.cpp"
#if defined(__clang__)
#pragma GCC diagnostic pop
#elif defined(_MSC_VER)
#pragma warning (pop)
#endif

#include "tbs_base_types.h"
#include "tbs_math.h"
#include "tbs_string.h"

#include "tbs_opengl.h"

#include "win32_2d_playground.h"
#include "win32_opengl.cpp"

#include "tbs_opengl.cpp"
#include "delaunay_voronoi.cpp"
#include "tile_editor_imgui_widgets.cpp"
#include "tile_editor.cpp"
#include "shape_shooter.cpp"

global bool appRunning = true;
global scene_id currentScene = SceneID_None;

static LARGE_INTEGER Win32GetWallClockTime()
{
    LARGE_INTEGER result;
    QueryPerformanceCounter(&result);

    return result;
}

static f32 Win32GetElapsedSeconds(LARGE_INTEGER startTime, LARGE_INTEGER endTime)
{
    LONGLONG elapsedTicks = endTime.QuadPart - startTime.QuadPart;

    LARGE_INTEGER freq;
    QueryPerformanceFrequency(&freq);
    LONGLONG elapsedMicroseconds = elapsedTicks * 1000000;
    elapsedMicroseconds /= freq.QuadPart;

    f32 seconds = s_cast(f32, elapsedMicroseconds) / 1000000.0f;

    return seconds;
}

static FILETIME Win32GetLastWriteTime(const char *filename)
{
    WIN32_FILE_ATTRIBUTE_DATA fileAtt = {};
    // TODO: error handling
    GetFileAttributesEx(filename, GetFileExInfoStandard, &fileAtt);

    FILETIME result = fileAtt.ftLastWriteTime;
    return result;
}

#if defined(__clang__)
#pragma GCC diagnostic push
// NOTE(torgrim): Allow this here since it lines up better with the
// the msdn win32 documentation.
#pragma GCC diagnostic ignored "-Wzero-as-null-pointer-constant"
#endif
static file_data Win32ReadFile(const char *fileName, bool zeroTerminate)
{
    file_data result = {};
    HANDLE fileHandle = CreateFile(fileName,
                                   GENERIC_READ,
                                   FILE_SHARE_READ,
                                   NULL,
                                   OPEN_EXISTING,
                                   NULL,
                                   NULL);

    if(fileHandle == INVALID_HANDLE_VALUE)
    {
        DWORD errorCode = GetLastError();

        // NOTE: access denied. This is most often caused by when we hot reload
        // shader files because the editor where the edit was made is holding
        // a lock on the file for some time. If this is the cause we try sleep and
        // try again.
        if(errorCode == 32)
        {
            Sleep(1000);

            fileHandle = CreateFile(fileName,
                                    GENERIC_READ,
                                    FILE_SHARE_READ,
                                    NULL,
                                    OPEN_EXISTING,
                                    NULL,
                                    NULL);
        }

        if(fileHandle == INVALID_HANDLE_VALUE)
        {
            tbs_assert(false);
            PrintDebugMsg("Win32 ERROR::Invalid File Handle, Error Code: %ld\n", GetLastError());

            return result;
        }
    }

    DWORD fileSize = GetFileSize(fileHandle, NULL);
    size_t allocSize = fileSize;
    if(zeroTerminate)
    {
        allocSize += 1;
    }

    void *file_content = new u8[allocSize];
    DWORD bytesRead;
    BOOL readSuccess = ReadFile(fileHandle, file_content, fileSize, &bytesRead, NULL);
    if(readSuccess == FALSE)
    {
        tbs_assert(false);
        PrintDebugMsg("Could not read file\n");
    }
    else if(zeroTerminate)
    {
        result.data = s_cast(u8*, file_content);
        result.data[bytesRead] = '\0';
        result.size = bytesRead + 1;
    }
    else
    {
        result.data = s_cast(u8*, file_content);
        result.size = bytesRead;
    }

    CloseHandle(fileHandle);

    return result;
}

static inline HINSTANCE Win32GetCurrentInstance()
{
    HINSTANCE result = GetModuleHandleA(NULL);
    return result;
}
#if defined(__clang__)
#pragma GCC diagnostic pop
#endif

static f32 NextRand01(std::mt19937 &mt)
{
    auto r = mt();
    auto m = std::mt19937::max();
    f32 result = (f32)((f64)r / (f64)m);
    return result;
}

static f32 NextRandRangeF32(std::mt19937 &mt, f32 min, f32 max)
{
    assert(min <= max);
    f32 range = max - min;
    f32 r = NextRand01(mt);
    f32 result = min + (r * range);
    return result;
}

static u32 NextRandRangeU32(std::mt19937 &mt, u32 min, u32 max)
{
    tbs_s_assert(sizeof(std::uint_fast32_t) == sizeof(u32), "sizeof(std::uint_fast32_t) != sizeof(u32)");
    tbs_assert(min < max);

    std::uint_fast32_t r = mt();
    u32 dist = max - min;
    u32 v = r % (dist + 1);
    tbs_assert(std::numeric_limits<u32>::max() - min > (dist+1));
    u32 result = min + v;
    return result;
}

static void DEBUG_RenderGUIDrawData(ImDrawData *imguiDD, opengl_shader_info *shader)
{
    i32 guiDrawW = (i32)(imguiDD->DisplaySize.x * imguiDD->FramebufferScale.x);
    i32 guiDrawH = (i32)(imguiDD->DisplaySize.y * imguiDD->FramebufferScale.y);

    tbs_assert(imguiDD->DisplayPos.x == 0 && imguiDD->DisplayPos.y == 0);
    tbs_assert(imguiDD->FramebufferScale.x == 1 && imguiDD->FramebufferScale.y == 1);

    mat4 imguiProj = CreateOrthof32(imguiDD->DisplayPos.x,
                                    imguiDD->DisplayPos.x + imguiDD->DisplaySize.x,
                                    imguiDD->DisplayPos.y,
                                    imguiDD->DisplayPos.y + imguiDD->DisplaySize.y);

    glUseProgram(shader->programID);
    glUniformMatrix4fv(glGetUniformLocation(shader->programID, "proj"), 1, GL_FALSE, mat4_addr(imguiProj));
    glUniform1i(glGetUniformLocation(shader->programID, "tex"), 0);


    glEnable(GL_BLEND);
    glBlendEquation(GL_FUNC_ADD);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDisable(GL_CULL_FACE);
    glDisable(GL_DEPTH_TEST);
    glEnable(GL_SCISSOR_TEST);

    GLuint imguiVAOID;
    GLuint imguiVBOID;
    GLuint imguiEBOID;
    glGenVertexArrays(1, &imguiVAOID);
    glBindVertexArray(imguiVAOID);
    glGenBuffers(1, &imguiVBOID);
    glGenBuffers(1, &imguiEBOID);

    glBindBuffer(GL_ARRAY_BUFFER, imguiVBOID);

    // vertex, uv, color
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);

    glVertexAttribPointer(0, 2, GL_FLOAT,           GL_FALSE, sizeof(ImDrawVert), r_cast(void*, offsetof(ImDrawVert, pos)));
    glVertexAttribPointer(1, 2, GL_FLOAT,           GL_FALSE, sizeof(ImDrawVert), r_cast(void*, offsetof(ImDrawVert, uv)));
    glVertexAttribPointer(2, 4, GL_UNSIGNED_BYTE,   GL_TRUE,  sizeof(ImDrawVert), r_cast(void*, offsetof(ImDrawVert, col)));


    glBindVertexArray(imguiVAOID);
    glBindBuffer(GL_ARRAY_BUFFER, imguiVBOID);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, imguiEBOID);

    for(i32 cmdListIndex = 0; cmdListIndex < imguiDD->CmdListsCount; ++cmdListIndex)
    {
        ImDrawList *cmdList = imguiDD->CmdLists[cmdListIndex];

        glBufferData(GL_ARRAY_BUFFER, s_cast(GLsizeiptr, (size_t)cmdList->VtxBuffer.Size * sizeof(ImDrawVert)), r_cast(GLvoid*, cmdList->VtxBuffer.Data), GL_STREAM_DRAW);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, s_cast(GLsizeiptr, (size_t)cmdList->IdxBuffer.Size * sizeof(ImDrawIdx)), r_cast(GLvoid*, cmdList->IdxBuffer.Data), GL_STREAM_DRAW);

        for(i32 cmdIndex = 0; cmdIndex < cmdList->CmdBuffer.Size; ++cmdIndex)
        {
            ImDrawCmd *cmd = &cmdList->CmdBuffer[cmdIndex];

            ImVec4 clipRect = cmd->ClipRect;

            if(clipRect.x < s_cast(f32, guiDrawW) && clipRect.y < s_cast(f32, guiDrawH) && clipRect.z >= 0.0f && clipRect.w >= 0.0f)
            {
                GLint scissorX = s_cast(GLint, clipRect.x);
                GLint scissorY = s_cast(GLint, s_cast(f32, guiDrawH) - clipRect.w);
                GLint scissorWidth = s_cast(GLsizei, clipRect.z - clipRect.x);
                GLint scissorHeight = s_cast(GLsizei, clipRect.w - clipRect.y);
                glScissor(scissorX, scissorY, scissorWidth, scissorHeight);

                glBindTexture(GL_TEXTURE_2D, s_cast(GLuint, r_cast(uintptr_t, cmd->TextureId)));
                glDrawElements(GL_TRIANGLES, s_cast(GLsizei, cmd->ElemCount), GL_UNSIGNED_SHORT, r_cast(void*, (cmd->IdxOffset * sizeof(ImDrawIdx))));
            }
        }
    }

    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindTexture(GL_TEXTURE_2D, 0);
    glUseProgram(0);

    glDeleteBuffers(1, &imguiVBOID);
    glDeleteBuffers(1, &imguiEBOID);
    glDeleteVertexArrays(1, &imguiVAOID);

    glDisable(GL_BLEND);
    glDisable(GL_SCISSOR_TEST);

}

static LRESULT CALLBACK MainWindowCallback(HWND window, UINT msg, WPARAM wParam, LPARAM lParam)
{
    if(msg == WM_CLOSE)
    {
        appRunning = false;
    }
    else if(msg == WM_CHAR)
    {
        ImGuiIO &io = ImGui::GetIO();
        io.AddInputCharacter((u32)wParam);
    }

    return DefWindowProc(window, msg, wParam, lParam);
}

int main()
{
    HINSTANCE instance = Win32GetCurrentInstance();
    HWND windowHandle = Win32InitOpenGL(instance, MainWindowCallback, 1920, 1080);
    if(windowHandle)
    {

        f32 runtime = 0;
        f32 dt = 1.0f / 144.0f;

        application_context appCtx = {};
        appCtx.dt = dt;
        appCtx.runtime = runtime;
        appCtx.clientSize = v2(1920.0f, 1080.0f);

        // TODO(torgrim): Handle longer paths?
        {
            DWORD l = GetModuleFileNameA(NULL, appCtx.executablePath, MAX_PATH);
            // TODO(torgrim): Handle errors properly
            tbs_assert(l < MAX_PATH);
            auto r = GetParentDirectoryOffset(appCtx.executablePath);
            if(r.valid)
            {
                appCtx.executablePath[r.offset+1] = '\0';
            }
        }

        PrintDebugMsg("Executable Directory: %s\n", appCtx.executablePath);

        RawStringConcat(appCtx.texturePath, appCtx.executablePath, "..\\assets\\textures\\");
        RawStringConcat(appCtx.shaderPath, appCtx.executablePath, "..\\code\\shaders\\");

        {
            char strBuffer[MAX_PATH];
            appCtx.shaderList[ShaderID_Rect] = OpenGLCreateProgramFromSingleFile(GetShaderFullPath(&appCtx, strBuffer, "rect.glsl"));
            appCtx.shaderList[ShaderID_Point] = OpenGLCreateProgramFromSingleFile(GetShaderFullPath(&appCtx, strBuffer, "point.glsl"));
            appCtx.shaderList[ShaderID_Triangle] = OpenGLCreateProgramFromSingleFile(GetShaderFullPath(&appCtx, strBuffer, "triangle.glsl"));
            appCtx.shaderList[ShaderID_Sprite] = OpenGLCreateProgramFromSingleFile(GetShaderFullPath(&appCtx, strBuffer, "sprite.glsl"));
            appCtx.shaderList[ShaderID_Imgui] = OpenGLCreateProgramFromSingleFile(GetShaderFullPath(&appCtx, strBuffer, "imgui.glsl"));
            appCtx.shaderList[ShaderID_Tile] = OpenGLCreateProgramFromSingleFile(GetShaderFullPath(&appCtx, strBuffer, "tile.glsl"));
            appCtx.shaderList[ShaderID_Delaunay] = OpenGLCreateProgramFromSingleFile(GetShaderFullPath(&appCtx, strBuffer, "voronoi.glsl"));
            appCtx.shaderList[ShaderID_Voronoi] = OpenGLCreateProgramFromSingleFile(GetShaderFullPath(&appCtx, strBuffer, "delaunay.glsl"));
            appCtx.shaderList[ShaderID_ShapeShooterPlayer] = OpenGLCreateProgramFromSingleFile(GetShaderFullPath(&appCtx, strBuffer, "shape_shooter_player.glsl"));
            appCtx.shaderList[ShaderID_ShapeShooterPrimitive] = OpenGLCreateProgramFromSingleFile(GetShaderFullPath(&appCtx, strBuffer, "shape_shooter_primitive.glsl"));
            appCtx.shaderList[ShaderID_ShapeShooterSdfCircle] = OpenGLCreateProgramFromSingleFile(GetShaderFullPath(&appCtx, strBuffer, "shape_shooter_sdf_circle.glsl"));
        }

        GLuint imguiVAOID;
        GLuint imguiVBOID;
        GLuint imguiEBOID;
        glGenVertexArrays(1, &imguiVAOID);
        glBindVertexArray(imguiVAOID);
        glGenBuffers(1, &imguiVBOID);
        glGenBuffers(1, &imguiEBOID);

        glBindBuffer(GL_ARRAY_BUFFER, imguiVBOID);

        // vertex, uv, color
        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(1);
        glEnableVertexAttribArray(2);

        glVertexAttribPointer(0, 2, GL_FLOAT,           GL_FALSE, sizeof(ImDrawVert), r_cast(void*, offsetof(ImDrawVert, pos)));
        glVertexAttribPointer(1, 2, GL_FLOAT,           GL_FALSE, sizeof(ImDrawVert), r_cast(void*, offsetof(ImDrawVert, uv)));
        glVertexAttribPointer(2, 4, GL_UNSIGNED_BYTE,   GL_TRUE,  sizeof(ImDrawVert), r_cast(void*, offsetof(ImDrawVert, col)));

        glBindVertexArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

        GLuint MsaaFBO;
        glGenFramebuffers(1, &MsaaFBO);
        glBindFramebuffer(GL_FRAMEBUFFER, MsaaFBO);
        GLuint MsaaFBOTexID;
        glGenTextures(1, &MsaaFBOTexID);
        glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, MsaaFBOTexID);
        glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, 8, GL_RGBA8, 512, 512, false);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D_MULTISAMPLE, MsaaFBOTexID, 0);
        glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, 0);

        {
            GLenum status = glCheckFramebufferStatus(GL_READ_FRAMEBUFFER);
            tbs_assert(status == GL_FRAMEBUFFER_COMPLETE);
            status = glCheckFramebufferStatus(GL_DRAW_FRAMEBUFFER);
            tbs_assert(status == GL_FRAMEBUFFER_COMPLETE);
        }


        glBindFramebuffer(GL_FRAMEBUFFER, 0);


        ImGui::CreateContext();
        ImGuiIO& io = ImGui::GetIO();

        {
            char strBuffer[MAX_PATH];
            RawStringConcat(strBuffer, appCtx.executablePath, "tbs_imgui.ini");
            io.IniFilename = RawStringAllocCopy(strBuffer);
            PrintDebugMsg("ImGui save file: %s\n", io.IniFilename);
        }

        io.ConfigFlags = ImGuiConfigFlags_DockingEnable;
        io.ConfigWindowsResizeFromEdges = true;

        io.KeyMap[ImGuiKey_Tab]         = InputKey_Tab;
        io.KeyMap[ImGuiKey_LeftArrow]   = InputKey_LeftArrow;
        io.KeyMap[ImGuiKey_RightArrow]  = InputKey_RightArrow;
        io.KeyMap[ImGuiKey_UpArrow]     = InputKey_UpArrow;
        io.KeyMap[ImGuiKey_DownArrow]   = InputKey_DownArrow;
        io.KeyMap[ImGuiKey_PageUp]      = InputKey_PageUp;
        io.KeyMap[ImGuiKey_PageDown]    = InputKey_PageDown;
        io.KeyMap[ImGuiKey_Home]        = InputKey_Home;
        io.KeyMap[ImGuiKey_End]         = InputKey_End;
        io.KeyMap[ImGuiKey_Insert]      = InputKey_Insert;
        io.KeyMap[ImGuiKey_Delete]      = InputKey_Delete;
        io.KeyMap[ImGuiKey_Backspace]   = InputKey_Backspace;
        io.KeyMap[ImGuiKey_Space]       = InputKey_Space;
        io.KeyMap[ImGuiKey_Enter]       = InputKey_Enter;
        io.KeyMap[ImGuiKey_Escape]      = InputKey_Escape;
        io.KeyMap[ImGuiKey_A]           = InputKey_A;
        io.KeyMap[ImGuiKey_C]           = InputKey_C;
        io.KeyMap[ImGuiKey_V]           = InputKey_V;
        io.KeyMap[ImGuiKey_X]           = InputKey_X;
        io.KeyMap[ImGuiKey_Y]           = InputKey_Y;
        io.KeyMap[ImGuiKey_Z]           = InputKey_Z;
        io.KeyMap[ImGuiKey_KeyPadEnter] = InputKey_KeyPadEnter;

        tbs_s_assert(sizeof(ImDrawIdx) == 2, "ImDrawIdx was not correct size");

        GLuint imguiFontTexID;
        {
            i32 fw;
            i32 fh;
            u8 *pixels;
            io.Fonts->GetTexDataAsRGBA32(&pixels, &fw, &fh);
            glGenTextures(1, &imguiFontTexID);
            glBindTexture(GL_TEXTURE_2D, imguiFontTexID);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, fw, fh, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);

            io.Fonts->TexID = r_cast(ImTextureID, imguiFontTexID);
            glBindTexture(GL_TEXTURE_2D, 0);
        }


        dv::delaunay_voronoi_render_state dvrState = {};

        //currentScene = SceneID_TileEditor;
        currentScene = SceneID_ShapeShooter;

        tile_map_editor_state *tileEditorState;
        {
            char strBuffer[MAX_PATH];
            tileEditorState = InitTileEditor(32, 32, 16, 16, GetTextureAssetFullPath(&appCtx, strBuffer, "forest_environment_tiles.png"), "Environment");
            tileEditorState->offsetX += 1920.0f * 0.5f;
            tileEditorState->offsetY += 1080.0f * 0.5f;
        }

        // TODO(torgrim): Need to actually check that this extension is available
        // before loading and using it
        wglSwapIntervalEXT(2);

        bool msaa = false;
        input_map inputMap = {};
        for(u32 mIndex = 0; mIndex < MouseButton_Count; mIndex++)
        {
            inputMap.mouseDownDuration[mIndex] = -1.0f;
        }
        for(u32 kIndex = 0; kIndex < array_count(inputMap.keyDownDuration); kIndex++)
        {
            inputMap.keyDownDuration[kIndex] = -1.0f;
        }

        u64 frameID = 0;
        //ShowWindow(windowHandle, SW_MAXIMIZE);
        ShowWindow(windowHandle, SW_SHOW);

        shape_shooter::game_context *gameCtx = new shape_shooter::game_context;
        tbs_assert(gameCtx->isInit == false);
        gameCtx->mode = shape_shooter::GameMode_StartMenu;

        while(appRunning)
        {
            i32 currentFrameTotalVerticesCount = 0;
            LARGE_INTEGER frameStartTime = Win32GetWallClockTime();
            MSG msg;
            inputMap.mouseWheelDelta = 0.0f;

            if(PeekMessageA(&msg, NULL, 0, 0, PM_REMOVE))
            {
                switch(msg.message)
                {
                    case WM_QUIT:
                    {
                        appRunning = false;
                    } break;
                    case WM_KEYDOWN:
                    {
                        u64 keyCode = msg.wParam;
                        switch(keyCode)
                        {
                            case VK_LEFT:
                            {
                                inputMap.keyDown[InputKey_LeftArrow] = true;
                            }  break;
                            case VK_RIGHT:
                            {
                                inputMap.keyDown[InputKey_RightArrow] = true;
                            }  break;
                            case VK_UP:
                            {
                                inputMap.keyDown[InputKey_UpArrow] = true;
                            }  break;
                            case VK_DOWN:
                            {
                                inputMap.keyDown[InputKey_DownArrow] = true;
                            }  break;
                            case VK_BACK:
                            {
                                inputMap.keyDown[InputKey_Backspace] = true;
                            } break;
                            case VK_ESCAPE:
                            {
                                inputMap.keyDown[InputKey_Escape] = true;
                            } break;
                            case VK_ADD:
                            {
                                inputMap.keyDown[InputKey_Plus] = true;
                            } break;
                            case VK_SPACE:
                            {
                                inputMap.keyDown[InputKey_Space] = true;
                            } break;
                            case 'A':
                            {
                                inputMap.keyDown[InputKey_A] = true;
                            } break;
                            case 'S':
                            {
                                inputMap.keyDown[InputKey_S] = true;
                            } break;
                            case 'D':
                            {
                                inputMap.keyDown[InputKey_D] = true;
                            } break;
                            case 'W':
                            {
                                inputMap.keyDown[InputKey_W] = true;
                            } break;
                            case 'P':
                            {
                                inputMap.keyDown[InputKey_P] = true;
                            } break;
                        }

                        if(keyCode == 'M')
                        {
                            msaa = !msaa;
                        }

                        TranslateMessage(&msg);
                        DispatchMessage(&msg);
                    } break;
                    case WM_KEYUP:
                    {
                        u64 keyCode = msg.wParam;
                        switch(keyCode)
                        {
                            case VK_LEFT:
                            {
                                inputMap.keyDown[InputKey_LeftArrow] = false;
                            } break;
                            case VK_RIGHT:
                            {
                                inputMap.keyDown[InputKey_RightArrow] = false;
                            } break;
                            case VK_UP:
                            {
                                inputMap.keyDown[InputKey_UpArrow] = false;
                            } break;
                            case VK_DOWN:
                            {
                                inputMap.keyDown[InputKey_DownArrow] = false;
                            } break;
                            case VK_BACK:
                            {
                                inputMap.keyDown[InputKey_Backspace] = false;
                            } break;
                            case VK_ESCAPE:
                            {
                                inputMap.keyDown[InputKey_Escape] = false;
                            } break;
                            case VK_ADD:
                            {
                                inputMap.keyDown[InputKey_Plus] = false;
                            } break;
                            case VK_SPACE:
                            {
                                inputMap.keyDown[InputKey_Space] = false;
                            } break;
                            case 'A':
                            {
                                inputMap.keyDown[InputKey_A] = false;
                            } break;
                            case 'S':
                            {
                                inputMap.keyDown[InputKey_S] = false;
                            } break;
                            case 'D':
                            {
                                inputMap.keyDown[InputKey_D] = false;
                            } break;
                            case 'W':
                            {
                                inputMap.keyDown[InputKey_W] = false;
                            } break;
                            case 'P':
                            {
                                inputMap.keyDown[InputKey_P] = false;
                            } break;
                        }
                    } break;
                    case WM_LBUTTONDOWN:
                    {
                        inputMap.mouseButtonDown[MouseButton_Left] = true;
                    } break;
                    case WM_LBUTTONUP:
                    {
                        inputMap.mouseButtonDown[MouseButton_Left] = false;
                    } break;
                    case WM_RBUTTONDOWN:
                    {
                        inputMap.mouseButtonDown[MouseButton_Right] = true;
                    } break;
                    case WM_RBUTTONUP:
                    {
                        inputMap.mouseButtonDown[MouseButton_Right] = false;
                    } break;
                    case WM_MBUTTONDOWN:
                    {
                        inputMap.mouseButtonDown[MouseButton_Middle] = true;
                    } break;
                    case WM_MBUTTONUP:
                    {
                        inputMap.mouseButtonDown[MouseButton_Middle] = false;
                    } break;
                    case WM_MOUSEWHEEL:
                    {
                        inputMap.mouseWheelDelta = GET_WHEEL_DELTA_WPARAM(msg.wParam);
                    } break;
                    default:
                    {
                        TranslateMessage(&msg);
                        DispatchMessage(&msg);
                    }
                }
            }

            if(msaa)
            {
                glEnable(GL_MULTISAMPLE);
            }
            else
            {
                glDisable(GL_MULTISAMPLE);
            }

            RECT clientRect;
            GetClientRect(windowHandle, &clientRect);

            POINT cursorPos;
            GetCursorPos(&cursorPos);
            ScreenToClient(windowHandle, &cursorPos);

            inputMap.mousePosPrev = inputMap.mousePos;
            inputMap.mousePos = v2((f32)cursorPos.x, (f32)(clientRect.bottom - cursorPos.y));

            appCtx.clientSize = v2((f32)clientRect.right, (f32)clientRect.bottom);

            // TODO(torgrim): Handle the case where we are minimized or otherwise inactive
            // properly.
            if(appCtx.clientSize.x <= 0 || appCtx.clientSize.y <= 0)
            {
                continue;
            }

            for(u32 mIndex = 0; mIndex < MouseButton_Count; mIndex++)
            {
                if(inputMap.mouseButtonDown[mIndex])
                {
                    if(inputMap.mouseDownDuration[mIndex] < 0.0f)
                    {
                        inputMap.mouseDownDuration[mIndex] = 0.0f;
                    }
                    else
                    {
                        inputMap.mouseDownDuration[mIndex] += dt;
                    }
                }
                else
                {
                    inputMap.mouseDownDuration[mIndex] = -1.0f;
                }
            }

            for(u32 kIndex = 0; kIndex < array_count(inputMap.keyDown); kIndex++)
            {
                if(inputMap.keyDown[kIndex])
                {
                    if(inputMap.keyDownDuration[kIndex] < 0.0f)
                    {
                        inputMap.keyDownDuration[kIndex] = 0.0f;
                    }
                    else
                    {
                        inputMap.keyDownDuration[kIndex] += dt;
                    }
                }
                else
                {
                    inputMap.keyDownDuration[kIndex] = -1.0f;
                }
            }

            io.DisplaySize.x = appCtx.clientSize.x;
            io.DisplaySize.y = appCtx.clientSize.y;
            io.DeltaTime = dt;
            io.MouseDown[0] = inputMap.mouseButtonDown[MouseButton_Left];
            io.MouseDown[1] = inputMap.mouseButtonDown[MouseButton_Right];
            io.MousePos.x = (f32)cursorPos.x;
            io.MousePos.y = (f32)cursorPos.y;

            if(inputMap.mouseDownDuration[MouseButton_Left] == 0.0f)
            {
                inputMap.mouseDownPos = inputMap.mousePos;
            }

            ImGui::NewFrame();

            for(i32 keyIndex = 0; keyIndex < INPUT_KEY_COUNT; keyIndex++)
            {
                io.KeysDown[keyIndex] = inputMap.keyDown[keyIndex];
            }

            if(inputMap.keyDown[InputKey_Escape])
            {
                currentScene = SceneID_PauseMenu;
            }

            glBindFramebuffer(GL_FRAMEBUFFER, MsaaFBO);
            glViewport(0, 0, clientRect.right, clientRect.bottom);

            glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, MsaaFBOTexID);
            glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, 8, GL_RGBA8, clientRect.right, clientRect.bottom, false);
            glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, 0);

            if(currentScene == SceneID_TileEditor)
            {
                glClearColor(0.3f, 0.3f, 0.3f, 1.0f);
                glClear(GL_COLOR_BUFFER_BIT);
                UpdateAndRenderTileEditor(&appCtx, &inputMap, tileEditorState);
                UpdateAndRenderTileEditorGUI(&appCtx, &inputMap, tileEditorState, 0);
            }
            else if(currentScene == SceneID_ShapeShooter)
            {
                glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
                glClear(GL_COLOR_BUFFER_BIT);
                shape_shooter::UpdateAndRender(&appCtx, &inputMap, gameCtx);
            }
            else if(currentScene == SceneID_Delaunay_Voronoi)
            {
                glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
                glClear(GL_COLOR_BUFFER_BIT);
                if(!dvrState.isInit)
                {
                    dvrState = dv::InitDelaunayVoronoiScene(&appCtx);
                    dv::IncrementDelaunayVoronoiAndUpdateBuffers(&dvrState);
                }
                if(inputMap.keyDown[InputKey_Plus])
                {
                    dv::IncrementDelaunayVoronoiAndUpdateBuffers(&dvrState);
                }

                dv::RenderDelaunayVoronoiScene(&appCtx, &dvrState);
            }
            else if(currentScene == SceneID_PauseMenu)
            {
                glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
                glClear(GL_COLOR_BUFFER_BIT);

                ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(32.0f, 32.0f));
                ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 4.0f);
                ImGuiWindowFlags mainMenuFlags = ImGuiWindowFlags_NoMove;
                mainMenuFlags |= ImGuiWindowFlags_NoResize;
                mainMenuFlags |= ImGuiWindowFlags_NoCollapse;
                mainMenuFlags |= ImGuiWindowFlags_NoTitleBar;
                mainMenuFlags |= ImGuiWindowFlags_AlwaysAutoResize;
                mainMenuFlags |= ImGuiWindowFlags_NoSavedSettings;
                ImGui::SetNextWindowSize(ImVec2(300.0f, 0.0f));
                ImGui::SetNextWindowPos(ImVec2((appCtx.clientSize.x / 2.0f) - 150.0f, (appCtx.clientSize.y / 2.0f) - 150.0f));
                ImGui::SetNextWindowBgAlpha(1.0f);
                ImGui::Begin("First Window", nullptr, mainMenuFlags);

                f32 crw = ImGui::GetWindowContentRegionWidth();
                ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(7.0f, 14.0f));
                ImGui::PushStyleVar(ImGuiStyleVar_FrameRounding, 4.0f);
                ImGui::Text("Select Viewer");
                if(ImGui::Button("Tile Editor", ImVec2(crw, 50.0f)))
                {
                    currentScene = SceneID_TileEditor;
                }
                if(ImGui::Button("Delaunay/Voronoi", ImVec2(crw, 50.0f)))
                {
                    currentScene = SceneID_Delaunay_Voronoi;
                }
                if(ImGui::Button("Shape Shooter", ImVec2(crw, 50.0f)))
                {
                    currentScene = SceneID_ShapeShooter;
                }
                ImGui::PopStyleVar(2);
                ImGui::End();

                ImGui::PopStyleVar(2);
            }

            ImGui::EndFrame();

            ImGui::Render();

            ImDrawData *imguiDD = ImGui::GetDrawData();
            DEBUG_RenderGUIDrawData(imguiDD, GetShaderInfo(&appCtx, ShaderID_Imgui));

            glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
            glBindFramebuffer(GL_READ_FRAMEBUFFER, MsaaFBO);
            glDrawBuffer(GL_BACK);

            glViewport(0, 0, clientRect.right, clientRect.bottom);
            glClear(GL_COLOR_BUFFER_BIT);

            glBlitFramebuffer(0, 0, clientRect.right, clientRect.bottom, 0, 0, clientRect.right, clientRect.bottom, GL_COLOR_BUFFER_BIT, GL_NEAREST);

            HDC dc = GetDC(windowHandle);
            SwapBuffers(dc);

            OpenGLCatchError();

            dt = Win32GetElapsedSeconds(frameStartTime, Win32GetWallClockTime());
            runtime += dt;
            if(dt > 1.0f / 30.0f)
            {
                dt = 1.0f / 30.0f;
            }

            appCtx.runtime = runtime;
            appCtx.dt = dt;

            frameID++;
        }
    }

    return 0;
}
