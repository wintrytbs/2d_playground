#ifndef TBS_OPENGL_H
struct opengl_shader_info
{
    GLuint vertShaderID;
    GLuint fragShaderID;
    GLuint programID;

    char *filename;
};

struct opengl_texture_info
{
    GLuint texID;
    i32 width;
    i32 height;
};

constexpr u32 OPENGL_MAX_DEBUG_MSG_SIZE = 512;

global const char *VERT_SHADER_PREFIX = "#define VERT_SHADER\n";
global const char *FRAG_SHADER_PREFIX = "#define FRAG_SHADER\n";
global const char *SHADER_VERSION_PREFIX = "#version 430 core\n";

#define TBS_OPENGL_H
#endif
