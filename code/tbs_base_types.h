#define tbs_assert(cond) assert(cond)
#define tbs_s_assert(cond, message) static_assert(cond, message)
#define array_count(arr) (sizeof(arr) / sizeof((arr)[0]))

typedef uint8_t  u8;
typedef int8_t   i8;
typedef uint16_t u16;
typedef int16_t  i16;
typedef uint32_t u32;
typedef int32_t  i32;
typedef uint64_t u64;
typedef int64_t  i64;

typedef float f32;
typedef double f64;

#define local_persist static
#define global static

#define s_cast(type, var) static_cast<type>(var)
#define r_cast(type, var) reinterpret_cast<type>(var)
