#include "tile_editor.h"

// TODO: Move this to the tile_editor_imgui_widgets file?
static tile_selection ShowTileSetWidget(tile_set *tileSet)
{
    static bool isSelecting = false;
    static v2i mouseDownTile;

    tile_selection result = {};

    ImGuiIO &io = ImGui::GetIO();
    ImVec2 canvasSize = ImGui::GetContentRegionAvail();
    ImVec2 canvasMinPos = ImGui::GetCursorScreenPos();
    ImVec2 canvasMaxPos = ImVec2(canvasMinPos.x + canvasSize.x, canvasMinPos.y + canvasSize.y);
    v2i mouseTilePos((i32)FloorF32((io.MousePos.x - canvasMinPos.x) / 16.0f), (i32)FloorF32((io.MousePos.y - canvasMinPos.y) / 16.0f));

    ImDrawList *windowDL = ImGui::GetWindowDrawList();
    windowDL->AddRectFilled(canvasMinPos, canvasMaxPos, IM_COL32(50, 50, 50, 255));
    windowDL->AddRect(canvasMinPos, canvasMaxPos,  IM_COL32(255, 255, 255, 255));

    ImGui::InvisibleButton("canvas_button", canvasSize, ImGuiButtonFlags_MouseButtonLeft | ImGuiButtonFlags_MouseButtonRight);

    f32 tileSetWidth = (f32)(tileSet->tileSize.x * tileSet->tileCountX);
    f32 tileSetHeight = (f32)(tileSet->tileSize.y * tileSet->tileCountY);
    v2 gridStep = v2((f32)tileSet->tileSize.x, (f32)tileSet->tileSize.y);
    windowDL->PushClipRect(canvasMinPos, canvasMaxPos, true);
    for(f32 x = 0.0f; x <= tileSetWidth; x += gridStep.x)
    {
        windowDL->AddLine(ImVec2(canvasMinPos.x + x, canvasMinPos.y), ImVec2(canvasMinPos.x + x, canvasMinPos.y + tileSetHeight), IM_COL32(200, 200, 200, 40));
    }
    for(f32 y = 0.0f; y <= tileSetHeight; y += gridStep.y)
    {
        windowDL->AddLine(ImVec2(canvasMinPos.x, canvasMinPos.y + y), ImVec2(canvasMinPos.x + tileSetWidth, canvasMinPos.y + y), IM_COL32(200, 200, 200, 40));
    }

    for(i32 y = 0; y < tileSet->tileCountY; y++)
    {
        for(i32 x = 0; x < tileSet->tileCountX; x++)
        {
            i32 actualY = (tileSet->tileCountY - 1) - y;
            i32 tileIndex = tileSet->tileCountX * actualY + x;
            tile_data t = tileSet->tiles[tileIndex];
            f32 minPosX = canvasMinPos.x + (f32)(x * tileSet->tileSize.x);
            f32 maxPosX = canvasMinPos.x + (f32)((x+1) * tileSet->tileSize.x);
            f32 minPosY = canvasMinPos.y + (f32)(y * tileSet->tileSize.y);
            f32 maxPosY = canvasMinPos.y + (f32)((y+1) * tileSet->tileSize.y);
            windowDL->AddImage((ImTextureID)(intptr_t)(tileSet->texInfo.texID),
                                ImVec2(minPosX, minPosY),
                                ImVec2(maxPosX, maxPosY),
                                ImVec2(t.uvMin.x, t.uvMax.y),
                                ImVec2(t.uvMax.x, t.uvMin.y));
        }
    }

    if(isSelecting)
    {
        if(!ImGui::IsMouseDown(ImGuiMouseButton_Left))
        {
            if((mouseTilePos.x >= 0 && mouseTilePos.x < tileSet->tileCountX &&
                mouseTilePos.y >= 0 && mouseTilePos.y < tileSet->tileCountY) ||
               (mouseDownTile.x >=0 && mouseDownTile.x < tileSet->tileCountX &&
                mouseDownTile.y >=0 && mouseDownTile.y < tileSet->tileCountY))
            {
                i32 cTileX = ClampI32(mouseTilePos.x, 0, tileSet->tileCountX-1);
                i32 cTileY = ClampI32(mouseTilePos.y, 0, tileSet->tileCountY-1);
                i32 dTileX = ClampI32(mouseDownTile.x, 0, tileSet->tileCountX-1);
                i32 dTileY = ClampI32(mouseDownTile.y, 0, tileSet->tileCountY-1);
                isSelecting = false;
                v2i minMaxX = (cTileX <= dTileX ? v2i(cTileX, dTileX) : v2i(dTileX, cTileX));
                v2i minMaxY = (cTileY <= dTileY ? v2i(cTileY, dTileY) : v2i(dTileY, cTileY));
                minMaxY = (tileSet->tileCountY - 1) - minMaxY;
                minMaxY = v2i(minMaxY.y, minMaxY.x);
                result.minTile = v2i(minMaxX.x, minMaxY.x);
                result.maxTile = v2i(minMaxX.y, minMaxY.y);
                result.tileSet = tileSet;
            }
        }
    }

    if(ImGui::IsItemHovered() && ImGui::IsMouseClicked(ImGuiMouseButton_Left))
    {
        isSelecting = true;
        mouseDownTile.x = mouseTilePos.x;
        mouseDownTile.y = mouseTilePos.y;
    }

    if(isSelecting)
    {
        if((mouseTilePos.x >= 0 && mouseTilePos.x < tileSet->tileCountX &&
            mouseTilePos.y >= 0 && mouseTilePos.y < tileSet->tileCountY) ||
           (mouseDownTile.x >=0 && mouseDownTile.x < tileSet->tileCountX &&
            mouseDownTile.y >=0 && mouseDownTile.y < tileSet->tileCountY))
        {
            i32 cTileX = ClampI32(mouseTilePos.x, 0, tileSet->tileCountX-1);
            i32 cTileY = ClampI32(mouseTilePos.y, 0, tileSet->tileCountY-1);
            i32 dTileX = ClampI32(mouseDownTile.x, 0, tileSet->tileCountX-1);
            i32 dTileY = ClampI32(mouseDownTile.y, 0, tileSet->tileCountY-1);
            ImVec2 minGridTilePos = canvasMinPos + ImVec2((f32)(dTileX * 16), (f32)(dTileY * 16));
            ImVec2 maxGridTilePos = canvasMinPos + ImVec2((f32)(cTileX * 16), (f32)(cTileY * 16));

            v2 minMaxX = (minGridTilePos.x <= maxGridTilePos.x) ? v2(minGridTilePos.x, maxGridTilePos.x) : v2(maxGridTilePos.x, minGridTilePos.x);
            v2 minMaxY = (minGridTilePos.y <= maxGridTilePos.y) ? v2(minGridTilePos.y, maxGridTilePos.y) : v2(maxGridTilePos.y, minGridTilePos.y);

            minGridTilePos.x = minMaxX.x;
            minGridTilePos.y = minMaxY.x;
            maxGridTilePos.x = minMaxX.y + (f32)tileSet->tileSize.x;
            maxGridTilePos.y = minMaxY.y + (f32)tileSet->tileSize.y;
            windowDL->AddRectFilled(minGridTilePos, maxGridTilePos, IM_COL32(0, 77, 204, 102));
        }
    }
    else if(mouseTilePos.x >= 0 && mouseTilePos.x < tileSet->tileCountX &&
            mouseTilePos.y >= 0 && mouseTilePos.y < tileSet->tileCountY)
    {
        windowDL->AddRectFilled(ImVec2(canvasMinPos.x + (f32)(mouseTilePos.x * tileSet->tileSize.x), canvasMinPos.y + (f32)(mouseTilePos.y * tileSet->tileSize.y)),
                ImVec2(canvasMinPos.x + (f32)((mouseTilePos.x + 1) * tileSet->tileSize.y), canvasMinPos.y + (f32)((mouseTilePos.y + 1) * tileSet->tileSize.y)),
                IM_COL32(255, 255, 255, 64));
    }

    ImGui::PopClipRect();

    return result;
}

static tile_set LoadTileSet(const char *filename, i32 tileWidth, i32 tileHeight)
{
    opengl_texture_info tileSheetTexture = OpenGLCreateTextureWithInfoFromFile(filename);
    glBindTexture(GL_TEXTURE_2D, tileSheetTexture.texID);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glBindTexture(GL_TEXTURE_2D, 0);

    tile_set tileSet = {};
    tileSet.texInfo = tileSheetTexture;
    tileSet.tileSize = v2i(tileWidth, tileHeight);
    tileSet.tileCountX = (i32)FloorF32((f32)tileSet.texInfo.width / (f32)tileWidth);
    tileSet.tileCountY = (i32)FloorF32((f32)tileSet.texInfo.height / (f32)tileHeight);
    tileSet.tiles = new tile_data[(size_t)tileSet.tileCountX * (size_t)tileSet.tileCountY];
    f32 fullSizeX = (f32)tileSet.texInfo.width;
    f32 fullSizeY = (f32)tileSet.texInfo.height;
    f32 uvStepX = (f32)tileWidth / (f32)fullSizeX;
    f32 uvStepY = (f32)tileHeight / (f32)fullSizeY;
    u64 nextID = 1u;
    for(i32 y = 0; y < tileSet.tileCountY; y++)
    {
        for(i32 x = 0; x < tileSet.tileCountX; x++)
        {
            i32 tileIndex = tileSet.tileCountX * y + x;
            tileSet.tiles[tileIndex].uvMin = v2((f32)x * uvStepX, (f32)y * uvStepY);
            tileSet.tiles[tileIndex].uvMax = v2((f32)(x+1) * uvStepX, (f32)(y+1) * uvStepY);
            tileSet.tiles[tileIndex].id = nextID++;
        }
    }

    return tileSet;
}

static tbs_array_g<v2i> CalcTileIntersects(v2i tile0, v2i tile1)
{
    tbs_array_g<v2i> tileIntersect{};

    i32 xDiff = tile1.x - tile0.x;
    i32 yDiff = tile1.y - tile0.y;
    i32 xDiffA = AbsI32(xDiff);
    i32 yDiffA = AbsI32(yDiff);
    if(xDiffA >= 1 || yDiffA >= 1)
    {
        i32 dx;
        i32 dy;
        i32 x;
        i32 y;
        i32 xe;
        i32 ye;
        i32 sx;
        i32 sy;

        if(xDiffA >= yDiffA)
        {
            if(xDiff >= 0)
            {
                dx = xDiff;
                x = tile0.x;
                y = tile0.y;
                xe = tile1.x;
                if(yDiff >= 0)
                {
                    sy = 1;
                    dy = yDiff;
                }
                else
                {
                    sy = -1;
                    dy = -yDiff;
                }
            }
            else
            {
                x = tile1.x;
                y = tile1.y;
                xe = tile0.x;
                dx = xe - x;
                if(yDiff >= 0)
                {
                    sy = -1;
                    dy = yDiff;
                }
                else
                {
                    sy = 1;
                    dy = -yDiff;
                }
            }

            if(dx > 1)
            {
                i32 e = -(dx >> 1);
                while(x <= xe)
                {
                    tileIntersect.Add({x,y});
                    x++;
                    e += dy;
                    if(e >= 0)
                    {
                        y += sy;
                        e -= dx;
                    }
                }
            }
            else if(dx == 1 && dy == 1)
            {
                tileIntersect.Add({x,y});
                tileIntersect.Add({x+1,y+sy});
            }
            else
            {
                tileIntersect.Add({x,y});
                tileIntersect.Add({x+1,y});
            }
        }
        else
        {
            if(yDiff >= 0)
            {
                dy = yDiff;
                x = tile0.x;
                y = tile0.y;
                ye = tile1.y;
                if(xDiff >= 0)
                {
                    sx = 1;
                    dx = xDiff;
                }
                else
                {
                    sx = -1;
                    dx = -xDiff;
                }
            }
            else
            {
                x = tile1.x;
                y = tile1.y;
                ye = tile0.y;
                dy = ye - y;
                if(xDiff >= 0)
                {
                    sx = -1;
                    dx = xDiff;
                }
                else
                {
                    sx = 1;
                    dx = -xDiff;
                }
            }

            if(dy > 1)
            {
                i32 e = -(dy >> 1);
                while(y <= ye)
                {
                    tileIntersect.Add({x,y});
                    y++;
                    e += dx;
                    if(e >= 0)
                    {
                        x += sx;
                        e -= dy;
                    }
                }
            }
            else
            {
                tileIntersect.Add({x,y});
                tileIntersect.Add({x, y+1});
            }
        }
    }
    else
    {
        tileIntersect.Add({tile1.x, tile1.y});
    }

    return tileIntersect;
}

static valid_rectangle_info CalcValidRectangleInfo(v2i origMinTile, v2i origMaxTile, v2i validMinBounds, v2i validMaxBounds)
{
    valid_rectangle_info result = {};

    i32 countX = (origMaxTile.x - origMinTile.x) + 1;
    i32 countY = (origMaxTile.y - origMinTile.y) + 1;

    i32 leftCount = 0;
    i32 rightCount = 0;
    i32 bottomCount = 0;
    i32 topCount = 0;
    if(origMinTile.x < validMinBounds.x)
    {
        leftCount = (validMinBounds.x - origMinTile.x);
        leftCount = MinI32(leftCount, countX);
    }
    if(origMaxTile.x > validMaxBounds.x)
    {
        rightCount = origMaxTile.x - validMaxBounds.x;
        rightCount = MinI32(rightCount, countX);
    }
    if(origMinTile.y < validMinBounds.y)
    {
        bottomCount = validMinBounds.y - origMinTile.y;
        bottomCount = MinI32(bottomCount, countY);
    }
    if(origMaxTile.y > validMaxBounds.y)
    {
        topCount = origMaxTile.y - validMaxBounds.y;
        topCount = MinI32(topCount, countY);
    }

    result.tileCount.x = countX - (leftCount + rightCount);
    result.tileCount.y = countY - (bottomCount + topCount);
    result.minTile.x = origMinTile.x + leftCount;
    result.minTile.y = origMinTile.y + bottomCount;
    result.maxTile.x = origMaxTile.x - rightCount;
    result.maxTile.y = origMaxTile.y - topCount;
    result.leftCount = leftCount;
    result.rightCount = rightCount;
    result.bottomCount = bottomCount;
    result.topCount = topCount;

    return result;
}

static void DrawCmd_AddColoredRectTileSpace(simple_draw_cmd *cmd, v2i minTile, v2i tileCount, v2 tileSize, v4 color)
{
    tbs_assert(tileCount.x > 0 && tileCount.y > 0);
    tbs_assert(tileSize.x > 0 && tileSize.y > 0);

    f32 minPosX = tileSize.x * (f32)minTile.x;
    f32 minPosY = tileSize.y * (f32)minTile.y;
    f32 maxPosX = minPosX + (tileSize.x * (f32)tileCount.x);
    f32 maxPosY = minPosY + (tileSize.y * (f32)tileCount.y);

    cmd->vertices.Add({v2(minPosX, minPosY), {}, color});
    cmd->vertices.Add({v2(maxPosX, minPosY), {}, color});
    cmd->vertices.Add({v2(maxPosX, maxPosY), {}, color});
    cmd->vertices.Add({v2(maxPosX, maxPosY), {}, color});
    cmd->vertices.Add({v2(minPosX, maxPosY), {}, color});
    cmd->vertices.Add({v2(minPosX, minPosY), {}, color});
}


static tile_map_editor_state *InitTileEditor(i32 tileCountX, i32 tileCountY, i32 tileWidth, i32 tileHeight, const char *tileSetToLoadOnStartup = nullptr, const char *tileSetName = nullptr)
{
    tile_map_editor_state *result = new tile_map_editor_state();
    tbs_assert(array_count(result->layerNames) == MAX_LAYER_COUNT);
    tbs_assert(array_count(result->layerNames[0]) == 64);
    result->zoom = 1.0f;
    result->offsetX = 0.0f;
    result->offsetY = 0.0f;
    result->tileCountX = tileCountX;
    result->tileCountY = tileCountY;
    result->tileWidth =  tileWidth;
    result->tileHeight =  tileHeight;


    tile_map_tile *initialLayer = new tile_map_tile[s_cast(size_t, result->tileCountX * result->tileCountY)]();
    result->layerList[0] = initialLayer;
    RawStringCopy(result->layerNames[0], "Layer 0");

    result->layerCount++;

    if(tileSetToLoadOnStartup)
    {
        // TODO: tile set tiles shouldn't necessarely have to match
        // the map tile width and height
        // NOTE: tileset 0 is reserved.
        result->tileSetList[1] = LoadTileSet(tileSetToLoadOnStartup, result->tileWidth, result->tileHeight);
        result->tileSetList[1].id = 1;
        if(tileSetName)
        {
            RawStringCopy(result->tileSetList[1].name, tileSetName);
        }

        result->tileSetCount = 2;
    }
    else
    {
        // NOTE: Tile set at index 0 is the empty/invalid tile set
        result->tileSetCount = 1;
    }

    return result;
}


static void UpdateAndRenderTileEditor(application_context *appCtx, input_map *inputMap, tile_map_editor_state *state)
{
    const v4 validRectColor = v4(0.0f, 0.3f, 0.8f, 0.3f);
    const v4 invalidRectColor = v4(1.0f, 0.0f, 0.0f, 0.3f);

    opengl_shader_info *shader = GetShaderInfo(appCtx, ShaderID_Tile);

    ImGuiIO &io = ImGui::GetIO();
    i32 currentFrameTotalVerticesCount = 0;

    i32 mouseDownTileX;
    i32 mouseDownTileY;
    {
        // TODO: Do prev cursor tile need to be part of the state?
        state->prevCursorTileX = state->cursorTileX;
        state->prevCursorTileY = state->cursorTileY;

        f32 invZoom = 1.0f / state->zoom;
        state->cursorTileX = (i32)FloorF32((((inputMap->mousePos.x - state->offsetX) * invZoom) / 16.0f));
        state->cursorTileY = (i32)FloorF32((((inputMap->mousePos.y - state->offsetY) * invZoom) / 16.0f));

        mouseDownTileX = (i32)FloorF32((((inputMap->mouseDownPos.x - state->offsetX) * invZoom) / 16.0f));
        mouseDownTileY = (i32)FloorF32((((inputMap->mouseDownPos.y - state->offsetY) * invZoom) / 16.0f));
    }

    GLuint tileBuffer;
    GLuint tileVAOID;
    {
        GLint vertLoc = glGetAttribLocation(shader->programID, "in_vert");
        GLint uvLoc = glGetAttribLocation(shader->programID, "in_uv");
        GLint colLoc = glGetAttribLocation(shader->programID, "in_color");
        tbs_assert(vertLoc > -1);
        tbs_assert(colLoc > -1);
        tbs_assert(uvLoc > -1);
        glGenVertexArrays(1, &tileVAOID);
        glBindVertexArray(tileVAOID);
        glGenBuffers(1, &tileBuffer);
        glBindBuffer(GL_ARRAY_BUFFER, tileBuffer);
        glVertexAttribPointer((GLuint)vertLoc, 2, GL_FLOAT, GL_FALSE, sizeof(tile_vertex), (void *)(offsetof(tile_vertex, pos)));
        glVertexAttribPointer((GLuint)uvLoc,   2, GL_FLOAT, GL_FALSE, sizeof(tile_vertex), (void *)(offsetof(tile_vertex, uv)));
        glVertexAttribPointer((GLuint)colLoc,  4, GL_FLOAT, GL_FALSE, sizeof(tile_vertex), (void *)(offsetof(tile_vertex, col)));
        glEnableVertexAttribArray((GLuint)vertLoc);
        glEnableVertexAttribArray((GLuint)uvLoc);
        glEnableVertexAttribArray((GLuint)colLoc);
        glBindVertexArray(0);
    }

    glEnable(GL_BLEND);

    tbs_array_g<simple_draw_cmd> drawCommands;

    {
        v2 tileMapSize = v2((f32)(state->tileCountX * state->tileWidth), (f32)(state->tileCountY * state->tileHeight));
        v4 tileMapCavasColor(0.1f, 0.1f, 0.1f, 1.0f);
        simple_draw_cmd mapBackgroundCmd = {{}, 0, GL_TRIANGLES};
        mapBackgroundCmd.vertices.Add({v2(0.0f,             0.0f),          {}, tileMapCavasColor});
        mapBackgroundCmd.vertices.Add({v2(tileMapSize.x,    0.0f),          {}, tileMapCavasColor});
        mapBackgroundCmd.vertices.Add({v2(tileMapSize.x,    tileMapSize.y), {}, tileMapCavasColor});
        mapBackgroundCmd.vertices.Add({v2(tileMapSize.x,    tileMapSize.y), {}, tileMapCavasColor});
        mapBackgroundCmd.vertices.Add({v2(0.0f,             tileMapSize.y), {}, tileMapCavasColor});
        mapBackgroundCmd.vertices.Add({v2(0.0f,             0.0f),          {}, tileMapCavasColor});

        drawCommands.Add(mapBackgroundCmd);
    }

    {
        simple_draw_cmd *drawCmds = new simple_draw_cmd[state->tileSetCount*state->layerCount]();
        for(size_t setIndex = 0; setIndex < (state->tileSetCount)*(state->layerCount); ++setIndex)
        {
            drawCmds[setIndex] = {{}, state->tileSetList[setIndex%state->tileSetCount].texInfo.texID, GL_TRIANGLES};
        }
        v4 rectColor(1.0f, 1.0f, 1.0f, 1.0f);
        for(i32 tileY = 0; tileY < state->tileCountY; ++tileY)
        {
            for(i32 tileX = 0; tileX < state->tileCountX; ++tileX)
            {
                for(size_t layerIndex = 0; layerIndex < state->layerCount; layerIndex++)
                {
                    if(state->layerHidden[layerIndex] == false)
                    {
                        i32 tileIndex = (state->tileCountX * tileY) + tileX;
                        tile_map_tile mt = state->layerList[layerIndex][tileIndex];
                        if(mt.tileSetID > 0)
                        {
                            tile_set *tileSet = state->tileSetList + mt.tileSetID;
                            simple_draw_cmd *drawCmd = drawCmds + ((state->tileSetCount * layerIndex) + mt.tileSetID);
                            tile_data t = tileSet->tiles[(tileSet->tileCountX * mt.tileY) + mt.tileX];
                            f32 minPosX = (f32)(tileX * tileSet->tileSize.x);
                            f32 minPosY = (f32)(tileY * tileSet->tileSize.y);
                            f32 maxPosX = minPosX + (f32)tileSet->tileSize.x;
                            f32 maxPosY = minPosY + (f32)tileSet->tileSize.y;
                            f32 uvMinX = t.uvMin.x;
                            f32 uvMinY = t.uvMin.y;
                            f32 uvMaxX = t.uvMax.x;
                            f32 uvMaxY = t.uvMax.y;

                            drawCmd->vertices.Add({v2(minPosX, minPosY), v2(uvMinX, uvMinY), rectColor});
                            drawCmd->vertices.Add({v2(maxPosX, minPosY), v2(uvMaxX, uvMinY), rectColor});
                            drawCmd->vertices.Add({v2(maxPosX, maxPosY), v2(uvMaxX, uvMaxY), rectColor});
                            drawCmd->vertices.Add({v2(maxPosX, maxPosY), v2(uvMaxX, uvMaxY), rectColor});
                            drawCmd->vertices.Add({v2(minPosX, maxPosY), v2(uvMinX, uvMaxY), rectColor});
                            drawCmd->vertices.Add({v2(minPosX, minPosY), v2(uvMinX, uvMinY), rectColor});
                        }
                    }
                }
            }
        }

        for(size_t dIndex = 0; dIndex < state->tileSetCount*state->layerCount; ++dIndex)
        {
            if(drawCmds[dIndex].vertices.count > 0)
            {
                drawCommands.Add(drawCmds[dIndex]);
            }
        }

        delete[] drawCmds;
    }

    {
        v2 stepSize = v2((f32)state->tileWidth, (f32)state->tileHeight);
        v2 tileMapSize = v2((f32)(state->tileCountX * state->tileWidth), (f32)(state->tileCountY * state->tileHeight));

        if(!io.WantCaptureMouse)
        {
            if(inputMap->mouseDownDuration[MouseButton_Middle] > 0.0f)
            {
                state->offsetX += inputMap->mousePos.x - inputMap->mousePosPrev.x;
                state->offsetY += inputMap->mousePos.y - inputMap->mousePosPrev.y;
            }

            if(inputMap->mouseWheelDelta != 0.0f)
            {
                state->zoom += inputMap->mouseWheelDelta * 0.0005f;
                state->zoom = ClampF32(state->zoom, 0.01f, 10.0f);
            }
        }

        // NOTE(torgrim): Grid drawing
        v4 lineColor = v4(0.5f, 0.5f, 0.5f, 0.2f);
        simple_draw_cmd gridLineCmd = {{}, 0, GL_LINES};
        for(f32 x = 0.0f; x < tileMapSize.x; x += stepSize.x)
        {
            gridLineCmd.vertices.Add({v2(x, 0.0f), {}, lineColor});
            gridLineCmd.vertices.Add({v2(x, tileMapSize.y), {}, lineColor});
        }
        for(f32 y = 0.0f; y < tileMapSize.y; y += stepSize.y)
        {
            gridLineCmd.vertices.Add({v2(0.0f, y), {}, lineColor});
            gridLineCmd.vertices.Add({v2(tileMapSize.x, y), {}, lineColor});
        }

        drawCommands.Add(gridLineCmd);
    }

    if(!io.WantCaptureMouse)
    {
        if(!state->hasSelectionItem)
        {

            bool isInsideMapBounds = false;
            bool wasInsideMapBounds = false;
            if(state->cursorTileX >= 0 && state->cursorTileX < state->tileCountX &&
               state->cursorTileY >= 0 && state->cursorTileY < state->tileCountY)
            {
                isInsideMapBounds = true;
            }
            if(state->prevCursorTileX >= 0 && state->prevCursorTileX < state->tileCountX &&
               state->prevCursorTileY >= 0 && state->prevCursorTileY < state->tileCountY)
            {
                wasInsideMapBounds = true;
            }


            if(state->currentEditTool == EditTool_Stamp)
            {
                {
                    simple_draw_cmd drawCmd = {{}, 0, GL_TRIANGLES};
                    f32 minPosX = (f32)(state->cursorTileX * 16);
                    f32 minPosY = (f32)(state->cursorTileY * 16);
                    f32 maxPosX = (f32)((state->cursorTileX + 1) * 16);
                    f32 maxPosY = (f32)((state->cursorTileY + 1) * 16);
                    v4 rectColor(1.0f, 1.0f, 1.0f, 0.5f);

                    if(!isInsideMapBounds)
                    {
                        rectColor = v4(1.0f, 0.0f, 0.0f, 0.5f);
                    }

                    drawCmd.vertices.Add({v2(minPosX, minPosY), {}, rectColor});
                    drawCmd.vertices.Add({v2(maxPosX, minPosY), {}, rectColor});
                    drawCmd.vertices.Add({v2(maxPosX, maxPosY), {}, rectColor});
                    drawCmd.vertices.Add({v2(maxPosX, maxPosY), {}, rectColor});
                    drawCmd.vertices.Add({v2(minPosX, maxPosY), {}, rectColor});
                    drawCmd.vertices.Add({v2(minPosX, minPosY), {}, rectColor});

                    drawCommands.Add(drawCmd);
                }

                if(inputMap->mouseDownDuration[MouseButton_Left] > 0)
                {
                    tbs_array_g<v2i> tileIntersect = CalcTileIntersects({mouseDownTileX, mouseDownTileY}, {state->cursorTileX, state->cursorTileY});
                    simple_draw_cmd markedCmd = {{}, 0, GL_TRIANGLES};
                    for(i32 i = 0; i < tileIntersect.count; i++)
                    {
                        v2i markedTile = tileIntersect.data[i];
                        f32 minPosX = (f32)(markedTile.x * 16);
                        f32 minPosY = (f32)(markedTile.y * 16);
                        f32 maxPosX = (f32)((markedTile.x + 1) * 16);
                        f32 maxPosY = (f32)((markedTile.y + 1) * 16);
                        v4 rectColor = v4(1.0f, 0.0f, 1.0f, 0.5f);

                        markedCmd.vertices.Add({v2(minPosX, minPosY), {}, rectColor});
                        markedCmd.vertices.Add({v2(maxPosX, minPosY), {}, rectColor});
                        markedCmd.vertices.Add({v2(maxPosX, maxPosY), {}, rectColor});
                        markedCmd.vertices.Add({v2(maxPosX, maxPosY), {}, rectColor});
                        markedCmd.vertices.Add({v2(minPosX, maxPosY), {}, rectColor});
                        markedCmd.vertices.Add({v2(minPosX, minPosY), {}, rectColor});
                    }
                    if(markedCmd.vertices.count > 0)
                    {
                        drawCommands.Add(markedCmd);
                    }
                }
            }
            else if(state->currentEditTool == EditTool_Eraser)
            {
                if((isInsideMapBounds || wasInsideMapBounds) && inputMap->mouseDownDuration[MouseButton_Left] >= 0.0f)
                {
                    tbs_array_g<v2i> tileIntersect = CalcTileIntersects({state->prevCursorTileX, state->prevCursorTileY}, {state->cursorTileX, state->cursorTileY});
                    for(i32 i = 0; i < tileIntersect.count; i++)
                    {
                        v2i tilePos = tileIntersect.data[i];
                        if(tilePos.x >= 0 && tilePos.x < state->tileCountX &&
                           tilePos.y >= 0 && tilePos.y < state->tileCountY)
                        {
                            i32 tileIndex = state->tileCountX * tilePos.y + tilePos.x;
                            state->layerList[state->activeLayer][tileIndex] = {};
                        }
                    }

                    tileIntersect.Free();
                }
            }
            else if(state->currentEditTool == EditTool_RectangleErase)
            {
                static bool isSelecting = false;
                if(isSelecting && inputMap->mouseDownDuration[MouseButton_Right] == 0)
                {
                    isSelecting = false;
                }
                if(isSelecting)
                {
                    if(inputMap->mouseDownDuration[MouseButton_Right] == 0)
                    {
                        isSelecting = false;
                    }
                    else if(inputMap->mouseDownDuration[MouseButton_Left] < 0)
                    {
                        isSelecting = false;
                        i32 minTileX = MinI32(state->cursorTileX, mouseDownTileX);
                        i32 minTileY = MinI32(state->cursorTileY, mouseDownTileY);
                        i32 maxTileX = MaxI32(state->cursorTileX, mouseDownTileX);
                        i32 maxTileY = MaxI32(state->cursorTileY, mouseDownTileY);
                        valid_rectangle_info validRectInfo = CalcValidRectangleInfo(v2i(minTileX, minTileY), v2i(maxTileX, maxTileY), v2i(0,0), v2i(state->tileCountX-1, state->tileCountY-1));

                        for(i32 tileY = 0; tileY < validRectInfo.tileCount.y; ++tileY)
                        {
                            for(i32 tileX = 0; tileX < validRectInfo.tileCount.x; ++tileX)
                            {
                                i32 x = validRectInfo.minTile.x + tileX;
                                i32 y = validRectInfo.minTile.y + tileY;
                                tbs_assert(x >= 0 && x < state->tileCountX);
                                tbs_assert(y >= 0 && y < state->tileCountY);
                                i32 tileIndex = state->tileCountX * y + x;
                                state->layerList[state->activeLayer][tileIndex] = {};
                            }
                        }
                    }
                }

                if(inputMap->mouseDownDuration[MouseButton_Left] == 0)
                {
                    isSelecting = true;
                }
                else if(isSelecting && inputMap->mouseDownDuration[MouseButton_Left] > 0)
                {
                    i32 minTileX = MinI32(state->cursorTileX, mouseDownTileX);
                    i32 minTileY = MinI32(state->cursorTileY, mouseDownTileY);
                    i32 maxTileX = MaxI32(state->cursorTileX, mouseDownTileX);
                    i32 maxTileY = MaxI32(state->cursorTileY, mouseDownTileY);

                    i32 countX = (maxTileX - minTileX) + 1;
                    i32 countY = (maxTileY - minTileY) + 1;

                    valid_rectangle_info validRect = CalcValidRectangleInfo(v2i(minTileX, minTileY), v2i(maxTileX, maxTileY), v2i(0,0), v2i(state->tileCountX-1, state->tileCountY-1));

                    f32 tileWidth = 16.0f;
                    f32 tileHeight = 16.0f;

                    simple_draw_cmd dc = {{}, 0, GL_TRIANGLES};
                    if(validRect.tileCount.x > 0 && validRect.tileCount.y > 0)
                    {
                        v2 tileSize(tileWidth, tileHeight);
                        DrawCmd_AddColoredRectTileSpace(&dc, validRect.minTile, validRect.tileCount, tileSize, validRectColor);
                    }

                    if(validRect.leftCount > 0 && validRect.tileCount.y > 0)
                    {
                        v2i minTile(minTileX, validRect.minTile.y);
                        v2i tileCount(validRect.leftCount, validRect.tileCount.y);
                        v2 tileSize(tileWidth, tileHeight);
                        DrawCmd_AddColoredRectTileSpace(&dc, minTile, tileCount, tileSize, invalidRectColor);
                    }

                    if(validRect.rightCount > 0 && validRect.tileCount.y > 0)
                    {
                        v2i minTile(minTileX + (countX - validRect.rightCount), validRect.minTile.y);
                        v2i tileCount(validRect.rightCount, validRect.tileCount.y);
                        v2 tileSize(tileWidth, tileHeight);
                        DrawCmd_AddColoredRectTileSpace(&dc, minTile, tileCount, tileSize, invalidRectColor);
                    }
                    if(validRect.bottomCount > 0)
                    {
                        v2i minTile(minTileX, minTileY);
                        v2i tileCount(countX, validRect.bottomCount);
                        v2 tileSize(tileWidth, tileHeight);
                        DrawCmd_AddColoredRectTileSpace(&dc, minTile, tileCount, tileSize, invalidRectColor);
                    }
                    if(validRect.topCount > 0)
                    {
                        v2i minTile(minTileX, minTileY + (countY - validRect.topCount));
                        v2i tileCount(countX, validRect.topCount);
                        v2 tileSize(tileWidth, tileHeight);
                        DrawCmd_AddColoredRectTileSpace(&dc, minTile, tileCount, tileSize, invalidRectColor);
                    }

                    if(dc.vertices.count > 0)
                    {
                        drawCommands.Add(dc);
                    }

                }
            }
        }
        else
        {
            bool isInsideMapBounds = false;
            bool wasInsideMapBounds = false;
            if(state->cursorTileX >= 0 && state->cursorTileX < state->tileCountX &&
               state->cursorTileY >= 0 && state->cursorTileY < state->tileCountY)
            {
                isInsideMapBounds = true;
            }
            if(state->prevCursorTileX >= 0 && state->prevCursorTileX < state->tileCountX &&
               state->prevCursorTileY >= 0 && state->prevCursorTileY < state->tileCountY)
            {
                wasInsideMapBounds = true;
            }

            tile_set *selectedTileSet = state->currentSelection.tileSet;
            tbs_assert(selectedTileSet->id > 0);
            f32 tileWidth = (f32)selectedTileSet->tileSize.x;
            f32 tileHeight = (f32)selectedTileSet->tileSize.y;

            i32 tileSelectionCountX = (state->currentSelection.maxTile.x - state->currentSelection.minTile.x) + 1;
            i32 tileSelectionCountY = (state->currentSelection.maxTile.y - state->currentSelection.minTile.y) + 1;

            if(state->currentEditTool == EditTool_Stamp)
            {
                if(inputMap->mouseDownDuration[MouseButton_Right] == 0.0f)
                {
                    state->hasSelectionItem = false;
                    state->currentSelection = {};
                }
                else
                {
                    i32 minTileX = state->cursorTileX - tileSelectionCountX / 2;
                    i32 minTileY = state->cursorTileY - tileSelectionCountY / 2;
                    i32 maxTileX = minTileX + (tileSelectionCountX-1);
                    i32 maxTileY = minTileY + (tileSelectionCountY-1);

                    f32 minPosX = (f32)(state->cursorTileX * 16);
                    f32 minPosY = (f32)(state->cursorTileY * 16);
                    minPosX -= (f32)((tileSelectionCountX / 2) * 16);
                    minPosY -= (f32)((tileSelectionCountY / 2) * 16);

                    valid_rectangle_info validRect = CalcValidRectangleInfo(v2i(minTileX, minTileY), v2i(maxTileX, maxTileY), v2i(0,0), v2i(state->tileCountX-1, state->tileCountY-1));

                    if(validRect.tileCount.x > 0 && validRect.tileCount.y > 0 && inputMap->mouseDownDuration[MouseButton_Left] >= 0.0f)
                    {
                        for(i32 tileY = 0; tileY < validRect.tileCount.y; ++tileY)
                        {
                            for(i32 tileX = 0; tileX < validRect.tileCount.x; ++tileX)
                            {
                                i32 setX = state->currentSelection.minTile.x + validRect.leftCount + tileX;
                                i32 setY = state->currentSelection.minTile.y + validRect.bottomCount + tileY;
                                tbs_assert(setX >= 0 && setX <= selectedTileSet->tileCountX);
                                tbs_assert(setY >= 0 && setY <= selectedTileSet->tileCountY);
                                i32 srcIndex = selectedTileSet->tileCountX * setY + setX;
                                tile_data t = selectedTileSet->tiles[srcIndex];
                                i32 x = validRect.minTile.x + tileX;
                                i32 y = validRect.minTile.y + tileY;
                                tbs_assert(x >= 0 && x < state->tileCountX);
                                tbs_assert(y >= 0 && y < state->tileCountY);
                                i32 dstIndex = state->tileCountX * y + x;
                                state->layerList[state->activeLayer][dstIndex] = {selectedTileSet->id, setX, setY};
                            }
                        }
                    }

                    v4 rectColor(1.0f, 1.0f, 1.0f, 1.0f);
                    simple_draw_cmd drawCmd = {{}, state->currentSelection.tileSet->texInfo.texID, GL_TRIANGLES};
                    f32 tileOffsetX = minPosX;
                    f32 tileOffsetY = minPosY;
                    for(i32 tileY = 0; tileY < tileSelectionCountY; ++tileY)
                    {
                        tileOffsetX = minPosX;
                        for(i32 tileX = 0; tileX < tileSelectionCountX; ++tileX)
                        {
                            i32 tileIndex = selectedTileSet->tileCountX * (state->currentSelection.minTile.y + tileY) + (state->currentSelection.minTile.x + tileX);
                            tile_data t = selectedTileSet->tiles[tileIndex];
                            f32 maxPosX = tileOffsetX + tileWidth;
                            f32 maxPosY = tileOffsetY + tileHeight;
                            f32 uvMinX = t.uvMin.x;
                            f32 uvMinY = t.uvMin.y;
                            f32 uvMaxX = t.uvMax.x;
                            f32 uvMaxY = t.uvMax.y;

                            drawCmd.vertices.Add({v2(tileOffsetX,   tileOffsetY), v2(uvMinX, uvMinY), rectColor});
                            drawCmd.vertices.Add({v2(maxPosX,       tileOffsetY), v2(uvMaxX, uvMinY), rectColor});
                            drawCmd.vertices.Add({v2(maxPosX,       maxPosY),     v2(uvMaxX, uvMaxY), rectColor});
                            drawCmd.vertices.Add({v2(maxPosX,       maxPosY),     v2(uvMaxX, uvMaxY), rectColor});
                            drawCmd.vertices.Add({v2(tileOffsetX,   maxPosY),     v2(uvMinX, uvMaxY), rectColor});
                            drawCmd.vertices.Add({v2(tileOffsetX,   tileOffsetY), v2(uvMinX, uvMinY), rectColor});

                            tileOffsetX += tileWidth;
                        }

                        tileOffsetY += tileHeight;
                    }

                    if(drawCmd.vertices.count > 0)
                    {
                        drawCommands.Add(drawCmd);
                    }


                    drawCmd = {{}, 0, GL_TRIANGLES};

                    // NOTE: Drawing selection rects. First is showing
                    // which selected area is inside the map area
                    // the potential 4 others show which area
                    // is outside of the map.
                    if(validRect.tileCount.x > 0 && validRect.tileCount.y > 0)
                    {
                        v2 tileSize(tileWidth, tileHeight);
                        DrawCmd_AddColoredRectTileSpace(&drawCmd, validRect.minTile, validRect.tileCount, tileSize, validRectColor);
                    }

                    if(validRect.leftCount > 0 && validRect.tileCount.y > 0)
                    {
                        v2i minTile(minTileX, validRect.minTile.y);
                        v2i tileCount(validRect.leftCount, validRect.tileCount.y);
                        v2 tileSize(tileWidth, tileHeight);
                        DrawCmd_AddColoredRectTileSpace(&drawCmd, minTile, tileCount, tileSize, invalidRectColor);
                    }

                    if(validRect.rightCount > 0 && validRect.tileCount.y > 0)
                    {
                        v2i minTile(minTileX + (tileSelectionCountX - validRect.rightCount), validRect.minTile.y);
                        v2i tileCount(validRect.rightCount, validRect.tileCount.y);
                        v2 tileSize(tileWidth, tileHeight);
                        DrawCmd_AddColoredRectTileSpace(&drawCmd, minTile, tileCount, tileSize, invalidRectColor);
                    }
                    if(validRect.bottomCount > 0)
                    {
                        v2i minTile(minTileX, minTileY);
                        v2i tileCount(tileSelectionCountX, validRect.bottomCount);
                        v2 tileSize(tileWidth, tileHeight);
                        DrawCmd_AddColoredRectTileSpace(&drawCmd, minTile, tileCount, tileSize, invalidRectColor);
                    }
                    if(validRect.topCount > 0)
                    {
                        v2i minTile(minTileX, minTileY + (tileSelectionCountY - validRect.topCount));
                        v2i tileCount(tileSelectionCountX, validRect.topCount);
                        v2 tileSize(tileWidth, tileHeight);
                        DrawCmd_AddColoredRectTileSpace(&drawCmd, minTile, tileCount, tileSize, invalidRectColor);
                    }

                    if(drawCmd.vertices.count > 0)
                    {
                        drawCommands.Add(drawCmd);
                    }
                }
            }
            else if(state->currentEditTool == EditTool_RectangleFill)
            {
                // TODO(torgrim): Should this fill method actually work
                // such that the texturing starts at the min tile instead
                // of the valid min tile?
                static bool isSelecting = false;
                if(isSelecting)
                {
                    i32 minTileX = MinI32(state->cursorTileX, mouseDownTileX);
                    i32 minTileY = MinI32(state->cursorTileY, mouseDownTileY);
                    i32 maxTileX = MaxI32(state->cursorTileX, mouseDownTileX);
                    i32 maxTileY = MaxI32(state->cursorTileY, mouseDownTileY);

                    i32 countX = (maxTileX - minTileX) + 1;
                    i32 countY = (maxTileY - minTileY) + 1;

                    valid_rectangle_info validRect = CalcValidRectangleInfo(v2i(minTileX, minTileY), v2i(maxTileX, maxTileY), v2i(0,0), v2i(state->tileCountX-1, state->tileCountY-1));

                    if(inputMap->mouseDownDuration[MouseButton_Right] == 0.0f)
                    {
                        isSelecting = false;
                    }
                    else if(inputMap->mouseDownDuration[MouseButton_Left] < 0.0f)
                    {
                        isSelecting = false;
                        if(validRect.tileCount.x > 0 && validRect.tileCount.y > 0)
                        {
                            for(i32 tileY = 0; tileY < validRect.tileCount.y; ++tileY)
                            {
                                for(i32 tileX = 0; tileX < validRect.tileCount.x; ++tileX)
                                {
                                    i32 setX = state->currentSelection.minTile.x + (tileX % tileSelectionCountX);
                                    i32 setY = state->currentSelection.minTile.y + (tileY % tileSelectionCountY);
                                    tbs_assert(setX >= 0 && setX <= selectedTileSet->tileCountX);
                                    tbs_assert(setY >= 0 && setY <= selectedTileSet->tileCountY);
                                    i32 srcIndex = selectedTileSet->tileCountX * setY + setX;
                                    tile_data t = selectedTileSet->tiles[srcIndex];
                                    i32 x = validRect.minTile.x + tileX;
                                    i32 y = validRect.minTile.y + tileY;
                                    tbs_assert(x >= 0 && x < state->tileCountX);
                                    tbs_assert(y >= 0 && y < state->tileCountY);
                                    i32 dstIndex = state->tileCountX * y + x;
                                    state->layerList[state->activeLayer][dstIndex] = {selectedTileSet->id, setX, setY};
                                }
                            }
                        }
                    }
                    else
                    {
                        if(validRect.tileCount.x > 0 && validRect.tileCount.y > 0)
                        {
                            simple_draw_cmd dc = {{}, selectedTileSet->texInfo.texID, GL_TRIANGLES};
                            v4 color = v4(1.0f, 1.0f, 1.0f, 1.0f);
                            for(i32 tileY = 0; tileY < validRect.tileCount.y; ++tileY)
                            {
                                for(i32 tileX = 0; tileX < validRect.tileCount.x; ++tileX)
                                {
                                    i32 setX = state->currentSelection.minTile.x + (tileX % tileSelectionCountX);
                                    i32 setY = state->currentSelection.minTile.y + (tileY % tileSelectionCountY);
                                    tbs_assert(setX >= 0 && setX <= selectedTileSet->tileCountX);
                                    tbs_assert(setY >= 0 && setY <= selectedTileSet->tileCountY);
                                    i32 srcIndex = selectedTileSet->tileCountX * setY + setX;
                                    tile_data t = selectedTileSet->tiles[srcIndex];
                                    i32 x = validRect.minTile.x + tileX;
                                    i32 y = validRect.minTile.y + tileY;
                                    tbs_assert(x >= 0 && x < state->tileCountX);
                                    tbs_assert(y >= 0 && y < state->tileCountY);
                                    f32 minPosX = tileWidth * (f32)x;
                                    f32 minPosY = tileHeight * (f32)y;
                                    f32 maxPosX = minPosX + tileWidth;
                                    f32 maxPosY = minPosY + tileHeight;

                                    dc.vertices.Add({v2(minPosX, minPosY), {t.uvMin.x, t.uvMin.y}, color});
                                    dc.vertices.Add({v2(maxPosX, minPosY), {t.uvMax.x, t.uvMin.y}, color});
                                    dc.vertices.Add({v2(maxPosX, maxPosY), {t.uvMax.x, t.uvMax.y}, color});
                                    dc.vertices.Add({v2(maxPosX, maxPosY), {t.uvMax.x, t.uvMax.y}, color});
                                    dc.vertices.Add({v2(minPosX, maxPosY), {t.uvMin.x, t.uvMax.y}, color});
                                    dc.vertices.Add({v2(minPosX, minPosY), {t.uvMin.x, t.uvMin.y}, color});
                                }
                            }

                            drawCommands.Add(dc);
                        }

                        simple_draw_cmd dc = {{}, 0, GL_TRIANGLES};
                        if(validRect.tileCount.x > 0 && validRect.tileCount.y > 0)
                        {
                            v2 tileSize(tileWidth, tileHeight);
                            DrawCmd_AddColoredRectTileSpace(&dc, validRect.minTile, validRect.tileCount, tileSize, validRectColor);
                        }

                        if(validRect.leftCount > 0 && validRect.tileCount.y > 0)
                        {
                            v2i minTile(minTileX, validRect.minTile.y);
                            v2i tileCount(validRect.leftCount, validRect.tileCount.y);
                            v2 tileSize(tileWidth, tileHeight);
                            DrawCmd_AddColoredRectTileSpace(&dc, minTile, tileCount, tileSize, invalidRectColor);
                        }

                        if(validRect.rightCount > 0 && validRect.tileCount.y > 0)
                        {
                            v2i minTile(minTileX + (countX - validRect.rightCount), validRect.minTile.y);
                            v2i tileCount(validRect.rightCount, validRect.tileCount.y);
                            v2 tileSize(tileWidth, tileHeight);
                            DrawCmd_AddColoredRectTileSpace(&dc, minTile, tileCount, tileSize, invalidRectColor);
                        }
                        if(validRect.bottomCount > 0)
                        {
                            v2i minTile(minTileX, minTileY);
                            v2i tileCount(countX, validRect.bottomCount);
                            v2 tileSize(tileWidth, tileHeight);
                            DrawCmd_AddColoredRectTileSpace(&dc, minTile, tileCount, tileSize, invalidRectColor);
                        }
                        if(validRect.topCount > 0)
                        {
                            v2i minTile(minTileX, minTileY + (countY - validRect.topCount));
                            v2i tileCount(countX, validRect.topCount);
                            v2 tileSize(tileWidth, tileHeight);
                            DrawCmd_AddColoredRectTileSpace(&dc, minTile, tileCount, tileSize, invalidRectColor);
                        }

                        if(dc.vertices.count > 0)
                        {
                            drawCommands.Add(dc);
                        }
                    }
                }
                else
                {
                    if(inputMap->mouseDownDuration[MouseButton_Right] == 0.0f)
                    {
                        state->hasSelectionItem = false;
                        state->currentSelection = {};
                    }
                }

                if(inputMap->mouseDownDuration[MouseButton_Left] == 0)
                {
                    isSelecting = true;
                }
            }
            else if(isInsideMapBounds && state->currentEditTool == EditTool_FloodFill)
            {
                if(inputMap->mouseDownDuration[MouseButton_Right] == 0.0f)
                {
                    state->hasSelectionItem = false;
                    state->currentSelection = {};
                }
                else
                {
                    i32 minTileX = state->cursorTileX;
                    i32 minTileY = state->cursorTileY;
                    tbs_array_g<v2i> tilesToFill;
                    size_t maxTileCount = s_cast(size_t, (state->tileCountX * state->tileCountY));
                    bool *visited = new bool[maxTileCount]();
                    v2i *queue = new v2i[maxTileCount]();
                    u32 queueCount = 0;
                    u32 queueFront = 0;
                    queue[queueCount++] = v2i(state->cursorTileX, state->cursorTileY);
                    visited[state->tileCountX * state->cursorTileY + state->cursorTileX] = true;
                    i32 matchSet = 0;
                    i32 matchSetTileX = 0;
                    i32 matchSetTileY = 0;
                    {
                        i32 tileIndex = state->tileCountX * state->cursorTileY + state->cursorTileX;
                        tile_map_tile t = state->layerList[state->activeLayer][tileIndex];
                        matchSet = t.tileSetID;
                        matchSetTileX = t.tileX;
                        matchSetTileY = t.tileY;
                    }
                    while(queueCount > 0)
                    {
                        queueCount--;
                        tbs_assert(queueFront < maxTileCount);
                        v2i tile = queue[queueFront++];
                        minTileX = tile.x < minTileX ? tile.x : minTileX;
                        minTileY = tile.y < minTileY ? tile.y : minTileY;
                        tilesToFill.Add(tile);

                        v2i up(tile.x, tile.y + 1);
                        v2i down(tile.x, tile.y - 1);
                        v2i left(tile.x-1, tile.y);
                        v2i right(tile.x+1, tile.y);

                        if(up.y < state->tileCountY)
                        {
                            i32 tileIndex = state->tileCountX * up.y + up.x;
                            tile_map_tile t = state->layerList[state->activeLayer][tileIndex];
                            if(visited[tileIndex] == false && t.tileSetID == matchSet && t.tileX == matchSetTileX && t.tileY == matchSetTileY)
                            {
                                visited[tileIndex] = true;
                                tbs_assert(queueFront+queueCount < maxTileCount);
                                queue[queueFront+queueCount] = up;
                                queueCount++;
                            }
                        }
                        if(down.y >= 0)
                        {
                            i32 tileIndex = state->tileCountX * down.y + down.x;
                            tile_map_tile t = state->layerList[state->activeLayer][tileIndex];
                            if(visited[tileIndex] == false && t.tileSetID == matchSet && t.tileX == matchSetTileX && t.tileY == matchSetTileY)
                            {
                                visited[tileIndex] = true;
                                tbs_assert(queueFront+queueCount < maxTileCount);
                                queue[queueFront+queueCount] = down;
                                queueCount++;
                            }
                        }
                        if(left.x >= 0)
                        {
                            i32 tileIndex = state->tileCountX * left.y + left.x;
                            tile_map_tile t = state->layerList[state->activeLayer][tileIndex];
                            if(visited[tileIndex] == false && t.tileSetID == matchSet && t.tileX == matchSetTileX && t.tileY == matchSetTileY)
                            {
                                visited[tileIndex] = true;
                                tbs_assert(queueFront+queueCount < maxTileCount);
                                queue[queueFront+queueCount] = left;
                                queueCount++;
                            }
                        }
                        if(right.x < state->tileCountX)
                        {
                            i32 tileIndex = state->tileCountX * right.y + right.x;
                            tile_map_tile t = state->layerList[state->activeLayer][tileIndex];
                            if(visited[tileIndex] == false && t.tileSetID == matchSet && t.tileX == matchSetTileX && t.tileY == matchSetTileY)
                            {
                                visited[tileIndex] = true;
                                tbs_assert(queueFront+queueCount < maxTileCount);
                                queue[queueFront+queueCount] = right;
                                queueCount++;
                            }
                        }
                    }

                    v4 fillRectColor = v4(1.0f, 1.0f, 1.0f, 0.8f);
                    simple_draw_cmd drawCmd = {{}, selectedTileSet->texInfo.texID, GL_TRIANGLES};
                    for(i32 i = 0; i < tilesToFill.count; ++i)
                    {
                        v2i t = tilesToFill.data[i];
                        i32 tileIndex = state->tileCountX * t.y + t.x;
                        i32 setTileX = state->currentSelection.minTile.x + ((t.x - minTileX) % tileSelectionCountX);
                        i32 setTileY = state->currentSelection.minTile.y + ((t.y - minTileY) % tileSelectionCountY);
                        i32 setTileIndex = selectedTileSet->tileCountX * setTileY + setTileX;
                        tbs_assert(setTileIndex < selectedTileSet->tileCountX * selectedTileSet->tileCountY);
                        tile_data setTile = selectedTileSet->tiles[setTileIndex];
                        f32 minPosX = (f32)(t.x * 16);
                        f32 minPosY = (f32)(t.y * 16);
                        f32 maxPosX = minPosX + (f32)16;
                        f32 maxPosY = minPosY + (f32)16;
                        f32 uvMinX = setTile.uvMin.x;
                        f32 uvMinY = setTile.uvMin.y;
                        f32 uvMaxX = setTile.uvMax.x;
                        f32 uvMaxY = setTile.uvMax.y;

                        drawCmd.vertices.Add({v2(minPosX, minPosY), {uvMinX, uvMinY}, fillRectColor});
                        drawCmd.vertices.Add({v2(maxPosX, minPosY), {uvMaxX, uvMinY}, fillRectColor});
                        drawCmd.vertices.Add({v2(maxPosX, maxPosY), {uvMaxX, uvMaxY}, fillRectColor});
                        drawCmd.vertices.Add({v2(maxPosX, maxPosY), {uvMaxX, uvMaxY}, fillRectColor});
                        drawCmd.vertices.Add({v2(minPosX, maxPosY), {uvMinX, uvMaxY}, fillRectColor});
                        drawCmd.vertices.Add({v2(minPosX, minPosY), {uvMinX, uvMinY}, fillRectColor});
                    }

                    if(inputMap->mouseDownDuration[MouseButton_Left] == 0)
                    {
                        for(i32 i = 0; i < tilesToFill.count; ++i)
                        {
                            v2i t = tilesToFill.data[i];
                            i32 dstIndex = (state->tileCountX * t.y) + t.x;
                            i32 setTileX = state->currentSelection.minTile.x + ((t.x - minTileX) % tileSelectionCountX);
                            i32 setTileY = state->currentSelection.minTile.y + ((t.y - minTileY) % tileSelectionCountY);
                            state->layerList[state->activeLayer][dstIndex] = {selectedTileSet->id, setTileX, setTileY};
                        }
                    }

                    if(drawCmd.vertices.count)
                    {
                        drawCommands.Add(drawCmd);
                    }

                    delete[] visited;
                    delete[] queue;

                    tilesToFill.Free();
                }
            }
        }
    }

    {
        if(drawCommands.count > 0)
        {
            glUseProgram(shader->programID);
            mat3 scaleOffset = Translate2D_w(state->offsetX, state->offsetY) * Scale2D_w(state->zoom);
            mat4 proj = CreateOrtho((u32)appCtx->clientSize.x, (u32)appCtx->clientSize.y);
            glUniformMatrix4fv(glGetUniformLocation(shader->programID, "proj"), 1, GL_FALSE, mat4_addr(proj));
            glUniformMatrix3fv(glGetUniformLocation(shader->programID, "scale_offset"), 1, GL_FALSE, mat3_addr(scaleOffset));
            glUniform1i(glGetUniformLocation(shader->programID, "use_texture"), 0);
            glBindVertexArray(tileVAOID);
            glBindBuffer(GL_ARRAY_BUFFER, tileBuffer);

            for(i32 cmdIndex = 0; cmdIndex < drawCommands.count; cmdIndex++)
            {
                simple_draw_cmd& cmd = drawCommands[cmdIndex];
                if(cmd.texID > 0)
                {
                    glBindTexture(GL_TEXTURE_2D, cmd.texID);
                    glUniform1i(glGetUniformLocation(shader->programID, "use_texture"), 1);
                }
                else
                {
                    glBindTexture(GL_TEXTURE_2D, 0);
                    glUniform1i(glGetUniformLocation(shader->programID, "use_texture"), 0);
                }

                glUniformMatrix4fv(glGetUniformLocation(shader->programID, "proj"), 1, GL_FALSE, mat4_addr(proj));

                glBufferData(GL_ARRAY_BUFFER, (GLsizeiptr)cmd.vertices.CalcSize(), cmd.vertices.data, GL_STREAM_DRAW);
                glDrawArrays(cmd.drawMode, 0, cmd.vertices.count);

                currentFrameTotalVerticesCount += cmd.vertices.count;
                cmd.vertices.Free();
            }

            drawCommands.Free();
        }
    }

    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glDeleteBuffers(1, &tileBuffer);
    glDeleteVertexArrays(1, &tileVAOID);

    glDisable(GL_BLEND);
}

static void UpdateAndRenderTileEditorGUI(application_context *appCtx, input_map *inputMap, tile_map_editor_state *state, i32 currentFrameTotalVerticesCount)
{
    ImGuiIO &io = ImGui::GetIO();

    ImGuiWindowFlags mainDockWindowFlags = ImGuiWindowFlags_None;
    mainDockWindowFlags |= ImGuiWindowFlags_NoTitleBar;
    mainDockWindowFlags |= ImGuiWindowFlags_NoCollapse;
    mainDockWindowFlags |= ImGuiWindowFlags_NoResize;
    mainDockWindowFlags |= ImGuiWindowFlags_NoMove;
    mainDockWindowFlags |= ImGuiWindowFlags_NoBringToFrontOnFocus;
    mainDockWindowFlags |= ImGuiWindowFlags_NoNavFocus;
    mainDockWindowFlags |= ImGuiWindowFlags_NoBackground;
    mainDockWindowFlags |= ImGuiWindowFlags_MenuBar;
    mainDockWindowFlags |= ImGuiWindowFlags_NoDocking;
    mainDockWindowFlags |= ImGuiWindowFlags_NoSavedSettings;
    ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
    ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
    ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));
    bool ignored = true;
    ImGuiViewport *viewport = ImGui::GetMainViewport();
    ImGui::SetNextWindowPos(viewport->WorkPos);
    ImGui::SetNextWindowSize(viewport->WorkSize);
    ImGui::SetNextWindowViewport(viewport->ID);
    ImGui::Begin("DockSpace Dummy", &ignored, mainDockWindowFlags);
    ImGui::PopStyleVar(3);

    ImGuiDockNodeFlags mainDockSpaceFlags = ImGuiDockNodeFlags_None;
    mainDockSpaceFlags |= ImGuiDockNodeFlags_PassthruCentralNode;
    ImGuiID mainDockSpaceID = ImGui::GetID("MainDockSpace");
    ImGui::DockSpace(mainDockSpaceID, ImVec2(0.0f, 0.0f), mainDockSpaceFlags);

    static bool FileMenu_LoadTileSet = false;
    if(ImGui::BeginMenuBar())
    {
        if(ImGui::BeginMenu("File"))
        {
            ImGui::MenuItem("Load Tile Set", NULL, &FileMenu_LoadTileSet);

            ImGui::EndMenu();
        }

        ImGui::EndMenuBar();
    }

    if(FileMenu_LoadTileSet)
    {
        static file_browser_context fbCtx = {};
        if(!fbCtx.init)
        {
            RawStringCopy(fbCtx.searchDir, appCtx->texturePath);
            fbCtx.extension[0] = '\0';
            fbCtx.dialogResult = DialogResult_None;
            memset(fbCtx.dataResult, 0, 255);
            fbCtx.init = true;
        }
        if(ShowFileBrowserWidget(&fbCtx))
        {
            FileMenu_LoadTileSet = false;
            fbCtx.init = false;

            if(fbCtx.dialogResult == DialogResult_Open)
            {
                // TODO(torgrim): Try and load sprite sheet. Also
                // check the file extension for valid match since
                // currently don't have a way of filtering with multiple
                // extensions in the browser at the moment(PNG and JPEG)
                tbs_assert(state->tileSetCount < 256);
                tile_set &tileSet = state->tileSetList[state->tileSetCount++];
                tileSet = LoadTileSet(fbCtx.dataResult, 16, 16);
                RawStringCopy(tileSet.name, "New TileSet");
                tileSet.id = (u8)state->tileSetCount-1u;
            }
        }
    }


    {
        ImGuiWindow *mainWin = ImGui::GetCurrentWindow();
        f32 overlayOffsetY = mainWin->MenuBarHeight() + 5.0f;
        ImGuiViewport *guiViewport = ImGui::GetMainViewport();
        ImVec2 waPos = viewport->WorkPos;
        ImVec2 waSize = viewport->WorkSize;
        ImVec2 overlayPos = ImVec2(waPos.x + waSize.x - 5.0f, 300.0f);
        ImGui::SetNextWindowPos(overlayPos, ImGuiCond_FirstUseEver, ImVec2(1.0f, 0.0f));
        ImGui::SetNextWindowViewport(guiViewport->ID);
        ImGui::SetNextWindowSize(ImVec2(250.0f, 0), ImGuiCond_FirstUseEver);
        //ImGui::SetNextWindowPos(ImVec2(.0f, 50.0f), ImGuiCond_FirstUseEver);
    }
    if(ImGui::Begin("Tools"))
    {
        i32 value = state->currentEditTool;
        ImGui::RadioButton("Stamp", &value, EditTool_Stamp);
        ImGui::RadioButton("Flood Fill", &value, EditTool_FloodFill);
        ImGui::RadioButton("Rectangle Fill", &value, EditTool_RectangleFill);
        ImGui::RadioButton("Eraser", &value, EditTool_Eraser);
        ImGui::RadioButton("Rectangle Erase", &value, EditTool_RectangleErase);
        state->currentEditTool = (edit_tool)value;
        // TODO(torgrim): Make this a cleaner solution as we add more tools
        // which require different states.
        if(state->currentEditTool == EditTool_Eraser || state->currentEditTool == EditTool_RectangleErase)
        {
            state->currentSelection = {};
            state->hasSelectionItem = false;
        }

    }
    ImGui::End();

    ImGui::SetNextWindowSize(ImVec2(800.0f, 600.0f), ImGuiCond_FirstUseEver);
    ImGui::SetNextWindowPos(ImVec2(10.0f, 50.0f), ImGuiCond_FirstUseEver);
    if(ImGui::Begin("Tile Sets"))
    {
        if(ImGui::BeginTabBar("##tile_set_tab_bar"))
        {
            for(size_t setIndex = 1; setIndex < state->tileSetCount; setIndex++)
            {
                tile_set *tileSet = state->tileSetList + setIndex;
                char idBuffer[255];
                sprintf_s(idBuffer, 255, "%s###tab_item_%zu", tileSet->name, setIndex);
                if(ImGui::BeginTabItem(idBuffer))
                {
                    tbs_s_assert(array_count(tileSet->name) == 64, "TileSet name member was not the correct size");
                    ImGui::InputText("Name", tileSet->name, array_count(tileSet->name));
                    tile_selection selection = ShowTileSetWidget(tileSet);
                    if(selection.tileSet)
                    {
                        state->currentSelection = selection;
                        state->hasSelectionItem = true;
                        if(state->currentEditTool == EditTool_Eraser || state->currentEditTool == EditTool_RectangleErase)
                        {
                            state->currentEditTool = EditTool_Stamp;
                        }
                    }

                    ImGui::EndTabItem();
                }
            }

            ImGui::EndTabBar();
        }
    }

    ImGui::End();

    ImGui::SetNextWindowSize(ImVec2(800.0f, 200.0f), ImGuiCond_FirstUseEver);
    ImGui::SetNextWindowPos(ImVec2(10.0f, 700.0f), ImGuiCond_FirstUseEver);
    if(ImGui::Begin("Tile Map Editor"))
    {
        ImGuiID dockID = ImGui::GetWindowDockID();
        const char *colNames[] =
        {
            "ID",
            "Name",
            "Hidden",
        };

        if(ImGui::Button("New Layer"))
        {
            size_t maxTileCount = s_cast(size_t, state->tileCountX * state->tileCountY);
            tile_map_tile *newLayer = new tile_map_tile[maxTileCount]();
            state->layerList[state->layerCount] = newLayer;
            RawStringCopy(state->layerNames[state->layerCount], "New Layer");
            state->layerCount++;
        }
        ImGuiTableFlags flags = ImGuiTableFlags_Resizable | ImGuiTableFlags_BordersOuter | ImGuiTableFlags_BordersH | ImGuiTableFlags_BordersV;
        static bool isEditingName = false;
        ImGui::InputText("Layer Name", state->layerNames[state->activeLayer], array_count(state->layerNames[state->activeLayer]));
        if(ImGui::BeginTable("Test Table", array_count(colNames), flags))
        {

            for(size_t cIndex = 0; cIndex < array_count(colNames); ++cIndex)
            {
                ImGui::TableSetupColumn(colNames[cIndex]);
            }
            ImGui::TableHeadersRow();
            char labelBuffer[256];
            for (u8 row = 0; row < state->layerCount; row++)
            {
                ImGui::TableNextRow();

                ImGui::PushID(row);

                ImGui::TableSetColumnIndex(0);
                sprintf_s(labelBuffer, 256, "%03d", row);
                if(ImGui::Selectable(labelBuffer, state->activeLayer == row, ImGuiSelectableFlags_SpanAllColumns | ImGuiSelectableFlags_AllowItemOverlap))
                {
                    state->activeLayer = row;
                }

                char *name = state->layerNames[row];
                ImGui::TableSetColumnIndex(1);
                ImGui::Text(name);

                ImGui::TableSetColumnIndex(2);
                ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(0,0));
                ImGui::Checkbox("##layer_hidden_check", state->layerHidden+row);
                ImGui::PopStyleVar();
                ImGui::PopID();
            }
            ImGui::EndTable();
        }
    }
    ImGui::End();

    {
        ImGuiWindow *mainWin = ImGui::GetCurrentWindow();
        f32 overlayOffsetY = mainWin->MenuBarHeight() + 5.0f;
        ImGuiViewport *guiViewport = ImGui::GetMainViewport();
        ImVec2 waPos = viewport->WorkPos;
        ImVec2 waSize = viewport->WorkSize;
        ImVec2 overlayPos = ImVec2(waPos.x + waSize.x - 5.0f, waPos.y + overlayOffsetY);
        ImGui::SetNextWindowPos(overlayPos, ImGuiCond_Always, ImVec2(1.0f, 0.0f));
        ImGui::SetNextWindowViewport(guiViewport->ID);

        ImGuiWindowFlags overlayFlags = ImGuiWindowFlags_NoDecoration;
        overlayFlags |= ImGuiWindowFlags_NoDocking;
        overlayFlags |= ImGuiWindowFlags_AlwaysAutoResize;
        overlayFlags |= ImGuiWindowFlags_NoSavedSettings;
        overlayFlags |= ImGuiWindowFlags_NoFocusOnAppearing;
        overlayFlags |= ImGuiWindowFlags_NoNav;
        overlayFlags |= ImGuiWindowFlags_NoMove;
        ImGui::Begin("frame_info_overlay", NULL, overlayFlags);
        ImGui::Text("Frame Info:\n\n");
        ImGui::Text("FPS: %f", 1.0f / appCtx->dt);
        ImGui::Text("dt: %f s", appCtx->dt);
        ImGui::Text("Zoom: %f", state->zoom);
        v2 cursorWorldRelPos(inputMap->mousePos.x - state->offsetX, inputMap->mousePos.y - state->offsetY);
        f32 invZoom = 1.0f / state->zoom;
        i32 tileX = (i32)FloorF32(((inputMap->mousePos.x - state->offsetX) * invZoom / 16.0f));
        i32 tileY = (i32)FloorF32(((inputMap->mousePos.y - state->offsetY) * invZoom / 16.0f));
        ImGui::Text("Mouse World Pos X: %f", cursorWorldRelPos.x);
        ImGui::Text("Mouse World Pos Y: %f", cursorWorldRelPos.y);
        ImGui::Text("TileX: %d", tileX);
        ImGui::Text("TileY: %d", tileY);
        ImGui::Text("Frame Vertices Count: %d", currentFrameTotalVerticesCount);
        ImGui::Text("ImGui::IO Mouse Pos X: %f", io.MousePos.x);
        ImGui::Text("ImGui::IO Mouse Pos Y: %f", io.MousePos.y);
        ImGui::Text("ImGui::WantCaptureMouse: %d", io.WantCaptureMouse);
#if 0
        ImGui::Text("Left mouse button down count: %d", buttonDownCount);
        ImGui::Text("Left mouse button duration: %f", mouseDownDuration[MouseButton_Left]);
        ImGui::Text("ImGui MouseLeftButton Down Duration: %f", io.MouseDownDuration[ImGuiMouseButton_Left]);
#endif
        ImGui::End();
    }

    ImGui::End();
}
