#ifndef TILE_EDITOR_H

struct tile_vertex
{
    v2 pos;
    v2 uv;
    v4 col;
};

struct simple_draw_cmd
{
    tbs_array_g<tile_vertex> vertices;
    u32 texID;
    GLenum drawMode;
};

struct tile_map_tile
{
    u8 tileSetID;
    i32 tileX;
    i32 tileY;
};

enum edit_tool
{
    EditTool_Stamp,
    EditTool_FloodFill,
    EditTool_RectangleFill,
    EditTool_Eraser,
    EditTool_RectangleErase,

    EDIT_TOOL_COUNT,
};

struct tile_data
{
    v2 uvMin;
    v2 uvMax;
    u64 id;
};

struct tile_set
{
    char name[64];
    opengl_texture_info texInfo;
    v2i tileSize;
    i32 tileCountX;
    i32 tileCountY;
    tile_data *tiles;
    u8 id;
};

struct tile_selection
{
    // TODO(torgrim): Use ID instead of pointers?
    tile_set *tileSet;
    v2i minTile;
    v2i maxTile;
};

struct valid_rectangle_info
{
    v2i tileCount;
    v2i minTile;
    v2i maxTile;

    i32 leftCount;
    i32 rightCount;
    i32 bottomCount;
    i32 topCount;
};

constexpr size_t MAX_LAYER_COUNT = 256;

struct tile_map_editor_state
{
    tile_map_tile *layerList[MAX_LAYER_COUNT];
    char layerNames[MAX_LAYER_COUNT][64];
    bool layerHidden[MAX_LAYER_COUNT];
    u32 layerCount;
    u32 activeLayer;

    u32 tileSetCount;
    tile_set tileSetList[256];

    edit_tool currentEditTool;
    f32 zoom;
    f32 offsetX;
    f32 offsetY;
    i32 tileWidth;
    i32 tileHeight;
    i32 tileCountX;
    i32 tileCountY;

    tile_selection currentSelection;
    bool hasSelectionItem;

    // TODO(torgrim): Change cursor to mouse instead to
    // be consistent with the rest of the application
    i32 cursorTileX;
    i32 cursorTileY;
    i32 prevCursorTileX;
    i32 prevCursorTileY;
};


#define TILE_EDITOR_H
#endif
