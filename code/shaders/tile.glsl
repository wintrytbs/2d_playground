
#if defined(VERT_SHADER)
layout(location = 0) in vec2 in_vert;
layout(location = 1) in vec2 in_uv;
layout(location = 2) in vec4 in_color;

out vec2 uv;
out vec4 color;

uniform mat4 proj;
uniform mat3 scale_offset;

void main()
{
    uv = in_uv;
    color = in_color;
    gl_Position = proj * vec4(scale_offset * vec3(in_vert, 1.0f), 1.0f);
}

#elif defined(FRAG_SHADER)
in vec2 uv;
in vec4 color;

out vec4 frag_result;

uniform bool use_texture;
uniform sampler2D tex;

void main()
{
    if(!use_texture)
    {
        frag_result = color;
    }
    else
    {
        frag_result = color * texture(tex, uv);
    }
}

#endif
