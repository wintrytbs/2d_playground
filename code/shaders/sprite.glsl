#if defined(VERT_SHADER)

layout(location = 0) in vec2 in_vert;
layout(location = 1) in vec2 in_uv;

out vec2 uv;

uniform mat4 proj;
uniform mat3 model;

void main()
{

    uv = in_uv;
    gl_Position = proj * vec4(model * vec3(in_vert, 1.0f), 1.0f);
}

#elif defined(FRAG_SHADER)

in vec2 uv;

out vec4 result_color;

uniform sampler2D sprite_texture;
uniform int block_size;
uniform int block_index;

void main()
{
    vec2 tSize = textureSize(sprite_texture, 0);
    float x_offset = (((uv.x * block_size) + (block_size * block_index)) / tSize.x);
    result_color = texture(sprite_texture, vec2(x_offset, uv.y));
}

#endif
