#if defined(VERT_SHADER)

layout(location = 0) in vec2 vert_in;
layout(location = 1) in vec2 in_uv;
layout(location = 2) in vec4 in_color;


out vec2 uv;
out vec4 color;

uniform mat4 proj;

void main()
{

    uv = in_uv;
    color = in_color;

    gl_Position = proj * vec4(vert_in, 0.0f, 1.0f);
}

#elif defined(FRAG_SHADER)

in vec2 uv;
in vec4 color;

out vec4 frag_result;

uniform sampler2D tex;

void main()
{
    frag_result = color * texture(tex, uv);
}

#endif
