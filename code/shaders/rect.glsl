#if defined(VERT_SHADER)
layout(location = 0) in vec2 in_vert;
layout(location = 1) in vec2 in_uv;

out vec2 uv;

uniform mat4 proj;
uniform mat3 model;
uniform bool is_fullscreen;

void main()
{
    if(!is_fullscreen)
    {
        gl_Position = proj * vec4(model * vec3(in_vert, 1.0f), 1.0f);
    }
    else
    {
        gl_Position = vec4(in_vert, 0.0f, 1.0f);
    }

    uv = in_uv;
}

#elif defined(FRAG_SHADER)

in vec2 uv;

out vec4 f_color;

uniform vec3 color;
uniform sampler2D background_tex;
uniform sampler2D light_tex;
uniform bool use_texture;

void main()
{
    if(use_texture)
    {
        f_color = texture(light_tex, uv) * texture(background_tex, uv);
    }
    else
    {
        f_color = vec4(color, 1.0f);
    }
}

#endif
