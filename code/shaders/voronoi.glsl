#if defined(VERT_SHADER)

layout(location = 0) in vec2 vert_in;
layout(location = 1) in vec2 in_tex_coord;

flat out vec3 triangle_color;
flat out vec2 tex_coords;

uniform mat4 proj;
uniform float iTime;
uniform vec2 scale;
uniform vec2 offset;
uniform sampler2D tex;

void main()
{

    vec2 dim = textureSize(tex, 0);
    float aspect = dim.y / dim.x;

    float red   = 1.0f - (vert_in.x);
    float green = (vert_in.y) * (vert_in.x);
    float blue  = vert_in.x;
    triangle_color = vec3(red * 0.2f, green * 0.2f, blue * 0.2f);
    gl_Position = proj * vec4(offset + scale * vert_in, 0.0f, 1.0f);

    tex_coords = vec2(vert_in.x, vert_in.y / aspect);
}

#elif defined(FRAG_SHADER)

flat in vec3 triangle_color;
flat in vec2 tex_coords;

out vec4 f_color;

uniform vec3 color;
uniform bool use_color;

uniform sampler2D tex;

void main()
{

    if(use_color)
    {
        f_color = vec4(triangle_color, 1.0f);
    }
    else
    {
        f_color = texture(tex, tex_coords);
    }
}

#endif
