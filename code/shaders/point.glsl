#if defined(VERT_SHADER)

layout(location = 0) in vec2 vert_pos;

uniform vec2 position;
uniform mat4 proj;
uniform bool use_buffer_data;
uniform vec2 scale;
uniform vec2 offset;

void main()
{
    if(!use_buffer_data)
    {
        gl_Position = proj * vec4(position, 0, 1.0f);
    }
    else
    {
        gl_Position = proj * vec4(offset + scale * vert_pos, 0, 1.0f);
        //gl_Position = proj * vec4(vert_pos, 0, 1.0f);
    }
}

#elif defined(FRAG_SHADER)

out vec4 f_color;

uniform vec3 color;

void main()
{
    f_color = vec4(color, 1.0f);
}

#endif
