#if defined(VERT_SHADER)

layout(location = 0) in vec2 in_pos;
layout(location = 1) in vec4 in_col;

out vec4 color;

uniform mat4 proj;

void main()
{
    color = in_col;
    gl_Position = proj * vec4(in_pos, 1.0f, 1.0f);
}

#elif defined(FRAG_SHADER)

in vec4 color;

out vec4 frag_color;

void main()
{
    frag_color = color;
}

#endif
