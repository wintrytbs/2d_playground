#if defined(VERT_SHADER)

layout(location = 0) in vec2 in_pos;
layout(location = 1) in vec4 in_col;
layout(location = 2) in float in_rad;
layout(location = 3) in vec2 in_cen;

out vec4 color;
out vec2 center;
out float radius;
out vec2 pos;

uniform mat4 proj;

void main()
{
    color = in_col;
    center = in_cen;
    radius = in_rad;
    pos = in_pos;
    gl_Position = proj * vec4(in_pos, 1.0f, 1.0f);
}

#elif defined(FRAG_SHADER)

in vec4 color;
in vec2 center;
in float radius;
in vec2 pos;

out vec4 frag_color;

void main()
{
#if 1
    float x = pos.x;
    float cx = center.x;
    float rx = x - cx;
    float y = pos.y;
    float cy = center.y;
    float ry = y - cy;
    if((rx*rx) + (ry*ry) < radius*radius)
    {
        frag_color = color;
    }
    else
    {
        discard;
    }
#else
    frag_color = color;
#endif

}

#endif
