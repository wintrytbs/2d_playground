#if defined(VERT_SHADER)

layout(location = 0) in vec2 in_pos;
layout(location = 1) in vec3 in_col;

out vec3 color;

uniform mat4 proj;
uniform mat3 model;

void main()
{
    color = in_col;
    gl_Position = proj * vec4(model * vec3(in_pos, 1.0f), 1.0f);
}

#elif defined(FRAG_SHADER)

in vec3 color;

out vec4 frag_color;

void main()
{
    frag_color = vec4(color, 1.0f);
}

#endif
