#include "shape_shooter.h"

namespace shape_shooter
{
    static bool IsPlayerColliding(player_state *const player,
                                  rect_2d playRect,
                                  std::vector<shape_circle> *circles)
    {
        // NOTE: Calculate collision boxes for player
        v2 pDim = player->dim;
        v2 playerForward = RotateDeg2D(player->rot) * v2(1.0f, 0.0f);
        v2 playerUp = v2(-playerForward.y, playerForward.x);
        v2 p0 = player->pos + (-playerForward * (pDim.x * 0.5f) + (-playerUp * (pDim.y * 0.5f)));
        v2 p1 = p0 + (playerForward * pDim.x);
        v2 p2 = p0 + (playerUp * pDim.y);
        v2 p3 = p1 + (playerUp * pDim.y);
        v2 p4 = player->pos + (-playerUp * pDim.y * 0.1f);
        v2 p5 = p4 + (playerForward * pDim.x);
        v2 p6 = p4 + (playerUp * pDim.y * 0.2f);
        v2 p7 = p5 + (playerUp * pDim.y * 0.2f);

        v2 playerCorners[] =
        {
            p0,
            p1,
            p2,
            p3,
            p4,
            p5,
            p6,
            p7,
        };

        for(u32 i = 0; i < array_count(playerCorners); i++)
        {
            v2 p = playerCorners[i];
            if(p.y < playRect.min.y || p.y > playRect.max.y ||
               p.x < playRect.min.x || p.x > playRect.max.x)
            {
                return true;
            }
        }

        for(const auto &c : *circles)
        {
            for(u32 b = 0; b < array_count(playerCorners); b++)
            {
                v2 p = playerCorners[b];
                f32 dist = Distance(p, c.pos);
                if(dist <= c.radius)
                {
                    return true;
                }
            }
        }

        return false;
    }

    static void SimulateParticles(std::vector<shape_circle> *circles,
                                  rect_2d playArea,
                                  f32 dt)
    {
        // NOTE: resolve wall collision
        for(auto &c : *circles)
        {
            bool hitWall = false;
            v2 dir{1.0f,1.0f};
            f32 tc = 0.0f;

            v2 newPosA = c.pos;
            v2 oldPosA = c.oldPos;
            v2 v0 = c.vel;
            f32 r0 = c.radius;

            if(newPosA.y < playArea.min.y + r0)
            {
                tc = (playArea.min.y + r0 - oldPosA.y) / (newPosA.y - oldPosA.y);
                dir.y = -1;
                hitWall = true;
            }
            else if(newPosA.y > playArea.max.y - r0)
            {
                tc = (playArea.max.y - r0 - oldPosA.y) / (newPosA.y - oldPosA.y);
                dir.y = -1;
                hitWall = true;
            }

            if(newPosA.x < playArea.min.x + r0)
            {
                tc = (playArea.min.x + r0 - oldPosA.x) / (newPosA.x - oldPosA.x);
                dir.x = -1;
                hitWall = true;
            }
            else if(newPosA.x > playArea.max.x - r0)
            {
                tc = (playArea.max.x - r0 - oldPosA.x) / (newPosA.x - oldPosA.x);
                dir.x = -1;
                hitWall = true;
            }

            if(hitWall)
            {
                tbs_assert(tc >= 0.0f && tc <= 1.0f);
                f32 cx = (1.0f - tc) * oldPosA.x + (tc * newPosA.x);
                f32 cy = (1.0f - tc) * oldPosA.y + (tc * newPosA.y);

                v0 = Hademard(v0, dir);
                v2 rDir = Normalize(v0);

                v2 cv = v2(cx, cy);
                v2 recip = newPosA - cv;
                f32 l = Length(recip);
                newPosA = cv + (rDir * l);

                // TODO: Should the old pos here become
                // the point of impact(cv above)?
                c.oldPos = cv;
                c.pos = newPosA;
                c.vel = v0;
            }


            tbs_assert(c.pos.x >= playArea.min.x + r0 && c.pos.x <= playArea.max.x - r0);
            tbs_assert(c.pos.y >= playArea.min.y + r0 && c.pos.y <= playArea.max.y - r0);
        }

        for(size_t a = 0; a < circles->size(); a++)
        {
            shape_circle &ca = circles->at(a);
            for(size_t b = a+1; b < circles->size(); b++)
            {
                if(a != b)
                {
                    shape_circle &cb = circles->at(b);
                    f32 minDist = Distance(ca.pos, cb.pos);
                    if(minDist <= ca.radius + cb.radius)
                    {
                        f32 maxDist = Distance(ca.oldPos, cb.oldPos);
                        tbs_assert(maxDist > ca.radius + cb.radius);
                    }
                }
            }
        }

resolve_collision:
        // NOTE: Resolve collision
        for(size_t a = 0; a < circles->size(); a++)
        {
            auto *cA = &(*circles)[a];
            v2 oldPosA = cA->oldPos;
            v2 newPosA = cA->pos;
            v2 v0 = cA->vel;
            f32 r0 = cA->radius;
            bool anyHit = false;
            for(size_t b = 0; b < circles->size(); b++)
            {
                if(b != a)
                {
                    auto *cB = &(*circles)[b];
                    v2 oldPosB = cB->oldPos;
                    v2 newPosB = cB->pos;
                    f32 r1 = cB->radius;
                    if(Distance(newPosA, newPosB) <= (r0 + r1))
                    {
                        f32 currentDist = Distance(newPosA, newPosB);
                        f32 target = r0 + r1;
                        v2 minA = newPosA;
                        v2 maxA = minA + Normalize(-v0) * ((r0+r1));
                        v2 minB = newPosB;
                        v2 nPos = Normalize(-cB->vel);
                        v2 nNeg = -Normalize(cB->vel);
                        v2 maxB = minB + Normalize(-cB->vel) * ((r0+r1));

                        f32 maxDist = Distance(maxA, maxB);
                        f32 maxDist2 = Distance(oldPosA, oldPosB);
                        f32 Dist2 = Distance(minA, maxA);
                        f32 Dist3 = Distance(minB, maxB);
                        tbs_assert(maxDist >= (r0 + r1));

                        f32 t = 0.5f;
                        f32 minDist = -1.0f;
                        v2 hitLocA = v2(0.0f, 0.0f);
                        v2 hitLocB = v2(0.0f, 0.0f);
                        for(u32 i = 0; i < 20; i++)
                        {
                            f32 ax = (1.0f-t) * minA.x + (t*maxA.x);
                            f32 ay = (1.0f-t) * minA.y + (t*maxA.y);

                            f32 bx = (1.0f-t) * minB.x + (t*maxB.x);
                            f32 by = (1.0f-t) * minB.y + (t*maxB.y);

                            f32 dist = Distance(v2(ax, ay), v2(bx, by));
                            if(dist < target)
                            {
                                minA = v2(ax, ay);
                                minB = v2(bx, by);
                                hitLocA = minA;
                                hitLocB = minB;
                            }
                            else if(dist > target)
                            {
                                maxA = v2(ax, ay);
                                maxB = v2(bx, by);
                                hitLocA = minA;
                                hitLocB = minB;
                                minDist = dist;
                            }
                            else
                            {
                                minDist = dist;
                                hitLocA = v2(ax, ay);
                                hitLocB = v2(bx, by);
                                break;
                            }
                        }

                        tbs_assert(minDist >= (r0+r1) && minDist - 0.1f <= (r0+r1));

                        v2 v1 = cB->vel;

                        v2 n0 = Normalize(newPosA - newPosB);
                        v2 n1 = Normalize(newPosB - newPosA);

                        f32 d0 = Dot(v0 - v1, n0);
                        f32 d1 = Dot(v1 - v0, n1);

                        v2 p0 = d0 * n0;
                        v2 p1 = d1 * n1;

                        v2 vp0 = v0 - p0;
                        v2 vp1 = v1 - p1;

                        v0 = vp0;
                        v1 = vp1;

                        cB->vel = v1;

                        oldPosA = hitLocA;
                        newPosA = hitLocA + v0 * dt;
                        cB->oldPos = hitLocB;
                        cB->pos = hitLocB + v1 * dt;

#if 0
                        tbs_assert((cB->pos.x > playArea.min.x + r1 && cB->pos.x < playArea.max.x - r1) ||
                                   (cB->oldPos.x > playArea.min.x + r1 && cB->oldPos.x < playArea.max.x - r1));
                        tbs_assert((cB->pos.y > playArea.min.y + r1 && cB->pos.y < playArea.max.y - r1) ||
                                   (cB->oldPos.y > playArea.min.y + r1 && cB->oldPos.y < playArea.max.y - r1));
#endif

                        anyHit = true;
                    }
                }
            }

            if(anyHit)
            {
                cA->oldPos = oldPosA;
                cA->pos = newPosA;
                cA->vel = v0;
            }

#if 0
            tbs_assert((cA->pos.x > playArea.min.x + r0 && cA->pos.x < playArea.max.x - r0) ||
                       (cA->oldPos.x > playArea.min.x + r0 && cA->oldPos.x < playArea.max.x - r0));
            tbs_assert((cA->pos.y > playArea.min.y + r0 && cA->pos.y < playArea.max.y - r0) ||
                       (cA->oldPos.y > playArea.min.y + r0 && cA->oldPos.y < playArea.max.y - r0));
#endif
        }

        for(size_t a = 0; a < circles->size(); a++)
        {
            shape_circle &ca = circles->at(a);
            for(size_t b = a+1; b < circles->size(); b++)
            {
                if(a != b)
                {
                    shape_circle &cb = circles->at(b);
                    f32 minDist = Distance(ca.pos, cb.pos);
                    if(minDist <= ca.radius + cb.radius)
                    {
                        static size_t retryCount = 0;
                        retryCount++;
                        PrintDebugMsg("Collision retry count: %zu\n", retryCount);
                        goto resolve_collision;
                    }
#if 0
                    tbs_assert(minDist > ca.radius + cb.radius);
                    if(minDist <= ca.radius + cb.radius)
                    {
                        f32 maxDist = Distance(ca.oldPos, cb.oldPos);
                        tbs_assert(maxDist > ca.radius + cb.radius);
                    }
#endif
                }
            }
        }

        for(auto &c : *circles)
        {
            bool hitWall = false;
            v2 dir{1.0f,1.0f};
            f32 tc = 0.0f;

            v2 newPosA = c.pos;
            v2 oldPosA = c.oldPos;
            v2 v0 = c.vel;
            f32 r0 = c.radius;

            if(newPosA.y < playArea.min.y + r0)
            {
                tc = (playArea.min.y + r0 - oldPosA.y) / (newPosA.y - oldPosA.y);
                dir.y = -1;
                hitWall = true;
            }
            else if(newPosA.y > playArea.max.y - r0)
            {
                tc = (playArea.max.y - r0 - oldPosA.y) / (newPosA.y - oldPosA.y);
                dir.y = -1;
                hitWall = true;
            }

            if(newPosA.x < playArea.min.x + r0)
            {
                tc = (playArea.min.x + r0 - oldPosA.x) / (newPosA.x - oldPosA.x);
                dir.x = -1;
                hitWall = true;
            }
            else if(newPosA.x > playArea.max.x - r0)
            {
                tc = (playArea.max.x - r0 - oldPosA.x) / (newPosA.x - oldPosA.x);
                dir.x = -1;
                hitWall = true;
            }

            if(hitWall)
            {
                tbs_assert(tc >= 0.0f && tc <= 1.0f);
                f32 cx = (1.0f - tc) * oldPosA.x + (tc * newPosA.x);
                f32 cy = (1.0f - tc) * oldPosA.y + (tc * newPosA.y);

                v0 = Hademard(v0, dir);
                v2 rDir = Normalize(v0);

                v2 cv = v2(cx, cy);
                v2 recip = newPosA - cv;
                f32 l = Length(recip);
                newPosA = cv + (rDir * l);
            }

            // TODO: Should the old pos here become
            // the point of impact(cv above)?
            c.oldPos = c.pos;
            c.pos = newPosA;
            c.vel = v0;

            tbs_assert(c.pos.x > playArea.min.x + r0 && c.pos.x < playArea.max.x - r0);
            tbs_assert(c.pos.y > playArea.min.y + r0 && c.pos.y < playArea.max.y - r0);
        }


    }

    template <typename T>
    static void RemoveIndicesFromVector(std::vector<T> *list, std::vector<size_t> *indices)
    {
        for(auto it = indices->rbegin(); it != indices->rend(); it++)
        {
            list->erase(list->begin() + (i64)(*it));
        }
    }

    static void UpdateAndRender(application_context *appCtx, input_map *inputMap, game_context *gameCtx)
    {
        tbs_assert(gameCtx->mode != GameMode_None);
        ImGuiIO &io = ImGui::GetIO();
        {
            // NOTE: setup base imgui window
            ImGuiWindowFlags mainDockWindowFlags = ImGuiWindowFlags_None;
            mainDockWindowFlags |= ImGuiWindowFlags_NoTitleBar;
            mainDockWindowFlags |= ImGuiWindowFlags_NoCollapse;
            mainDockWindowFlags |= ImGuiWindowFlags_NoResize;
            mainDockWindowFlags |= ImGuiWindowFlags_NoMove;
            mainDockWindowFlags |= ImGuiWindowFlags_NoBringToFrontOnFocus;
            mainDockWindowFlags |= ImGuiWindowFlags_NoNavFocus;
            mainDockWindowFlags |= ImGuiWindowFlags_NoBackground;
            mainDockWindowFlags |= ImGuiWindowFlags_MenuBar;
            mainDockWindowFlags |= ImGuiWindowFlags_NoDocking;
            mainDockWindowFlags |= ImGuiWindowFlags_NoSavedSettings;
            ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
            ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
            ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));
            bool ignored = true;
            ImGuiViewport *viewport = ImGui::GetMainViewport();
            ImGui::SetNextWindowPos(viewport->WorkPos);
            ImGui::SetNextWindowSize(viewport->WorkSize);
            ImGui::SetNextWindowViewport(viewport->ID);
            ImGui::Begin("DockSpace Dummy", &ignored, mainDockWindowFlags);
            ImGui::PopStyleVar(3);

            ImGuiDockNodeFlags mainDockSpaceFlags = ImGuiDockNodeFlags_None;
            mainDockSpaceFlags |= ImGuiDockNodeFlags_PassthruCentralNode;
            ImGuiID mainDockSpaceID = ImGui::GetID("MainDockSpace");
            ImGui::DockSpace(mainDockSpaceID, ImVec2(0.0f, 0.0f), mainDockSpaceFlags);
        }

        //PrintDebugMsgDebugger("Sizeof game_context: %zu\n", sizeof(game_context));
        f32 areaOffsetY = 0.0f;
        {
            ImGuiWindow *mainWin = ImGui::GetCurrentWindow();
            areaOffsetY = mainWin->MenuBarHeight() + 5.0f;
        }

        v2 areaSize = appCtx->clientSize;
        rect_2d playArea{{0,0}, {areaSize.x, areaSize.y - areaOffsetY}};

        if(gameCtx->mode == GameMode_StartMenu)
        {
            ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(32.0f, 32.0f));
            ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 4.0f);
            ImGuiWindowFlags startMenuFlags = ImGuiWindowFlags_NoMove;
            startMenuFlags |= ImGuiWindowFlags_NoResize;
            startMenuFlags |= ImGuiWindowFlags_NoCollapse;
            startMenuFlags |= ImGuiWindowFlags_NoTitleBar;
            startMenuFlags |= ImGuiWindowFlags_AlwaysAutoResize;
            startMenuFlags |= ImGuiWindowFlags_NoSavedSettings;
            ImGui::SetNextWindowSize(ImVec2(300.0f, 0.0f));
            ImGui::SetNextWindowPos(ImVec2((appCtx->clientSize.x / 2.0f) - 150.0f, (appCtx->clientSize.y / 2.0f) - 150.0f));
            ImGui::SetNextWindowBgAlpha(1.0f);
            ImGui::Begin("Pause Menu", nullptr, startMenuFlags);

            f32 crw = ImGui::GetWindowContentRegionWidth();
            ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(7.0f, 14.0f));
            ImGui::PushStyleVar(ImGuiStyleVar_FrameRounding, 4.0f);
            ImGui::Text("Shape Shooter");
            if(ImGui::Button("Play", ImVec2(crw, 50.0f)))
            {
                gameCtx->mode = GameMode_Start;
            }
            if(ImGui::Button("Quit", ImVec2(crw, 50.0f)))
            {
                // TODO: Here we should have our own platform
                // independent event/message system
                PostQuitMessage(0);
            }
            ImGui::PopStyleVar(2);
            ImGui::End();
            ImGui::PopStyleVar(2);
        }
        else if(gameCtx->mode == GameMode_Start)
        {
            gameCtx->projectiles = {};
            gameCtx->player = {};
            gameCtx->player.pos = appCtx->clientSize / 2.0f;
            gameCtx->player.dim = v2(30.0f, 30.0f);
            gameCtx->player.health = 1;
            gameCtx->player.score = 0;
            gameCtx->spawnTimer = 0.0f;

            tbs_assert(gameCtx->activeCircles.capacity() == SHAPE_MAX_COUNT);
            tbs_assert(gameCtx->inactiveCircles.capacity() == SHAPE_MAX_COUNT);
            gameCtx->activeCircles.clear();
            gameCtx->inactiveCircles.clear();

            gameCtx->particles.clear();
            gameCtx->projectiles.clear();
            gameCtx->shootTimer = SHOOT_TIMER_TRESHOLD;

            gameCtx->mode = GameMode_Playing;
            gameCtx->isInit = true;
        }
        else if(gameCtx->mode == GameMode_Restart)
        {
            gameCtx->projectiles = {};
            gameCtx->player = {};
            gameCtx->player.pos = appCtx->clientSize / 2.0f;
            gameCtx->player.dim = v2(30.0f, 30.0f);
            gameCtx->player.health = 1;
            gameCtx->player.score = 0;
            gameCtx->spawnTimer = 0.0f;

            tbs_assert(gameCtx->activeCircles.capacity() == SHAPE_MAX_COUNT);
            tbs_assert(gameCtx->inactiveCircles.capacity() == SHAPE_MAX_COUNT);
            gameCtx->activeCircles.clear();
            gameCtx->inactiveCircles.clear();

            gameCtx->particles.clear();
            gameCtx->projectiles.clear();
            gameCtx->shootTimer = SHOOT_TIMER_TRESHOLD;

            gameCtx->mode = GameMode_Playing;
            gameCtx->isInit = true;
        }
        else if(gameCtx->mode == GameMode_PauseMenu)
        {
            ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(32.0f, 32.0f));
            ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 4.0f);
            ImGuiWindowFlags pauseMenuFlags = ImGuiWindowFlags_NoMove;
            pauseMenuFlags |= ImGuiWindowFlags_NoResize;
            pauseMenuFlags |= ImGuiWindowFlags_NoCollapse;
            pauseMenuFlags |= ImGuiWindowFlags_NoTitleBar;
            pauseMenuFlags |= ImGuiWindowFlags_AlwaysAutoResize;
            pauseMenuFlags |= ImGuiWindowFlags_NoSavedSettings;
            ImGui::SetNextWindowSize(ImVec2(300.0f, 0.0f));
            ImGui::SetNextWindowPos(ImVec2((appCtx->clientSize.x / 2.0f) - 150.0f, (appCtx->clientSize.y / 2.0f) - 150.0f));
            ImGui::SetNextWindowBgAlpha(1.0f);
            ImGui::Begin("Pause Menu", nullptr, pauseMenuFlags);

            f32 crw = ImGui::GetWindowContentRegionWidth();
            ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(7.0f, 14.0f));
            ImGui::PushStyleVar(ImGuiStyleVar_FrameRounding, 4.0f);
            ImGui::Text("Pause");
            if(ImGui::Button("Resume", ImVec2(crw, 50.0f)))
            {
                gameCtx->mode = GameMode_Playing;
            }
            if(ImGui::Button("Restart", ImVec2(crw, 50.0f)))
            {
                gameCtx->mode = GameMode_Restart;
            }
            if(ImGui::Button("Quit", ImVec2(crw, 50.0f)))
            {
                // TODO: Here we should have our own platform
                // independent event/message system
                PostQuitMessage(0);
            }
            ImGui::PopStyleVar(2);
            ImGui::End();
            ImGui::PopStyleVar(2);
        }
        else if(gameCtx->mode == GameMode_Playing)
        {
            tbs_assert(gameCtx->isInit);
            {
                // TODO: Update player
                f32 rotSpeed = 200.0f * appCtx->dt;
                if(inputMap->keyDown[InputKey_A])
                {
                    gameCtx->player.rot += rotSpeed;
                }
                if(inputMap->keyDown[InputKey_D])
                {
                    gameCtx->player.rot -= rotSpeed;
                }

                v2 forward = RotateDeg2D(gameCtx->player.rot) * v2(1.0f, 0.0f);
                f32 acc = 10.0f;
                if(inputMap->keyDown[InputKey_W])
                {
                    gameCtx->player.vel += (forward * acc * appCtx->dt);
                }

                if(inputMap->keyDown[InputKey_Space])
                {
                    if(gameCtx->shootTimer >= SHOOT_TIMER_TRESHOLD)
                    {
                        gameCtx->shootTimer = 0.0f;
                        gameCtx->projectiles.push_back({gameCtx->player.pos + forward * (gameCtx->player.dim.x * 0.1f), v2(forward * 1000.0f), v2(2.0f, 2.0f)});
                    }
                }

                if(inputMap->keyDownDuration[InputKey_P] == 0.0f)
                {
                    gameCtx->mode = GameMode_PauseMenu;
                }
            }


            auto colStartTime = Win32GetWallClockTime();
            {
                // NOTE: Update all positions
                for(auto &c : gameCtx->activeCircles)
                {
                    c.oldPos = c.pos;
                    c.pos = c.pos + (c.vel * appCtx->dt);
                }

                std::vector<size_t> moveIndices;
                moveIndices.reserve(gameCtx->inactiveCircles.size());
                for(size_t i = 0u; i < gameCtx->inactiveCircles.size(); i++)
                {
                    // TODO: If a particle/ball is added as active it can
                    // still be inside another particle since collision is only handled
                    // for active particles. This causes some obvious problems that needs
                    // to be handled better.
                    shape_circle &c = gameCtx->inactiveCircles[i];
                    c.oldPos = c.pos;
                    c.pos = c.oldPos + (c.vel * appCtx->dt);
                    // NOTE: Check if the circle is active(isn't coming from outside the window area as
                    // a result of just spawning)
                    bool insideOther = false;
                    for(size_t j = 0u; j < gameCtx->activeCircles.size(); j++)
                    {
                        auto &other = gameCtx->activeCircles[j];
                        if(Distance(c.pos, other.pos) <= c.radius + other.radius)
                        {
                            insideOther = true;
                            break;
                        }
                    }
                    if(!insideOther)
                    {
                        if(c.oldPos.x > playArea.min.x + c.radius && c.oldPos.y > playArea.min.y + c.radius &&
                           c.oldPos.x < playArea.max.x - c.radius && c.oldPos.y < playArea.max.y - c.radius)
                        {
                            c.col = v4(1.0f, 0.0f, 0.0f, 0.0f);
                            gameCtx->activeCircles.push_back(c);
                            moveIndices.push_back(i);
                        }
                    }
                }

                RemoveIndicesFromVector(&gameCtx->inactiveCircles, &moveIndices);


                if(IsPlayerColliding(&gameCtx->player, playArea, &gameCtx->activeCircles))
                {
                    gameCtx->player.health--;
                }

                {
                    std::vector<size_t> projectileIndices;
                    std::vector<size_t> cIndices;
                    projectileIndices.reserve(gameCtx->projectiles.size());
                    cIndices.reserve(gameCtx->activeCircles.size());
                    size_t pIndex = 0;
                    static std::mt19937 partRand(50);
                    for(auto &p : gameCtx->projectiles)
                    {
                        size_t cIndex = 0;
                        for(const auto &c : gameCtx->activeCircles)
                        {
                            f32 dist = Distance(c.pos, p.pos);
                            if(dist <= p.size.x + c.radius)
                            {
                                gameCtx->player.score++;
                                projectileIndices.push_back(pIndex);
                                cIndices.push_back(cIndex);
                                for(u32 partIndex = 0; partIndex < 20; partIndex++)
                                {
                                    v4 particleColor(NextRand01(partRand), NextRand01(partRand), NextRand01(partRand), 1.0f);
                                    gameCtx->particles.push_back({c.pos, v2(NextRandRangeF32(partRand, -200.0f, 200.0f), NextRandRangeF32(partRand, -200.0f, 200.0f)), v2(5.0f, 5.0f), particleColor});
                                    // TODO: This is not quit correct because currently
                                    // the different projectiles can hit the same
                                    // circle or one particle can hit multiple circles
                                    // when what we want is max one unit increase in
                                    // score per valid hit. Need some way of either
                                    // directly removing the involved items when hit
                                    // or mark them as no longer hittable
                                }
                            }
                            cIndex++;
                        }
                        pIndex++;
                    }

                    // TODO: This can just be done once as long as
                    // the indices are in increasing order
                    RemoveIndicesFromVector(&gameCtx->projectiles, &projectileIndices);

                    std::vector<size_t> particleIndices;
                    particleIndices.reserve(gameCtx->particles.size());
                    for(size_t i = 0u; i < gameCtx->particles.size(); i++)
                    {
                        auto &p = gameCtx->particles[i];
                        p.pos = p.pos + (p.vel * appCtx->dt);
                        if(p.pos.x < playArea.min.x || p.pos.y < playArea.min.y ||
                        p.pos.x > playArea.max.x || p.pos.y > playArea.max.y)
                        {
                            particleIndices.push_back(i);
                        }
                    }

                    projectileIndices.clear();
                    for(size_t i = 0u; i < gameCtx->projectiles.size(); i++)
                    {
                        auto &p = gameCtx->projectiles[i];
                        p.pos = p.pos + (p.vel * appCtx->dt);
                        if(p.pos.x < playArea.min.x || p.pos.y < playArea.min.y ||
                        p.pos.x > playArea.max.x || p.pos.y > playArea.max.y)
                        {
                            projectileIndices.push_back(i);
                        }
                    }

                    RemoveIndicesFromVector(&gameCtx->activeCircles, &cIndices);
                    RemoveIndicesFromVector(&gameCtx->projectiles, &projectileIndices);
                    RemoveIndicesFromVector(&gameCtx->particles, &particleIndices);

                }

                SimulateParticles(&gameCtx->activeCircles, playArea, appCtx->dt);

                f32 elapsed = Win32GetElapsedSeconds(colStartTime, Win32GetWallClockTime());

                PrintDebugMsgDebugger("Elapsed Collision Time: %f, Entities tested: %zu\n", elapsed, gameCtx->activeCircles.size());

            }


            tbs_assert(gameCtx->player.health >= 0);
            if(gameCtx->player.health == 0)
            {
                // TODO: Create death particles/animation
                // TODO: set mode == game over
                gameCtx->mode = GameMode_GameOver;
                static std::mt19937 partRand(50);
                for(u32 i = 0; i < 40; i++)
                {
                    f32 size = NextRandRangeF32(partRand, 3.0f, 10.0f);
                    v4 particleColor(NextRand01(partRand), NextRand01(partRand), NextRand01(partRand), 1.0f);
                    gameCtx->particles.push_back({gameCtx->player.pos, v2(NextRandRangeF32(partRand, -200.0f, 200.0f), NextRandRangeF32(partRand, -200.0f, 200.0f)), v2(size, size), particleColor});
                }
            }
            else if(gameCtx->player.score >= SHAPE_MAX_COUNT)
            {
                gameCtx->mode = GameMode_Victory;
            }
            else
            {
                gameCtx->player.pos += gameCtx->player.vel;

#if 1
                /*
                 * first get two random numbers either 0 and 1 which
                 * are indices into which area to spawn into.
                 * Then get two random numbers(x,y) for coordinates inside
                 * that area. This can just be two floats between 0-1 and then
                 * use that to scale to the area.
                 * Then generate random velocity in a direction that intersects
                 * with the play area.
                 */
                struct spawn_area
                {
                    v2 rangeX;
                    v2 rangeY;
                };


                spawn_area spawnAreas[] =
                {
                    {v2(-200.0f, -100.0f), v2(-200.0f, appCtx->clientSize.y + 200.0f)},
                    {v2(-200.0f, appCtx->clientSize.x + 200.0f), v2(-200.0f, -100.0f)},
                    {v2(-200.0f, appCtx->clientSize.x + 200.0f), v2(appCtx->clientSize.y + 100.0f, appCtx->clientSize.y + 200.0f)},
                    {v2(appCtx->clientSize.x + 100.0f, appCtx->clientSize.x + 200.0f), v2(-200.0f, appCtx->clientSize.y + 200.0f)},
                };

                static std::mt19937 ranGen(300);
                if(gameCtx->spawnTimer > SPAWN_TIMER_TRESHOLD)
                {
                    gameCtx->spawnTimer = 0.0f;

                    size_t totalCircleCount = gameCtx->inactiveCircles.size() + gameCtx->activeCircles.size();
                    if(totalCircleCount < SHAPE_MAX_COUNT)
                    {
                        u32 areaIndex = NextRandRangeU32(ranGen, 0, 3);
                        spawn_area area = spawnAreas[areaIndex];
                        f32 distX = area.rangeX.y - area.rangeX.x;
                        f32 distY = area.rangeY.y - area.rangeY.y;
                        f32 r0 = NextRand01(ranGen);
                        f32 r1 = NextRand01(ranGen);
                        v2 pos(area.rangeX.x + (distX * r0), area.rangeY.x + (distY * r1));
                        v2 target(appCtx->clientSize.x * r0, appCtx->clientSize.y * r1);
                        v2 dir = target - pos;
                        v2 vel = Normalize(dir);
                        vel *= 400.0f;
                        gameCtx->inactiveCircles.push_back({pos, pos, vel, 30.0f, v4(0.0f, 0.0f, 1.0f, 1.0f)});
                    }
                }
                else
                {
                    gameCtx->spawnTimer += appCtx->dt;
                }
#endif

                gameCtx->shootTimer += appCtx->dt;
            }


        }
        else if(gameCtx->mode == GameMode_Victory)
        {
            gameCtx->player.pos += gameCtx->player.vel;
            for(auto &c : gameCtx->activeCircles)
            {
                c.oldPos = c.pos;
                c.pos = c.pos + (c.vel * appCtx->dt);
            }

            std::vector<size_t> moveIndices;
            moveIndices.reserve(gameCtx->inactiveCircles.size());
            for(size_t i = 0u; i < gameCtx->inactiveCircles.size(); i++)
            {
                // TODO: If a particle/ball is added as active it can
                // still be inside another particle since collision is only handled
                // for active particles. This causes some obvious problems that needs
                // to be handled better.
                shape_circle &c = gameCtx->inactiveCircles[i];
                c.oldPos = c.pos;
                c.pos = c.oldPos + (c.vel * appCtx->dt);
                // NOTE: Check if the circle is active(isn't coming from outside the window area as
                // a result of just spawning)
                if(c.oldPos.x > playArea.min.x + c.radius && c.oldPos.y > playArea.min.y + c.radius &&
                   c.oldPos.x < playArea.max.x - c.radius && c.oldPos.y < playArea.max.y - c.radius)
                {
                    c.col = v4(1.0f, 0.0f, 0.0f, 0.0f);
                    gameCtx->activeCircles.push_back(c);
                    moveIndices.push_back(i);
                }
            }


            RemoveIndicesFromVector(&gameCtx->inactiveCircles, &moveIndices);

            std::vector<size_t> projectileIndices;
            projectileIndices.reserve(gameCtx->projectiles.size());
            {
                std::vector<size_t> cIndices;
                cIndices.reserve(gameCtx->activeCircles.size());
                size_t pIndex = 0;
                static std::mt19937 partRand(50);
                for(auto &p : gameCtx->projectiles)
                {
                    size_t cIndex = 0;
                    for(const auto &c : gameCtx->activeCircles)
                    {
                        f32 dist = Distance(c.pos, p.pos);
                        if(dist <= p.size.x + c.radius)
                        {
                            projectileIndices.push_back(pIndex);
                            cIndices.push_back(cIndex);
                            for(u32 partIndex = 0; partIndex < 20; partIndex++)
                            {
                                v4 particleColor(NextRand01(partRand), NextRand01(partRand), NextRand01(partRand), 1.0f);
                                gameCtx->particles.push_back({c.pos, v2(NextRandRangeF32(partRand, -200.0f, 200.0f), NextRandRangeF32(partRand, -200.0f, 200.0f)), v2(10.0f, 10.0f), particleColor});
                            }
                        }
                        cIndex++;
                    }
                    pIndex++;
                }

                RemoveIndicesFromVector(&gameCtx->activeCircles, &cIndices);
            }

            SimulateParticles(&gameCtx->activeCircles, playArea, appCtx->dt);

            std::vector<size_t> particleIndices;
            particleIndices.reserve(gameCtx->particles.size());
            for(size_t i = 0u; i < gameCtx->particles.size(); i++)
            {
                auto &p = gameCtx->particles[i];
                p.pos = p.pos + (p.vel * appCtx->dt);
                if(p.pos.x < playArea.min.x || p.pos.y < playArea.min.y ||
                   p.pos.x > playArea.max.x || p.pos.y > playArea.max.y)
                {
                    particleIndices.push_back(i);
                }
            }

            for(size_t i = 0u; i < gameCtx->projectiles.size(); i++)
            {
                auto &p = gameCtx->projectiles[i];
                p.pos = p.pos + (p.vel * appCtx->dt);
                if(p.pos.x < playArea.min.x || p.pos.y < playArea.min.y ||
                   p.pos.x > playArea.max.x || p.pos.y > playArea.max.y)
                {
                    projectileIndices.push_back(i);
                }
            }

            RemoveIndicesFromVector(&gameCtx->projectiles, &projectileIndices);
            RemoveIndicesFromVector(&gameCtx->particles, &particleIndices);


            ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(32.0f, 32.0f));
            ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 4.0f);
            ImGuiWindowFlags victoryMenuFlags = ImGuiWindowFlags_NoMove;
            victoryMenuFlags |= ImGuiWindowFlags_NoResize;
            victoryMenuFlags |= ImGuiWindowFlags_NoCollapse;
            victoryMenuFlags |= ImGuiWindowFlags_NoTitleBar;
            victoryMenuFlags |= ImGuiWindowFlags_AlwaysAutoResize;
            victoryMenuFlags |= ImGuiWindowFlags_NoSavedSettings;
            ImGui::SetNextWindowSize(ImVec2(300.0f, 0.0f));
            ImGui::SetNextWindowPos(ImVec2((appCtx->clientSize.x / 2.0f) - 150.0f, (appCtx->clientSize.y / 2.0f) - 150.0f));
            ImGui::SetNextWindowBgAlpha(1.0f);
            ImGui::Begin("Pause Menu", nullptr, victoryMenuFlags);

            f32 crw = ImGui::GetWindowContentRegionWidth();
            ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(7.0f, 14.0f));
            ImGui::PushStyleVar(ImGuiStyleVar_FrameRounding, 4.0f);
            ImGui::Text("Victory!");
            ImGui::Text("Score: %d", gameCtx->player.score);
            if(ImGui::Button("Restart", ImVec2(crw, 50.0f)))
            {
                gameCtx->mode = GameMode_Restart;
            }
            if(ImGui::Button("Quit", ImVec2(crw, 50.0f)))
            {
                // TODO: Here we should have our own platform
                // independent event/message system
                PostQuitMessage(0);
            }
            ImGui::PopStyleVar(2);
            ImGui::End();
            ImGui::PopStyleVar(2);
        }
        else if(gameCtx->mode == GameMode_GameOver)
        {

            for(auto &c : gameCtx->activeCircles)
            {
                c.oldPos = c.pos;
                c.pos = c.pos + (c.vel * appCtx->dt);
            }

            std::vector<size_t> moveIndices;
            moveIndices.reserve(gameCtx->inactiveCircles.size());
            for(size_t i = 0u; i < gameCtx->inactiveCircles.size(); i++)
            {
                // TODO: If a particle/ball is added as active it can
                // still be inside another particle since collision is only handled
                // for active particles. This causes some obvious problems that needs
                // to be handled better.
                shape_circle &c = gameCtx->inactiveCircles[i];
                c.oldPos = c.pos;
                c.pos = c.oldPos + (c.vel * appCtx->dt);
                // NOTE: Check if the circle is active(isn't coming from outside the window area as
                // a result of just spawning)
                if(c.oldPos.x > playArea.min.x + c.radius && c.oldPos.y > playArea.min.y + c.radius &&
                   c.oldPos.x < playArea.max.x - c.radius && c.oldPos.y < playArea.max.y - c.radius)
                {
                    c.col = v4(1.0f, 0.0f, 0.0f, 0.0f);
                    gameCtx->activeCircles.push_back(c);
                    moveIndices.push_back(i);
                }
            }


            RemoveIndicesFromVector(&gameCtx->inactiveCircles, &moveIndices);

            std::vector<size_t> projectileIndices;
            projectileIndices.reserve(gameCtx->projectiles.size());
            {
                std::vector<size_t> cIndices;
                cIndices.reserve(gameCtx->activeCircles.size());
                size_t pIndex = 0;
                static std::mt19937 partRand(50);
                for(auto &p : gameCtx->projectiles)
                {
                    size_t cIndex = 0;
                    for(const auto &c : gameCtx->activeCircles)
                    {
                        f32 dist = Distance(c.pos, p.pos);
                        if(dist <= p.size.x + c.radius)
                        {
                            projectileIndices.push_back(pIndex);
                            cIndices.push_back(cIndex);
                            for(u32 partIndex = 0; partIndex < 20; partIndex++)
                            {
                                v4 particleColor(NextRand01(partRand), NextRand01(partRand), NextRand01(partRand), 1.0f);
                                gameCtx->particles.push_back({c.pos, v2(NextRandRangeF32(partRand, -200.0f, 200.0f), NextRandRangeF32(partRand, -200.0f, 200.0f)), v2(10.0f, 10.0f), particleColor});
                            }
                        }
                        cIndex++;
                    }
                    pIndex++;
                }

                RemoveIndicesFromVector(&gameCtx->activeCircles, &cIndices);
            }

            SimulateParticles(&gameCtx->activeCircles, playArea, appCtx->dt);

            std::vector<size_t> particleIndices;
            particleIndices.reserve(gameCtx->particles.size());
            for(size_t i = 0u; i < gameCtx->particles.size(); i++)
            {
                auto &p = gameCtx->particles[i];
                p.pos = p.pos + (p.vel * appCtx->dt);
                if(p.pos.x < playArea.min.x || p.pos.y < playArea.min.y ||
                   p.pos.x > playArea.max.x || p.pos.y > playArea.max.y)
                {
                    particleIndices.push_back(i);
                }
            }

            for(size_t i = 0u; i < gameCtx->projectiles.size(); i++)
            {
                auto &p = gameCtx->projectiles[i];
                p.pos = p.pos + (p.vel * appCtx->dt);
                if(p.pos.x < playArea.min.x || p.pos.y < playArea.min.y ||
                   p.pos.x > playArea.max.x || p.pos.y > playArea.max.y)
                {
                    projectileIndices.push_back(i);
                }
            }

            RemoveIndicesFromVector(&gameCtx->projectiles, &projectileIndices);
            RemoveIndicesFromVector(&gameCtx->particles, &particleIndices);

            ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(32.0f, 32.0f));
            ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 4.0f);
            ImGuiWindowFlags gameOverMenuFlags = ImGuiWindowFlags_NoMove;
            gameOverMenuFlags |= ImGuiWindowFlags_NoResize;
            gameOverMenuFlags |= ImGuiWindowFlags_NoCollapse;
            gameOverMenuFlags |= ImGuiWindowFlags_NoTitleBar;
            gameOverMenuFlags |= ImGuiWindowFlags_AlwaysAutoResize;
            gameOverMenuFlags |= ImGuiWindowFlags_NoSavedSettings;
            ImGui::SetNextWindowSize(ImVec2(300.0f, 0.0f));
            ImGui::SetNextWindowPos(ImVec2((appCtx->clientSize.x / 2.0f) - 150.0f, (appCtx->clientSize.y / 2.0f) - 150.0f));
            ImGui::SetNextWindowBgAlpha(1.0f);
            ImGui::Begin("Game Over Menu", nullptr, gameOverMenuFlags);

            f32 crw = ImGui::GetWindowContentRegionWidth();
            ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(7.0f, 14.0f));
            ImGui::PushStyleVar(ImGuiStyleVar_FrameRounding, 4.0f);
            ImGui::Text("Game Over");
            ImGui::Text("Score: %d", gameCtx->player.score);
            if(ImGui::Button("Restart", ImVec2(crw, 50.0f)))
            {
                gameCtx->mode = GameMode_Restart;
            }
            if(ImGui::Button("Quit", ImVec2(crw, 50.0f)))
            {
                // TODO: Here we should have our own platform
                // independent event/message system
                PostQuitMessage(0);
            }
            ImGui::PopStyleVar(2);
            ImGui::End();
            ImGui::PopStyleVar(2);
        }

        {
#if 1
            if(ImGui::BeginMenuBar())
            {
                if(ImGui::BeginMenu("App"))
                {
                    ImGui::EndMenu();
                }

                ImGui::EndMenuBar();
            }


            ImGuiWindow *mainWin = ImGui::GetCurrentWindow();
            f32 overlayOffsetY = mainWin->MenuBarHeight() + 5.0f;
            ImGuiViewport *guiViewport = ImGui::GetMainViewport();
            ImVec2 waPos = guiViewport->WorkPos;
            ImVec2 waSize = guiViewport->WorkSize;
            ImVec2 overlayPos = ImVec2(waPos.x + waSize.x - 5.0f, waPos.y + overlayOffsetY);
            ImGui::SetNextWindowPos(overlayPos, ImGuiCond_Always, ImVec2(1.0f, 0.0f));
            ImGui::SetNextWindowViewport(guiViewport->ID);

            ImGuiWindowFlags overlayFlags = ImGuiWindowFlags_NoDecoration;
            overlayFlags |= ImGuiWindowFlags_NoDocking;
            overlayFlags |= ImGuiWindowFlags_AlwaysAutoResize;
            overlayFlags |= ImGuiWindowFlags_NoSavedSettings;
            overlayFlags |= ImGuiWindowFlags_NoFocusOnAppearing;
            overlayFlags |= ImGuiWindowFlags_NoNav;
            overlayFlags |= ImGuiWindowFlags_NoMove;
            ImGui::Begin("frame_info_overlay_shape_shooter", NULL, overlayFlags);
            ImGui::Text("Frame Info:\n\n");
            ImGui::Text("FPS: %f", 1.0f / appCtx->dt);
            ImGui::Text("dt: %f s", appCtx->dt);
            ImGui::Text("Big Particle Active Count: %zu", gameCtx->activeCircles.size());
            ImGui::Text("Big Particle Inactive Count: %zu", gameCtx->inactiveCircles.size());
            ImGui::Text("Simple Particle Count: %zu", gameCtx->particles.size());
            ImGui::Text("Projectile Count: %zu", gameCtx->projectiles.size());
            ImGui::Text("ImGui::IO Mouse Pos X: %f", io.MousePos.x);
            ImGui::Text("ImGui::IO Mouse Pos Y: %f", io.MousePos.y);
            ImGui::Text("ImGui::WantCaptureMouse: %d", io.WantCaptureMouse);
            ImGui::End();
            ImGui::End();
#endif
}

        Render(appCtx, gameCtx);
    }

    static void Render(application_context *appCtx, game_context *gameCtx)
    {

        mat4 proj = CreateOrtho((u32)appCtx->clientSize.x, (u32)appCtx->clientSize.y);
        tbs_array_g<debug_vert_attribs> fanCircleCmd;
        for(const auto &c : gameCtx->inactiveCircles)
        {
            fanCircleCmd.Add({c.pos, c.col});
            for(f32 s = 0; s <= 360; s += 8.0f)
            {
                f32 x = Cos(DegreeToRadian(s)) * c.radius;
                f32 y = Sin(DegreeToRadian(s)) * c.radius;
                fanCircleCmd.Add({v2(c.pos.x + x, c.pos.y + y), c.col});
            }
        }

        for(const auto &c : gameCtx->activeCircles)
        {
            fanCircleCmd.Add({c.pos, c.col});
            for(f32 s = 0; s <= 360; s += 8.0f)
            {
                f32 x = Cos(DegreeToRadian(s)) * c.radius;
                f32 y = Sin(DegreeToRadian(s)) * c.radius;
                fanCircleCmd.Add({v2(c.pos.x + x, c.pos.y + y), c.col});
            }
        }

        if(fanCircleCmd.count > 0)
        {
            constexpr i32 totalSegments = (360 / 8) + 2;
            size_t totalCircleCount = gameCtx->inactiveCircles.size() + gameCtx->activeCircles.size();
            tbs_assert(totalCircleCount == (size_t)(fanCircleCmd.count / totalSegments));
            GLuint VAOID;
            GLuint VBOID;
            glGenVertexArrays(1, &VAOID);
            glGenBuffers(1, &VBOID);
            glBindVertexArray(VAOID);
            glBindBuffer(GL_ARRAY_BUFFER, VBOID);
            glBufferData(GL_ARRAY_BUFFER, (GLsizeiptr)fanCircleCmd.CalcSize(), fanCircleCmd.data, GL_STREAM_DRAW);
            glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(debug_vert_attribs), 0);
            glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(debug_vert_attribs), (void *)(offsetof(debug_vert_attribs, col)));
            glEnableVertexAttribArray(0);
            glEnableVertexAttribArray(1);

            GLuint progID = appCtx->shaderList[ShaderID_ShapeShooterPrimitive].programID;
            glUseProgram(progID);
            glUniformMatrix4fv(glGetUniformLocation(progID, "proj"), 1, GL_FALSE, mat4_addr(proj));
            for(i32 ci = 0; ci < fanCircleCmd.count / totalSegments; ci++)
            {
                glDrawArrays(GL_TRIANGLE_FAN, ci*totalSegments, totalSegments);
            }

            glBindBuffer(GL_ARRAY_BUFFER, 0);
            glBindVertexArray(0);
            glDeleteBuffers(1, &VBOID);
            glDeleteVertexArrays(1, &VAOID);

            fanCircleCmd.Free();
        }


        if(gameCtx->particles.size() > 0)
        {
            std::vector<debug_vert_attribs> vertAttribs;
            for(const auto &p : gameCtx->particles)
            {
                v2 p0 = p.pos;
                vertAttribs.push_back({p0, p.col});
                v2 p1 = v2(p0.x + p.size.x, p0.y);
                vertAttribs.push_back({p1, p.col});
                v2 p2 = p0 + p.size;
                vertAttribs.push_back({p2, p.col});

                vertAttribs.push_back({p2, p.col});
                v2 p3 = v2(p0.x, p0.y + p.size.y);
                vertAttribs.push_back({p3, p.col});
                vertAttribs.push_back({p0, p.col});
            }

            size_t byteSize = vertAttribs.size() * sizeof(debug_vert_attribs);
            GLuint VAOID;
            GLuint VBOID;
            glGenVertexArrays(1, &VAOID);
            glGenBuffers(1, &VBOID);
            glBindVertexArray(VAOID);
            glBindBuffer(GL_ARRAY_BUFFER, VBOID);
            glBufferData(GL_ARRAY_BUFFER, (GLsizeiptr)byteSize, vertAttribs.data(), GL_STREAM_DRAW);
            glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(debug_vert_attribs), 0);
            glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(debug_vert_attribs), (void *)(offsetof(debug_vert_attribs, col)));
            glEnableVertexAttribArray(0);
            glEnableVertexAttribArray(1);

            GLuint progID = appCtx->shaderList[ShaderID_ShapeShooterPrimitive].programID;
            glUseProgram(progID);
            glUniformMatrix4fv(glGetUniformLocation(progID, "proj"), 1, GL_FALSE, mat4_addr(proj));
            glDrawArrays(GL_TRIANGLES, 0, (GLsizei)vertAttribs.size());

            glBindBuffer(GL_ARRAY_BUFFER, 0);
            glBindVertexArray(0);
            glDeleteBuffers(1, &VBOID);
            glDeleteVertexArrays(1, &VAOID);
        }

        if(gameCtx->projectiles.size() > 0)
        {
            std::vector<debug_vert_attribs> vertAttribs;
            v4 col{0.0f, 1.0f, 0.0f, 1.0f};
            for(const auto &p : gameCtx->projectiles)
            {
                v2 p0 = p.pos;
                vertAttribs.push_back({p0, col});
                v2 p1 = v2(p0.x + p.size.x, p0.y);
                vertAttribs.push_back({p1, col});
                v2 p2 = p0 + p.size;
                vertAttribs.push_back({p2, col});

                vertAttribs.push_back({p2, col});
                v2 p3 = v2(p0.x, p0.y + p.size.y);
                vertAttribs.push_back({p3, col});
                vertAttribs.push_back({p0, col});
            }

            size_t byteSize = vertAttribs.size() * sizeof(debug_vert_attribs);
            GLuint VAOID;
            GLuint VBOID;
            glGenVertexArrays(1, &VAOID);
            glGenBuffers(1, &VBOID);
            glBindVertexArray(VAOID);
            glBindBuffer(GL_ARRAY_BUFFER, VBOID);
            glBufferData(GL_ARRAY_BUFFER, (GLsizeiptr)byteSize, vertAttribs.data(), GL_STREAM_DRAW);
            glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(debug_vert_attribs), 0);
            glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(debug_vert_attribs), (void *)(offsetof(debug_vert_attribs, col)));
            glEnableVertexAttribArray(0);
            glEnableVertexAttribArray(1);

            GLuint progID = appCtx->shaderList[ShaderID_ShapeShooterPrimitive].programID;
            glUseProgram(progID);
            glUniformMatrix4fv(glGetUniformLocation(progID, "proj"), 1, GL_FALSE, mat4_addr(proj));
            glDrawArrays(GL_TRIANGLES, 0, (GLsizei)vertAttribs.size());

            glBindBuffer(GL_ARRAY_BUFFER, 0);
            glBindVertexArray(0);
            glDeleteBuffers(1, &VBOID);
            glDeleteVertexArrays(1, &VAOID);
        }

#if 1
        if(gameCtx->player.health > 0)
        {
            mat3 rotMat = RotateDeg2D_w(gameCtx->player.rot);
            v3 forward = rotMat * v3(1.0f, 0.0f, 0.0f);

            mat3 model = Translate2D_w(gameCtx->player.pos) * rotMat * Scale2D_w(gameCtx->player.dim);
            f32 vertAttribs[] =
            {
                -0.5f, -0.5f, 1.0f, 0.0f, 0.0f,
                 0.5f, -0.5f, 0.0f, 1.0f, 0.0f,
                 0.5f,  0.5f, 0.0f, 0.0f, 1.0f,

                 0.5f,  0.5f, 0.0f, 0.0f, 1.0f,
                -0.5f,  0.5f, 0.0f, 0.0f, 1.0f,
                -0.5f, -0.5f, 1.0f, 0.0f, 0.0f,

                 0.0f, -0.1f, 1.0f, 0.0f, 0.0f,
                 1.0f, -0.1f, 0.0f, 1.0f, 0.0f,
                 1.0f,  0.1f, 0.0f, 0.0f, 1.0f,

                 1.0f,  0.1f, 0.0f, 0.0f, 1.0f,
                 0.0f,  0.1f, 0.0f, 0.0f, 1.0f,
                 0.0f, -0.1f, 1.0f, 0.0f, 0.0f,
            };

            GLuint VAOID;
            GLuint VBOID;
            glGenVertexArrays(1, &VAOID);
            glGenBuffers(1, &VBOID);
            glBindVertexArray(VAOID);
            glBindBuffer(GL_ARRAY_BUFFER, VBOID);
            glBufferData(GL_ARRAY_BUFFER, (GLsizeiptr)sizeof(vertAttribs), vertAttribs, GL_STREAM_DRAW);
            glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(f32) * 5, 0);
            glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(f32) * 5, (void *)(sizeof(f32) * 2));
            glEnableVertexAttribArray(0);
            glEnableVertexAttribArray(1);

            GLuint progID = appCtx->shaderList[ShaderID_ShapeShooterPlayer].programID;
            glUseProgram(progID);
            glUniformMatrix4fv(glGetUniformLocation(progID, "proj"), 1, GL_FALSE, mat4_addr(proj));
            glUniformMatrix3fv(glGetUniformLocation(progID, "model"), 1, GL_FALSE, mat3_addr(model));
            glDrawArrays(GL_TRIANGLES, 0, 12);


            {
                v2 playerForward = RotateDeg2D(gameCtx->player.rot) * v2(1.0f, 0.0f);
                v2 playerUp = v2(-playerForward.y, playerForward.x);
                // NOTE: Calculate collision boxes for player
                v2 p0 = gameCtx->player.pos + (-playerForward * (gameCtx->player.dim.x * 0.5f) + (-playerUp * (gameCtx->player.dim.y * 0.5f)));
                v2 p1 = p0 + (playerForward * gameCtx->player.dim.x);
                v2 p2 = p0 + (playerUp * gameCtx->player.dim.y);
                v2 p3 = p1 + (playerUp * gameCtx->player.dim.y);
                v2 p4 = gameCtx->player.pos + (-playerUp * gameCtx->player.dim.y * 0.1f);
                v2 p5 = p4 + (playerForward * gameCtx->player.dim.x);
                v2 p6 = p4 + (playerUp * gameCtx->player.dim.y * 0.2f);
                v2 p7 = p5 + (playerUp * gameCtx->player.dim.y * 0.2f);

                f32 debugPointsAttribs[] =
                {
                    p0.x, p0.y, 1.0f, 1.0f, 1.0f,
                    p1.x, p1.y, 0.0f, 1.0f, 1.0f,
                    p2.x, p2.y, 0.0f, 1.0f, 0.0f,
                    p3.x, p3.y, 1.0f, 0.0f, 1.0f,

                    p4.x, p4.y, 1.0f, 1.0f, 0.0f,
                    p5.x, p5.y, 1.0f, 1.0f, 0.0f,
                    p6.x, p6.y, 1.0f, 1.0f, 0.0f,
                    p7.x, p7.y, 1.0f, 1.0f, 0.0f,
                };

                glPointSize(2.0f);
                mat3 pointModel = CreateIdentity3D();
                glUniformMatrix3fv(glGetUniformLocation(progID, "model"), 1, GL_FALSE, mat3_addr(pointModel));
                glBufferData(GL_ARRAY_BUFFER, (GLsizeiptr)sizeof(debugPointsAttribs), debugPointsAttribs, GL_STREAM_DRAW);
                glDrawArrays(GL_POINTS, 0, array_count(debugPointsAttribs));
            }

            glBindBuffer(GL_ARRAY_BUFFER, 0);
            glBindVertexArray(0);
            glDeleteBuffers(1, &VBOID);
            glDeleteVertexArrays(1, &VAOID);
        }
#endif

    }
}
