namespace shape_shooter
{
    enum game_mode
    {
        GameMode_None,
        GameMode_StartMenu,
        GameMode_PauseMenu,
        GameMode_Start,
        GameMode_Playing,
        GameMode_GameOver,
        GameMode_Victory,
        GameMode_Restart,
    };

    enum editor_mode
    {
        EditorMode_None,
        EditorMode_AddTriangle,
        EditorMode_AddRectangle,
        EditorMode_AddCircle,
        EditorMode_AddLine,
        EditorMode_AddPolygon,
    };

    enum primitive_type
    {
        PrimitiveType_None,
        PrimitiveType_Circle,
    };

    struct debug_vert_attribs
    {
        v2 pos;
        v4 col;
    };

    tbs_s_assert(sizeof(debug_vert_attribs) == 24, "debug_vert_attribs struct unexpected size");

    struct player_state
    {
        v2 pos;
        v2 vel;
        v2 dim;
        f32 rot;
        i32 health;
        u32 score;
    };

    struct shape_circle
    {
        v2 pos;
        v2 oldPos;
        v2 vel;
        f32 radius;
        v4 col;
    };

    struct rect_2d
    {
        v2 min;
        v2 max;
    };

    struct simple_particle
    {
        v2 pos;
        v2 vel;
        v2 size;
        v4 col;
    };

    struct projectile_data
    {
        v2 pos;
        v2 vel;
        v2 size;
    };

    constexpr size_t SHAPE_MAX_COUNT = 100;
    constexpr f32 SPAWN_TIMER_TRESHOLD = 1.0f;
    constexpr f32 SHOOT_TIMER_TRESHOLD = 0.3f;
    struct game_context
    {
        bool isInit;

        player_state player;
        std::vector<projectile_data> projectiles;
        std::vector<shape_circle> inactiveCircles;
        std::vector<shape_circle> activeCircles;
        std::vector<simple_particle> particles;

        game_mode mode;
        editor_mode editorMode;
        f32 spawnTimer;

        f32 shootTimer;

        game_context() : isInit{false}
        {
            activeCircles.reserve(SHAPE_MAX_COUNT);
            inactiveCircles.reserve(SHAPE_MAX_COUNT);
        }

    };

    static void Render(application_context *appCtx, game_context *gameCtx);
}
